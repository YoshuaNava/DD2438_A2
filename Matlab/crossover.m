function child = crossover(parentA, parentB)

% Sorting the parentA and parent structure into assending order of fitness
[~, sx] = sort([parentA.being.fitness]);
parentA.being = parentA.being(sx);

[~, sx] = sort([parentB.being.fitness]);
parentB.being = parentB.being(sx);

% Child
child.being(1) = parentA.being(1);
child.being(2) = parentA.being(2);
child.being(3) = parentB.being(3);
child.being(4) = parentB.being(4);

end
