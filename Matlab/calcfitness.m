function [fitness, q_being] = calcfitness(q_being, int_point)
% The function gives the fitness for the each being, more closer it to the
% desired state, higher the fitness will be.

% fitness = sum(a*(distance of each bot from other three) + b*(number of
% interet points inside the circle)

% Interest Point in free sapce
x_point = int_point(:,1);
y_point = int_point(:,2);

% Weights for the intrest points and the distance
w_pnt = 0.9;
w_dist = 0.1;

%---------------------------Individual fitness----------------------------%
%---------------------------Robot1----------------------------------------%
% Distance from other bots
dist = ecdist(q_being(1).coord, q_being(2).coord) + ecdist(q_being(1).coord, q_being(3).coord) + ecdist(q_being(1).coord, q_being(4).coord);

% Number of intrest point in sensorfield
x = q_being(1).senfield(:,1);
y = q_being(1).senfield(:,2);
% Number of points for Bot1
in = inpolygon(x_point, y_point, x, y);

% Score
bot1 = (w_pnt*numel(x_point(in)) + w_dist*dist);
q_being(1).fitness = bot1;

%---------------------------Robot2----------------------------------------%
% Distance from other bots
dist = ecdist(q_being(2).coord, q_being(1).coord) + ecdist(q_being(2).coord, q_being(3).coord) + ecdist(q_being(2).coord, q_being(4).coord);

% Number of intrest point in sensorfield
x = q_being(2).senfield(:,1);
y = q_being(2).senfield(:,2);
% Number of points for Bot1
in = inpolygon(x_point, y_point, x, y);

% Score
bot2 = (w_pnt*numel(x_point(in)) + w_dist*dist);
q_being(2).fitness = bot2;

%---------------------------Robot3----------------------------------------%
% Distance from other bots
dist = ecdist(q_being(3).coord, q_being(1).coord) + ecdist(q_being(3).coord, q_being(2).coord) + ecdist(q_being(3).coord, q_being(4).coord);

% Number of intrest point in sensorfield
x = q_being(3).senfield(:,1);
y = q_being(3).senfield(:,2);
% Number of points for Bot1
in = inpolygon(x_point, y_point, x, y);

% Score
bot3 = (w_pnt*numel(x_point(in)) + w_dist*dist);
q_being(3).fitness = bot3;

%---------------------------Robot4----------------------------------------%
% Distance from other bots
dist = ecdist(q_being(4).coord, q_being(1).coord) + ecdist(q_being(4).coord, q_being(2).coord) + ecdist(q_being(4).coord, q_being(3).coord);

% Number of intrest point in sensorfield
x = q_being(4).senfield(:,1);
y = q_being(4).senfield(:,2);
% Number of points for Bot1
in = inpolygon(x_point, y_point, x, y);

% Score
bot4 = (w_pnt*numel(x_point(in)) + w_dist*dist);
q_being(4).fitness = bot4;


%---------------------------Total Rating----------------------------------%
fitness = bot1 + bot2 + bot3 + bot4;

end
