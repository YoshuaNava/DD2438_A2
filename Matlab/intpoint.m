function point = intpoint(x_max, y_max, dims, ob)
% Discrtize the space inside the bounded area and save all the points
% in the free space as interest points

point = [];

for i = 1:x_max
    for j = 1:y_max
        tmp = [i, j];
        flag = bounded(tmp, dims, ob(1).coord, ob(2).coord, ob(3).coord, ob(4).coord, ob(5).coord, ob(6).coord);
        if (flag == 1) 
            point = [point; tmp];
        end
    end
end
% intpoint(1).coord = map_data.item_1_1;
% intpoint(2).coord = map_data.item_1_2;
% intpoint(3).coord = map_data.item_1_3;
% intpoint(4).coord = map_data.item_1_4;
% intpoint(5).coord = map_data.item_1_5;
% intpoint(6).coord = map_data.item_1_6;
% intpoint(7).coord = map_data.item_1_7;
% intpoint(8).coord = map_data.item_1_8;
% intpoint(9).coord = map_data.item_1_9;
% intpoint(10).coord = map_data.item_1_10;
% intpoint(11).coord = map_data.item_1_11;
% intpoint(12).coord = map_data.item_1_12;
% intpoint(13).coord = map_data.item_1_13;
% intpoint(14).coord = map_data.item_1_14;
% intpoint(15).coord = map_data.item_1_15;
% intpoint(16).coord = map_data.item_1_16;
% intpoint(17).coord = map_data.item_1_17;
% intpoint(18).coord = map_data.item_1_18;
% intpoint(19).coord = map_data.item_1_19;
% intpoint(20).coord = map_data.item_1_20;
% 
% intpoint(21).coord = map_data.item_2_1;
% intpoint(22).coord = map_data.item_2_2;
% intpoint(23).coord = map_data.item_2_3;
% intpoint(24).coord = map_data.item_2_4;
% intpoint(25).coord = map_data.item_2_5;
% intpoint(26).coord = map_data.item_2_6;
% intpoint(27).coord = map_data.item_2_7;
% intpoint(28).coord = map_data.item_2_8;
% intpoint(29).coord = map_data.item_2_9;
% intpoint(30).coord = map_data.item_2_10;
% intpoint(31).coord = map_data.item_2_12;
% intpoint(32).coord = map_data.item_2_12;
% intpoint(33).coord = map_data.item_2_13;
% intpoint(34).coord = map_data.item_2_14;
% intpoint(35).coord = map_data.item_2_15;
% intpoint(36).coord = map_data.item_2_16;
% intpoint(37).coord = map_data.item_2_17;
% intpoint(38).coord = map_data.item_2_18;
% intpoint(39).coord = map_data.item_2_19;
% intpoint(40).coord = map_data.item_2_20;
% 
% intpoint(41).coord = map_data.item_3_1;
% intpoint(42).coord = map_data.item_3_2;
% intpoint(43).coord = map_data.item_3_3;
% intpoint(44).coord = map_data.item_3_4;
% intpoint(45).coord = map_data.item_3_5;
% intpoint(46).coord = map_data.item_3_6;
% intpoint(47).coord = map_data.item_3_7;
% intpoint(48).coord = map_data.item_3_8;
% intpoint(49).coord = map_data.item_3_9;
% intpoint(50).coord = map_data.item_3_10;
% intpoint(51).coord = map_data.item_3_12;
% intpoint(52).coord = map_data.item_3_12;
% intpoint(53).coord = map_data.item_3_13;
% intpoint(54).coord = map_data.item_3_14;
% intpoint(55).coord = map_data.item_3_15;
% intpoint(56).coord = map_data.item_3_16;
% intpoint(57).coord = map_data.item_3_17;
% intpoint(58).coord = map_data.item_3_18;
% intpoint(59).coord = map_data.item_3_19;
% intpoint(60).coord = map_data.item_3_20;
% 
% intpoint(61).coord = map_data.item_4_1;
% intpoint(62).coord = map_data.item_4_2;
% intpoint(63).coord = map_data.item_4_3;
% intpoint(64).coord = map_data.item_4_4;
% intpoint(65).coord = map_data.item_4_5;
% intpoint(66).coord = map_data.item_4_6;
% intpoint(67).coord = map_data.item_4_7;
% intpoint(68).coord = map_data.item_4_8;
% intpoint(69).coord = map_data.item_4_9;
% intpoint(70).coord = map_data.item_4_10;
% intpoint(71).coord = map_data.item_4_12;
% intpoint(72).coord = map_data.item_4_12;
% intpoint(73).coord = map_data.item_4_13;
% intpoint(74).coord = map_data.item_4_14;
% intpoint(75).coord = map_data.item_4_15;
% intpoint(76).coord = map_data.item_4_16;
% intpoint(77).coord = map_data.item_4_17;
% intpoint(78).coord = map_data.item_4_18;
% 
% intpoint(79).coord = map_data.item_5_1;
% intpoint(80).coord = map_data.item_5_2;
% intpoint(81).coord = map_data.item_5_4;
% intpoint(82).coord = map_data.item_5_5;
% intpoint(83).coord = map_data.item_5_6;
% intpoint(84).coord = map_data.item_5_7;
% intpoint(85).coord = map_data.item_5_8;
% intpoint(86).coord = map_data.item_5_9;
% intpoint(87).coord = map_data.item_5_10;
% intpoint(88).coord = map_data.item_5_12;
% intpoint(89).coord = map_data.item_5_12;
% intpoint(90).coord = map_data.item_5_13;
% intpoint(91).coord = map_data.item_5_14;
% intpoint(92).coord = map_data.item_5_15;
% intpoint(93).coord = map_data.item_5_16;
% intpoint(94).coord = map_data.item_5_17;
% intpoint(95).coord = map_data.item_5_18;
% intpoint(96).coord = map_data.item_5_19;
% intpoint(97).coord = map_data.item_5_20;
end
