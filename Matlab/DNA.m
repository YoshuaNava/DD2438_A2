function genes = DNA(q_being, nob, x_max, y_max, dims, ob, sen_range)
% The function create each member of the population based upon the genes
% definition.

genes = q_being;

    for i = 1:nob
        % Generate a random x and y position, such that the coordinate are
        % in side the boundry and outside the obstacle.
        
        % Random point based upon the boundry conditions
         randpoint = [rand(1)*x_max, rand(1)*y_max];
        
        % Check if the are not inside any obstacle
        flag = bounded(randpoint, dims, ob(1).coord, ob(2).coord, ob(3).coord, ob(4).coord, ob(5).coord, ob(6).coord);
        if (flag == 1)
            % Assign new coordinates
            genes(i).coord = randpoint;
            % Assign new sensor field
            [genes(i).senfield(:,1), genes(i).senfield(:,2)] = circle(genes(i).coord(1),genes(i).coord(2),sen_range);
        end
    end
    
end
