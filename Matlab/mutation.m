function child = mutation(child, mutationRate)

r = rand;
if(r < mutationRate)
    for i = 1:4
        child.being(i).coord(1) = child.being(i).coord(1) + (r - 0.5); 
        child.being(i).coord(2) = child.being(i).coord(2) + (r - 0.5);
    end
end

end
