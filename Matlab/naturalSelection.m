function [parentA, parentB] = naturalSelection(pop, popmax)
% Uniform sampling from AE for selecting the the most probable parent
% to create a child

% Calculating the CDF
CDF = zeros(1,popmax);
for itr = 1:popmax
    for j = 1:itr
        CDF(itr) = (CDF(itr) + pop(j).fitness);
    end
end
CDF = CDF /100;

tmp = 1;
% Create a genepool
r = abs(0.5-rand);
for itr = 1:popmax
    for j = 1:popmax
        if(CDF(j) > r + ((itr - 1) / popmax))
            tmp = itr;
            break;
        end
    end
    genepool(itr) = pop(tmp);
end

r = randi([1 10],1,2);

% Select Parent A
parentA = genepool(r(1,1));

% Select parent B
%r = ceil(10*rand);
parentB = genepool(r(1,2));


end
