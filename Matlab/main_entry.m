close all;
clc;

%---------------------------Reading Map from Json-------------------------%

% Read Map data from the Json file
map_data = loadjson('problem_A12.json');

%---------------------------Boundry and Obstacles-------------------------%
% Boundry
dims = map_data.boundary_polygon;
x_dims = dims(:,1);
y_dims = dims(:,2);
x_max = max(x_dims);
y_max = max(y_dims);

% Obstacles
ob(1).coord = map_data.polygon0;
ob(2).coord = map_data.polygon1;
ob(3).coord = map_data.polygon2;
ob(4).coord = map_data.polygon3;
ob(5).coord = map_data.polygon4;
ob(6).coord = map_data.polygon5;

%---------------------------Interest Points-------------------------------%
int_point = intpoint(x_max, y_max, dims, ob);

%---------------------------Start and Goal--------------------------------%
% Number of Robots given
nob = 4;

% Start positions for the 4 robots
q_start(1).coord = map_data.start_pos;
q_start(2).coord = map_data.start_pos2;
q_start(3).coord = map_data.start_pos2;
q_start(4).coord = map_data.start_pos3;

% Goal position
% q_goal(1).coord = map_data.goal_pos;
% q_goal(2).coord = map_data.goal_pos1;
% q_goal(3).coord = map_data.goal_pos3;
% q_goal(4).coord = map_data.goal_pos4;

%---------------------------Sensor Line of Vision-------------------------%
sen_range = map_data.sensor_range;

% %---------------------------Create sensor field---------------------------%
for itr = 1:nob
    [q_start(itr).senfield(:,1), q_start(itr).senfield(:,2)] = circle(q_start(itr).coord(1),q_start(itr).coord(2),sen_range);
end

%---------------------------Plotting of Map-------------------------------%
% Plotting figure
figure(1)
hold on

% % Plot boundry
% plot(x_dims,y_dims);
% hold on
%
% % Plot Obstacles
% for itr = 1:6
%     fill(ob(itr).coord(:,1), ob(itr).coord(:,2), 'g');
%     hold on;
% end
%
% axis([0 25 0 25]);
% axis square;
% hold on

%---------------------------Genetic Algorithm-----------------------------%
%---------------------------Genetic Algorithm-----------------------------%

% Intial constrains
% Maximum number of member in population
popmax = 10;
% Mutation rate
mutationRate = 0.1;
% Generations
max_generation = 10;

% Initial population
q_goal = q_start;
q_goal(1).fitness = 0;
q_goal(2).fitness = 0;
q_goal(3).fitness = 0;
q_goal(4).fitness = 0;


%---------------------------Initialize------------------------------------%
% Generate an initial population of being, each being created from the
% DNA.

for itr = 1:popmax
    pop(itr).being = DNA(q_goal, nob, x_max, y_max, dims, ob, sen_range);
    
    % Plot new points and Sensor field
    %         for j = 1:nob
    %              dummy = input('enter');
    %              plot(pop(i).being(j).coord(1), pop(i).being(j).coord(2), '*','LineWidth', 2);
    %              plot(pop(i).being(j).senfield(:,1), pop(i).being(j).senfield(:,2));
    %         end
end


for generation = 1:max_generation
    
    %---------------------------Selection-------------------------------------%
    % From the generated population, now we have to give them a fitness rating
    % on how close they are to the desired state. Closer they are to desired,
    % higher the rating.
    
    % First, we need to calculate the fitness for every being in the the
    % population
    for itr = 1:popmax
        [pop(itr).fitness, pop(itr).being] = calcfitness(pop(itr).being, int_point);
    end
    
    % Sum the fitness of the population
    sum = 0;
    for itr = 1:popmax
        sum = sum + pop(itr).fitness;
    end
    
    % New fitness
    for itr = 1:popmax
        pop(itr).fitness = 100*(pop(itr).fitness/sum);
    end
    
    % Sorting the population structure into assending order
    [~, sx] = sort([pop.fitness]);
    pop = pop(sx);
    
    q_goal = pop(popmax).being;
    %---------------------------Create sensor field---------------------------%
    for itr = 1:nob
        [q_goal(itr).senfield(:,1), q_goal(itr).senfield(:,2)] = circle(q_goal(itr).coord(1),q_goal(itr).coord(2),sen_range);
    end
    
    % Plot new position and Sensor field
    plot_map(q_goal);
    %     for itr = 1:nob
    %         plot_1 = plot(q_goal(itr).coord(1), q_goal(itr).coord(2), '*','LineWidth', 1);
    %         plot_2 = plot(q_goal(itr).senfield(:,1), q_goal(itr).senfield(:,2));
    %     end
    
    
    dummy = input('enter');
    clf;
    %     delete(plot_1);
    
    %---------------------------Reproduction----------------------------------%
    % Step1 - We have to select the parents in the population, basde upon
    % their fitness criteria. The systematic sampling picks the parent and
    % they are crossover to create a child. The mutation is done in this part
    % also.
    
    % New population
    for itr = 1:popmax
        % Systematic sampling for selecting parents
        [parentA, parentB] = naturalSelection(pop, popmax);
        % Crossover of the parents genes to create a child
        child = crossover(parentA, parentB);
        % Mutation
        child = mutation(child, mutationRate);
        
        pop(itr).being = child.being;
        pop(itr).fitness = 0;
        
    end
    
end







