function plot_map(q_goal)
map_data = loadjson('problem_A12.json');

%---------------------------Boundry and Obstacles-------------------------%
% Boundry
dims = map_data.boundary_polygon;
x_dims = dims(:,1);
y_dims = dims(:,2);


% Obstacles
ob(1).coord = map_data.polygon0;
ob(2).coord = map_data.polygon1;
ob(3).coord = map_data.polygon2;
ob(4).coord = map_data.polygon3;
ob(5).coord = map_data.polygon4;
ob(6).coord = map_data.polygon5;

%---------------------------Plotting of Map-------------------------------%

% Plot boundry
plot(x_dims,y_dims);
hold on

% Plot Obstacles
for itr = 1:6
    fill(ob(itr).coord(:,1), ob(itr).coord(:,2), 'g');
    hold on;
end

axis([0 25 0 25]);
axis square;
hold on
%---------------------------Genetic Algorithm-----------------------------%
%---------------------------Selection-------------------------------------%
for itr = 1:4
        plot_1 = plot(q_goal(itr).coord(1), q_goal(itr).coord(2), '*','LineWidth', 1);
        plot_2 = plot(q_goal(itr).senfield(:,1), q_goal(itr).senfield(:,2));
end

end
