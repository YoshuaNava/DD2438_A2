"""
Created on Mon Jan 23 19:57 2017

@author: Ivan Jokic

instructions:

1. Go to /Users/yourname/Documents/V-REP_PRO_EDU_V3_3_2_Mac/programming/remoteApiBindings/python
2. copy all content and paste in the same directory as your python code.
3. Go to /Users/yourname/Documents/V-REP_PRO_EDU_V3_3_2_Mac/programming/remoteApiBindings/lib
4. Copy file and paste in the same directory as your python code
5. Copy simExtRemoteApiStart(19999) and paste into a threaded child script under '-- Put some initialization code here:'
6. Start the simulation and let it run.
7. Write your python code (below) and run to control robot.

"""

from vrep_python import vrep
import sys

vrep.simxFinish(-1) # just in case, close all opened connections

clientID=vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP

if clientID!=-1:
    print('Connected to remote API server')

else:
    print('Connection not successful')
    sys.exit('Could not connect')

errorCode, left_motor_handle = vrep.simxGetObjectHandle(clientID, 'Pioneer_p3dx_leftMotor', vrep.simx_opmode_oneshot_wait)
errorCode, right_motor_handle = vrep.simxGetObjectHandle(clientID, 'Pioneer_p3dx_rightMotor', vrep.simx_opmode_oneshot_wait)


errorCode = vrep.simxSetJointTargetVelocity(clientID, left_motor_handle, 1, vrep.simx_opmode_streaming)
errorCode = vrep.simxSetJointTargetVelocity(clientID, right_motor_handle, 1, vrep.simx_opmode_streaming)
