import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import time
import math
import pprint as pp
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import distance
from scipy.spatial import ConvexHull
import networkx as nx

from util import scene_plot
from util import polygon_tools as poly_tools




class DiscreteScene:
    
    def __init__(self, scene_params, x_res, y_res):
        self.scene_params = scene_params
        self.x_res = x_res
        self.y_res = y_res
        self.x_lim = self.scene_params["boundary_polygon"][2][0]
        self.y_lim = self.scene_params["boundary_polygon"][2][1]
        self.x_disc = np.linspace(0, self.x_lim, num = self.x_res)
        self.y_disc = np.linspace(0, self.y_lim, num = self.y_res)
        
        self.boundary_triags = poly_tools._triangulate_polygone(self.scene_params['boundary_polygon'])
        self.obstacle_triags = map(poly_tools._triangulate_polygone, self.scene_params['obstacles'])
        self.obstacle_triags = np.array( list( itertools.chain(*self.obstacle_triags) ) )
        self.sensor_range = self.scene_params["sensor_range"]
        
        self.G_map = nx.Graph()
        self.G_convex_covers = nx.Graph()
        self.obstacle_penalty = 10000000000
        
        
        
    def discretize_map(self):
        self.map = np.zeros( (self.x_res, self.y_res) )
        for i in range(0, len(self.x_disc)):
            for j in range(0, len(self.y_disc)):
                point = np.array( (self.x_disc[i], self.y_disc[j]) )
                if poly_tools.withinBoundary(self.boundary_triags, point):
                    if poly_tools.checkPointCollision(self.obstacle_triags, self.boundary_triags, point, point):
#                        if poly_tools.obstacle_free(self.obstacle_triags, point, point):
                        self.map[i][j] = 1
                    else:
                        self.map[i][j] = 0
                else:
                    self.map[i][j] = -1
    
            

    def load_scene_item_points(self):
        num_items = self.scene_params["num_items"]
        num_points_total = 0
        for item_idx in range(0, num_items):
            num_points_item = self.scene_params["item_" + str(item_idx+1) + "_cardinality"]
            num_points_total = num_points_total + num_points_item
    
        self.item_points = np.zeros( (num_points_total, 2) )
        i = 0
        for item_idx in range(0, num_items):
            item = self.scene_params["item_" + str(item_idx+1) + "_points"]
            num_points_item = self.scene_params["item_" + str(item_idx+1) + "_cardinality"]
            for point_idx in range(0, num_points_item):
                self.item_points[i, 0] = item[point_idx][0]
                self.item_points[i, 1] = item[point_idx][1]
                i = i + 1





    # Recipe from the itertools documentation.
    def pairwise(self, iterable, cyclic=False):
        a, b = itertools.tee(iterable)
        first = next(b, None)
        if cyclic is True:
            return zip(a, itertools.chain(b, (first,)))
        return zip(a, b)
    
    
    
    
    def grid_2d_graph_8neighbors(self, m, n, periodic=False, create_using=None):
        """ Return the 2d grid graph of mxn nodes
        
        The grid graph has each node connected to its eight nearest neighbors.
        """
        G = nx.empty_graph(0, create_using)
        row_name, rows = m
        col_name, columns = n
        range_rows = range(rows)
        inv_range_rows = range(rows-1,0+1,-1)
        range_cols = range(columns)
        G.name = "grid_2d_graph(%s, %s)" % (row_name, col_name)
        nodes = [(i, j) for i in range_rows for j in range_cols]
    
        G.add_nodes_from(nodes)
        
        horizontal_neighbors = [((i, j), (pi, j))
                         for pi, i in self.pairwise(range_rows) for j in range_cols]
        G.add_edges_from(horizontal_neighbors)
        
        vertical_neighbors = [((i, j), (i, pj))
                         for i in range_rows for pj, j in self.pairwise(range_cols)]
        G.add_edges_from(vertical_neighbors)
                             
        diagonal_neighbors = [((i, j), (pi, pj))
                         for pi, i in self.pairwise(range_rows) for pj, j in self.pairwise(range_cols)]
        G.add_edges_from(diagonal_neighbors)
                             
        antidiagonal_neighbors = [((i, j), (pi, pj))
                         for pi, i in self.pairwise(inv_range_rows) for pj, j in self.pairwise(range_cols)]
        G.add_edges_from(antidiagonal_neighbors)
    
        return G
    
    

    def create_map_graph(self):
        self.G_map = self.grid_2d_graph_8neighbors( ('x', self.x_res), ('y', self.y_res))
#        self.G_map = nx.grid_2d_graph(self.x_res, self.y_res)
        nx.set_node_attributes(self.G_map, 'invalid', 0)
        nx.set_node_attributes(self.G_map, 'obstacle', 0)
        nx.set_node_attributes(self.G_map, 'covered', 0)
        nx.set_node_attributes(self.G_map, 'map_indices', (-1.0,-1.0))
        nodes_points_map = {}

        
        # Insert the map information into the graph
        for node in self.G_map.nodes_iter(data=True):
            i = node[0][0]
            j = node[0][1]
            
            point = (self.x_disc[i], self.y_disc[j])
            node[1]['map_indices'] = (i, j)
            
            nodes_points_map[node[0]] = point
            
            if self.map[i][j] == 1:
                node[1]['obstacle'] = 1
            if self.map[i][j] == -1:
                node[1]['invalid'] = 1
                
            if (node[1]['invalid'] == 1) or (node[1]['obstacle'] == 1):
                ebunch = nx.edges(self.G_map, node[0])
                self.G_map.remove_edges_from(ebunch)
                
        self.G_map = nx.relabel_nodes(self.G_map, nodes_points_map)
    
    
    
    
    
    def find_uncovered_map_cell(self):
        for node in self.G_map.nodes_iter(data=True):
            if (node[1]['covered'] == 0) and (node[1]['obstacle'] == 0) and (node[1]['invalid'] == 0):
                return node                
        return None
                
    
    
    
    
    def set_map_cell_covered(self, G_map, node):
        G_map.node[node]['covered'] = 1
        return G_map
    



    def generate_convex_cover(self):
        all_covered = False
        map_cp = self.map.copy()
        
        while (all_covered == False):
        #    print("\n\n\n\n")
        #    print("Processing new uncovered cell")
            p = self.find_uncovered_map_cell()
        #    pp.pprint(p)
            sensed_points = set()
            if(p is not None):
#                map_cp = self.map.copy()
                i = p[1]['map_indices'][0]
                j = p[1]['map_indices'][1]
                map_cp[i][j] = -10
                self.G_map = self.set_map_cell_covered(self.G_map, p[0])
        
                self.G_map, map_cp, sensed_points = self.grow_rectangle(self.G_map, p[0], sensed_points, p[0], map_cp)
                hull_points = np.array( list( sensed_points ) )
                if(len(sensed_points)>3):
                    hull = ConvexHull( hull_points )
                else:
                    hull = None
                self.G_convex_covers.add_node( p[0], sensed_points=hull_points, convex_hull=hull )
            else:
                all_covered = True
        
        print("\n\n")
        print("Scene covered until now:")
        self.plot_map_dynamic(map_cp)
        plt.show()
        
        # Find neighboring distances between convex covers
        for cover_set in self.G_convex_covers.nodes_iter(data=True):
            for other_cover in self.G_convex_covers.nodes_iter(data=True):
                covers_distance = distance.euclidean(cover_set[0], other_cover[0])
                
                free_path = poly_tools.obstacle_free(self.obstacle_triags, cover_set[0], other_cover[0])
                if(covers_distance != 0): #and free_path == True: # and (covers_distance <= 2*self.sensor_range):
                    if free_path:
                        self.G_convex_covers.add_edge(cover_set[0], other_cover[0], score=covers_distance)
                    else:
                        self.G_convex_covers.add_edge(cover_set[0], other_cover[0], score=self.obstacle_penalty)
                else:
                    self.G_convex_covers.add_edge(cover_set[0], other_cover[0], score=0)

        # Detect and count item points inside each convex cover
        self.num_item_points = self.item_points.shape[0]
        nx.set_node_attributes(self.G_convex_covers, 'num_items_inside', 0)
        nx.set_node_attributes(self.G_convex_covers, 'items_inside', 0)
        nx.set_node_attributes(self.G_convex_covers, 'id', 0)
        nx.set_node_attributes(self.G_convex_covers, 'centroid', (-1.0,-1.0))
        i = 0
        for cover_set in self.G_convex_covers.nodes_iter(data=True):
            hull =  cover_set[1]['convex_hull']
            if(hull is not None):
                items_inside = poly_tools.in_hull(hull.points, self.item_points)
                cover_set[1]['num_items_inside'] = np.sum(items_inside)
                cover_set[1]['items_inside'] = self.item_points[items_inside]
                cx = np.mean( hull.points[hull.vertices,0] )
                cy = np.mean( hull.points[hull.vertices,1] )
                cover_set[1]['centroid'] = (cx, cy)
            else:
                cover_set[1]['num_items_inside'] = 0
                cover_set[1]['items_inside'] = []

            cover_set[1]['id'] = i
            i = i + 1
            
        self.num_convex_covers = len( self.G_convex_covers )




    def grow_rectangle(self, G, p, sensed_points, origin, scene_map):
#        pp.pprint(p)
        neighbors = G.neighbors(p)
        if(p in sensed_points):
            return sensed_points
        else:
            sensed_points.add( p )
            i = G.node[p]['map_indices'][0]
            j = G.node[p]['map_indices'][1]
            if(distance.euclidean(np.array(p), np.array(origin)) != 0):
                scene_map[i][j] = 10
            G = self.set_map_cell_covered(G, p)
            
#        print("\n\n")
#        print("Scene covered until now:")
#        self.plot_map_dynamic(scene_map)
    
        for neighbor in neighbors:
            if(neighbor in sensed_points):
                continue
            else:
                node = G.node[neighbor]
#                if (node['covered'] == 1):
#                    continue
                # Distance can be chebyshev, cityblock or euclidean
                if (node['invalid'] == 0) and (node['obstacle'] == 0) and (distance.euclidean(np.array(origin), neighbor) <= self.sensor_range) and (poly_tools.obstacle_free(self.obstacle_triags, origin, neighbor) == True):
                    G, scene_map, sensed_points = self.grow_rectangle(G, neighbor, sensed_points, origin, scene_map)
                              
        return G, scene_map, sensed_points




    def calculate_convex_covers_adjacency_matrix(self):
        self.adjacency_mat_covers = np.zeros( (self.num_convex_covers, self.num_convex_covers) )
        for edge in self.G_convex_covers.edges_iter(data=True):
            self.G_map.node[edge[0]]['map_indices']
            i = self.G_convex_covers.node[edge[0]]['id']
            j = self.G_convex_covers.node[edge[1]]['id']
            
            self.adjacency_mat_covers[i][j] = edge[2]['score']
            self.adjacency_mat_covers[j][i] = edge[2]['score']
                
        return self.adjacency_mat_covers



    def get_convex_covers_origin_points(self):
        num_origin_points = len(self.G_convex_covers)
        origin_points = np.zeros( (num_origin_points, 2) )
        for cover_set in self.G_convex_covers.nodes_iter(data=True):
#            print("Convex cover", cover_set[1]['id'])
            origin = cover_set[0]
            origin_points[cover_set[1]['id']][0] = origin[0]
            origin_points[cover_set[1]['id']][1] = origin[1]
            
        return origin_points


    
    def plot_map_stored(self):
        print("\n\n")
        print("Discretized scene")
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.matshow(self.map.T, cmap=plt.cm.coolwarm, origin='lower')
        legend = ['Out of boundary', 'Free space', 'Obstacle']
        cbar = fig.colorbar(cax, ticks=[-1, 0, 1])
        cbar.ax.set_yticklabels(legend)
        plt.show()
    
    

    def plot_map_dynamic(self, scene_map):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.matshow(scene_map.T, cmap=plt.cm.coolwarm, origin='lower')
        legend = ['Out of boundary', 'Free space', 'Obstacle']
        cbar = fig.colorbar(cax, ticks=[-1, 0, 1])
        cbar.ax.set_yticklabels(legend)
        plt.show()
    
    
    
    def plot_scene(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Scene")
        scene_plot.simple_using_A2_format(self.scene_params, ax, fig)
        plt.show()    
    
    
    
    def plot_scene_with_convex_covers(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Scene with convex covers")
        scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
        for cover_set in self.G_convex_covers.nodes_iter(data=True):
            origin = cover_set[0]
#            centroid = cover_set[1]['centroid']
            ax.plot(origin[0], origin[1], 'r^')
            ax.text(origin[0], origin[1], str(cover_set[1]['id']), fontsize=14)
            hull = cover_set[1]['convex_hull']
            if(hull is not None):
                hull_points = cover_set[1]['sensed_points']
                for simplex in hull.simplices:
                    ax.plot(hull_points[simplex, 0], hull_points[simplex, 1], 'g-')
                
        plt.show()


        
        
        
    def plot_adjacency_matrix(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Convex covers adjacency matrix")
        ax.matshow(self.adjacency_mat_covers, cmap=plt.cm.afmhot)
        plt.show()