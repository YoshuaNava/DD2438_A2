import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import time
import math
import numpy as np
from numpy.linalg import norm
from scipy.spatial import distance
from control.matlab import *
from matplotlib import pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib import collections  as mc

from util import polygon_tools as poly_tools
from util import scene_plot
#import rrt_star_kp
#import RRT_STAR_KP



class MultiAgentControl:
    def __init__(self, num_agents, scene_params, dt_sim, F_pattern, motion_model='DP'):
        self.num_agents = num_agents

        self.scene_params = scene_params
        self.boundary_triags = poly_tools._triangulate_polygone(self.scene_params['boundary_polygon'])
        self.obstacle_triags = map(poly_tools._triangulate_polygone, self.scene_params['obstacles'])
        self.obstacle_triags = np.array( list( itertools.chain(*self.obstacle_triags) ) )
        self.v_start = self.scene_params["start_vel"]
        self.v_goal = self.scene_params["goal_vel"]
        self.L_car = self.scene_params["L_car"]
        self.v_max = self.scene_params["v_max"]
        self.a_max = self.scene_params["a_max"]
        self.omega_max = self.scene_params["omega_max"]
        self.phi_max = self.scene_params["phi_max"]
        self.agents_state = np.zeros(self.num_agents)
        
        # The motion model is either DP:Dynamic Point Mass or DC:Dynamic Car
        self.motion_model = motion_model
        
        self.dt_sim = dt_sim
        self.agents_trajs = {}
        for i in range(self.num_agents):
            self.agents_trajs[i] = []
        self.F_pattern = F_pattern
        

        
        

    def set_start_goal_formations(self, F_start, F_goal, v_start=None, v_goal=None):
        self.F_start = F_start
        self.F_goal = F_goal
        if (v_start is None):
            self.v_start = np.zeros(2)
        else:
            self.v_start = v_start
            
        if (v_goal is None):
            self.v_goal = np.zeros(2)
        else:
            self.v_goal = v_goal
            
            
            
    ################ Dynamical system equations ####################
    def A_t(self, x, u):
        # Dynamic point mass
        if(self.motion_model == 'DP'):
            # x = [x, y, v_x, v_y]
            # u = [u_x, u_y]
            return np.matrix([[0, 0, 1, 0], [0, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0]])
            
        # Dynamic car
        if(self.motion_model == 'DC'):
            # x = [x, y, theta, v]
            # u = [u_a, u_phi]
            return np.matrix([[0, 0, -x[3,0]*math.sin(x[2,0]), math.cos(x[2,0])], [0, 0, x[3,0]*math.cos(x[2,0]), math.sin(x[2,0])], [0, 0, 0, 1/self.L_car * math.tan(u[1])], [0, 0, 0, 0]])    
        
        return None



    def B_t(self, x, u):
        # Dynamic point mass
        if(self.motion_model == 'DP'):
            # x = [x, y, v_x, v_y]
            # u = [u_x, u_y]
            return np.matrix([[0, 0], [0, 0], [1, 0], [0, 1 ]])
    
    
        # Dynamic car
        if(self.motion_model == 'DC'):
            # x = [x, y, theta, v]
            # u = [u_a, u_phi]
            return np.matrix([[0, 0], [0, 0], [0, (x[3]/self.L_car) * pow(1/math.cos(u[1]),2) ], [1, 0]])
            
        return None

        

    def create_small_inputs_matrix(self):
        if(self.motion_model == 'DP'):
            u_x = 0.000001
            u_y = 0.000001
            return np.matrix([[u_x], [u_y]])
            
            
        if(self.motion_model == 'DC'):
            u_a0 = 0.000001
            u_phi0 = 0.000001
            return np.matrix([[u_a0], [u_phi0]])
            
        return None
        
        
        
        
    def simulate_controllers(self, X, X_goal, verbose_plots_py=False):
#        v_max = self.scene_params["v_max"]
        traj = []
        
        
        goal_reached = False
        
        
        if (verbose_plots_py):
            fig, ax = plt.subplots()
    
        
        while (goal_reached == False):

            traj.append(np.asarray(X))
            
            u0 = self.create_small_inputs_matrix()
            A = self.A_t(X, u0)
            B = self.B_t(X, u0)
            
            u = self.q1_GtG(X, X_goal, A, B)


            if (verbose_plots_py):
                fig, ax = plt.subplots()
                scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
                ax.plot(X_goal[0,0], X_goal[1,0], 'r^')
                ax.plot(X[0,0], X[1,0], 'ks')
                plt.show()
            

            X_dot = A @ (X-X_goal) + B @ u
            
            X = X + self.dt_sim * X_dot
            
            if(np.linalg.norm(X_goal - X) < 0.4):
                goal_reached = True
                traj.append(np.asarray(X))


        return traj.copy()
        
        
        
        
    def angle_of_vector(self, x):
        return math.atan2(x[1], x[0])
        
        
    

    def build_state_from_pos_vel(self, x, v):
        if(self.motion_model == 'DP'):
            return np.matrix( (x[0], x[1], v[0], v[1]) ).T
            
        if(self.motion_model == 'DC'):
            return np.matrix( (x[0], x[1], self.angle_of_vector(v), norm(v)) ).T

        return None
    


    
    def sign(self, x):
        if x>=0:
            return 1
        else:
            return -1
    
    
    def q1_GtG(self, X, X_goal, A, B):
        print("q1")
        if(self.motion_model == 'DP'):
            Q = np.diag([.1, .1, .1, .1])
            R = np.diag([.1, .1])
            (K, S, eigen) = lqr(A, B, Q, R);
            error_X = X - X_goal
#            print(X)
            u = -K * error_X
            if(norm(u) > self.a_max):
                u = self.a_max * u / norm(u)
        else:
            Q = np.diag([.1, .1, .1, .1])
            R = np.diag([.1, .1])
            try:
                (K, S, eigen) = lqr(A, B, Q, R);
                error_X = X - X_goal
                u = -K * error_X
                if (abs(u[0]) > self.a_max):
                    u[0] = self.sign(u[0]) * self.a_max
    #                print("Acceleration over limit")
                
                if (abs(u[1]) > self.phi_max):
                    u[1] = self.sign(u[1]) * self.phi_max
    #                print("Steering angle over limit")

            except:
                print("LQR exception")
                u = np.zeros( (2,1) )

            
            
        return u




    def q2_OA(self, x, x_goal):
        print("q2")
        if(self.motion_model == 'DP'):
            R = np.matrix( [[0, 1], [-1, 0]] )
            u = np.zeros( (2,1) )
        else:
            u = np.zeros( (2,1) )
            
        return u


    
    
    def run(self):
        for i in range(self.num_agents):
            print("Agent",i)
            X = self.build_state_from_pos_vel(self.F_start[i], self.v_start)

            X_goal = self.build_state_from_pos_vel(self.F_goal[i], self.v_goal)
            traj = self.simulate_controllers(X, X_goal, False)
            self.agents_trajs[i] = self.agents_trajs[i] + traj
            
        print("All controllers converged")
        self.agents_trajs = self.agents_trajs.copy()
        return self.agents_trajs
#            u = self.q1_GtG(X, X_goal)
                        
#            self.q2_OA(X, X_goal)



    def calculate_formation_error(self, F_des, F):
        formation_error = 0
        dist_des = np.zeros(6)
        k = 0
#        print("desired")
        for i in range(len(F_des)):
            for j in range(i, len(F_des)):
                if (i != j):
                    dist_des[k] = norm(F_des[i] - F_des[j])
#                    print(dist_des[k])
                    k = k + 1
        
        dist = np.zeros(6)
        k = 0
#        print("result")
        for i in range(len(F)):
            for j in range(i, len(F)):
                if (i != j):
                    dist[k] = norm(F[i] - F[j])
#                    print(dist[k])
                    k = k + 1
        
#        print("Formation error now")
        for i in range(6):
            formation_error = formation_error + (dist[i] - dist_des[i])**2
#            print(dist[i] - dist_des[i])
#        print(formation_error)
            
        return formation_error



        

    def play_trajectories(self):
        t = 0
        step = 0
        all_goals_reached = False
        agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
        accum_formation_error = 0
        F = np.zeros( (self.num_agents, 2) )
        while(all_goals_reached == False):
            fig = plt.figure()
            ax = fig.add_subplot(111)
            scene_plot.partial_using_T5_format(self.scene_params, ax, fig)
            goals_reached = 0
            for s in range(self.num_agents):
#                print("hey", s)
                traj = self.agents_trajs[s]
#                print(traj)
                traj_lines = []
                if(len(traj) > 0):
                    for i in range(1, len(traj)):
                        X_prev = traj[i-1]
                        X = traj[i-1]
                        line = tuple(( (X_prev[0,0], X_prev[1,0]), (X[0,0], X[1,0]) ))
                        traj_lines.append(line)
                                    
                    line_collection = mc.LineCollection(traj_lines, colors=agent_color[s], linewidths=2)
                    ax.add_collection(line_collection)
    
                    if(step < len(traj)):
                        pos = self.agents_trajs[s][step]
                        ax.plot(pos[0], pos[1], color='k', marker='^')
                    else:
                        pos = self.agents_trajs[s][len(traj) - 1]
                        ax.plot(pos[0], pos[1], color='k', marker= 's')
                        goals_reached = goals_reached + 1
                        
                    F[s, :] = pos[0:2,0]
                else:
                    goals_reached = goals_reached + 1
            
            x_middle = np.array( (np.mean(F[...,0]), np.mean(F[:,1])) )
            F_des = x_middle + self.F_pattern
            accum_formation_error = accum_formation_error + self.calculate_formation_error(F_des, F)
            
            if(goals_reached < self.num_agents):                
                t = step * self.dt_sim
                step = step + 1
                plt.show()
                print(step)
                print("Time", t, "seconds")
            else:
                all_goals_reached = True
                
#        plt.show()
        return accum_formation_error, t



    def play_trajectories_with_path(self, plot_live=False):
        t = 0
        step = 0
        all_goals_reached = False
        agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
        accum_formation_error = 0
        F = np.zeros( (self.num_agents, 2) )
        if(plot_live == False):
            fig = plt.figure()
            ax = fig.add_subplot(111)
            scene_plot.partial_using_T5_format(self.scene_params, ax, fig)
            for s in range(self.num_agents):
                traj = self.agents_trajs[s]
                traj_lines = []
                if(len(traj) > 0):
                    for i in range(1, len(traj)):
                        X_prev = traj[i-1]
                        X = traj[i]
                        line = tuple(( (X_prev[0,0], X_prev[1,0]), (X[0,0], X[1,0]) ))
                        traj_lines.append(line)
                                    
                    line_collection = mc.LineCollection(traj_lines, colors=agent_color[s], linewidths=2)
                    ax.add_collection(line_collection)
            plt.show()


        else:
            while(all_goals_reached == False):
                fig = plt.figure()
                ax = fig.add_subplot(111)
                scene_plot.partial_using_T5_format(self.scene_params, ax, fig)
                goals_reached = 0
                for s in range(self.num_agents):
    #                print("hey", s)
                    traj = self.agents_trajs[s]
    #                print(traj)
                    traj_lines = []
                    if(len(traj) > 0):
                        for i in range(1, len(traj)):
                            X_prev = traj[i-1]
                            X = traj[i]
                            line = tuple(( (X_prev[0,0], X_prev[1,0]), (X[0,0], X[1,0]) ))
                            traj_lines.append(line)
                                        
                        line_collection = mc.LineCollection(traj_lines, colors=agent_color[s], linewidths=2)
                        ax.add_collection(line_collection)
        
                        if(step < len(traj)):
                            pos = self.agents_trajs[s][step]
                            ax.plot(pos[0], pos[1], color='k', marker='^')
                        else:
                            pos = self.agents_trajs[s][len(traj) - 1]
                            ax.plot(pos[0], pos[1], color='k', marker= 's')
                            goals_reached = goals_reached + 1
                            
                        F[s, :] = pos[0:2,0]
                    else:
                        goals_reached = goals_reached + 1
                
                x_middle = np.array( (np.mean(F[...,0]), np.mean(F[:,1])) )
                F_des = x_middle + self.F_pattern
                accum_formation_error = accum_formation_error + self.calculate_formation_error(F_des, F)
                
                if(goals_reached < self.num_agents):                
                    t = step * self.dt_sim
                    step = step + 1
                    plt.show()
                    print(step)
                    print("Time", t, "seconds")
                else:
                    all_goals_reached = True
                
        if(plot_live == False):
            plt.show()
                
#        plt.show()
        return accum_formation_error, t





                


                
                