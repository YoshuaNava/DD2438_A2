import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import time
import math
import random
import pprint as pp
import numpy as np
from scipy.spatial.distance import cdist
from matplotlib import pyplot as plt
from scipy.spatial import distance
from scipy.spatial import ConvexHull
import networkx as nx

from util import scene_plot
from util import polygon_tools as poly_tools




class ConvexCovers:
    
    def __init__(self, scene_params):
        self.scene_params = scene_params
        
        self.start_points = self.scene_params["start_points"]
        self.goal_points = self.scene_params["goal_points"]
        self.boundary_triags = poly_tools._triangulate_polygone(self.scene_params['boundary_polygon'])
        self.obstacle_triags = map(poly_tools._triangulate_polygone, self.scene_params['obstacles'])
        self.obstacle_triags = np.array( list( itertools.chain(*self.obstacle_triags) ) )
        self.sensor_range = self.scene_params["sensor_range"]
        self.sigma_sampling = np.diag([self.sensor_range, self.sensor_range])
        
        self.graph = nx.Graph()
        self.num_convex_covers = 0
        self.obstacle_penalty = 10
#        self.obstacle_penalty = 1
        self.xy_dict = {}
        self.item_points = np.zeros( (1, 2) )

        
            
            

    def load_scene_item_points(self):
        self.item_points = self.scene_params["item_points"].copy()        
        self.num_item_points = self.scene_params["num_item_points"]
        self.all_item_points_idx = set( np.arange(self.num_item_points).flatten() )



    def create_points_dict(self):
        for i in range ( len(self.start_points) ):
            point = (self.start_points[i][0], self.start_points[i][1])
            self.xy_dict[point] = {'point_type': 'start', 'covered': False, 'idx':i}
        for i in range ( len(self.goal_points) ):
            point = (self.goal_points[i][0], self.goal_points[i][1])
            self.xy_dict[point] = {'point_type': 'goal', 'covered': False, 'idx':i}
            
        for i in range ( len(self.item_points) ):
            point = (self.item_points[i][0], self.item_points[i][1])
            self.xy_dict[point] = {'point_type': 'item', 'covered': False, 'idx':i}

    
    
    
    def find_uncovered_point(self):
        for point in self.xy_dict:
            if(self.xy_dict[point]['covered'] == False) and (self.xy_dict[point]['point_type'] == 'start'):
                return point

        for point in self.xy_dict:
            if(self.xy_dict[point]['covered'] == False) and (self.xy_dict[point]['point_type'] == 'goal'):
                return point

        if(self.sensor_range == 0):
            for point in self.xy_dict:
                if(self.xy_dict[point]['covered'] == False) and (self.xy_dict[point]['point_type'] == 'item'):
                    return point

        return None
                
    
    
    
    
    def set_point_covered(self, point):
        self.xy_dict[point]['covered'] = True
        
        
        
        

    def set_items_covered(self, items_inside_range):
        for i in range( len(items_inside_range) ):
            item = (items_inside_range[i,0], items_inside_range[i,1])
            if(self.xy_dict[item]['point_type'] == 'item'):
                self.xy_dict[item]['covered'] = True    




    def find_items_inside_circular_range(self, center_point):
        items_inside = []
        coverage = cdist(np.atleast_2d(center_point), self.item_points)[0]
        
        items_feasible_idx = np.where(coverage <= self.sensor_range)[0]
#        print(coverage)
#        print(items_feasible_idx)
        for i in range( len(items_feasible_idx) ):
            item = self.item_points[ items_feasible_idx[i] ] 
            free_path = poly_tools.obstacle_free_items(self.obstacle_triags, self.boundary_triags, np.array(center_point), item)
            
            if(free_path == True):
                item_idx = self.xy_dict[ tuple(item) ]['idx']
                items_inside.append(item_idx)

        return np.array(items_inside)





    def sample_free(self):
        sizes = np.zeros( self.boundary_triags.shape[0] )

        for i in range( self.boundary_triags.shape[0] ):
            sizes[i] = poly_tools._triag_area(self.boundary_triags[i, 0], self.boundary_triags[i, 1], self.boundary_triags[i, 2])

        while True:
            triangle_choice = np.argmax( np.random.multinomial(1, sizes/np.sum(sizes)) )

            # print(triangle_choice)

            a = self.boundary_triags[triangle_choice, 0]
            b = self.boundary_triags[triangle_choice, 1]
            c = self.boundary_triags[triangle_choice, 2]


            rand = np.random.rand(2)

            AB = b-a
            AC = c-b

            p = rand[0] * AB + rand[1] * AC

            x1 = a + p

            if not poly_tools.point_in_triangle(x1, a, b, c):
                x1 = a + AB + AC - p

            no_obstacle = True

            for t in range( self.obstacle_triags.shape[0] ):
                if poly_tools.point_in_triangle(x1, self.obstacle_triags[t, 0], self.obstacle_triags[t, 1], self.obstacle_triags[t, 2]):
                    no_obstacle = False
                    break

            if no_obstacle:
                return tuple(x1)




    def sample_free_gaussian(self, uncovered_items_idx):
        num_items = uncovered_items_idx.shape[0]
        centroids = self.item_points[uncovered_items_idx]
                
        num_centroids = num_items
        while True:
            rand_index = random.randint(0, num_centroids-1)
            x1 = np.random.multivariate_normal(centroids[rand_index], self.sigma_sampling, 1)[0]

            no_obstacle = not poly_tools.check_point_collision(self.obstacle_triags, self.boundary_triags, x1)
            if no_obstacle:
                return tuple(x1)



    def build_convex_hull(self, cover_origin, items_inside_range):
        if(len(items_inside_range) > 3):
            try:
                hull = ConvexHull( items_inside_range )
                cx = np.mean( hull.points[hull.vertices,0] )
                cy = np.mean( hull.points[hull.vertices,1] )
                centroid = (cx, cy)
            except:
                hull = None
                centroid = (cover_origin[0], cover_origin[1])

        else:
            hull = None
            centroid = (cover_origin[0], cover_origin[1])
            
        return hull, centroid





    def generate_convex_cover(self):
        all_covered = False
        covered_items_idx = set()
        
        id_cc = 0
        while (all_covered == False):
            point = self.find_uncovered_point()
            
            if(point is None):
                rand_point = True
                uncovered_items_idx = np.array( list( self.all_item_points_idx.difference( covered_items_idx ) ) )
#                point = self.sample_free()
                point = self.sample_free_gaussian(uncovered_items_idx)
            else:
                rand_point = False
                self.set_point_covered(point)
                

            items_sensed_idx = self.find_items_inside_circular_range(point)
            if(len(items_sensed_idx) > 0):
                items_inside_range = self.item_points[ items_sensed_idx ]
                self.set_items_covered(items_inside_range)
            else:
                items_inside_range = []
            
            covered_items_idx = covered_items_idx.union( items_sensed_idx.flatten() )
    
            hull, centroid = self.build_convex_hull(point, items_inside_range)
                
            if(rand_point == True):
                self.graph.add_node( point, id=id_cc, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type='rand', centroid=point )
            else:
                self.graph.add_node( point, id=id_cc, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type=self.xy_dict[point]['point_type'], centroid=point )
                
            id_cc = id_cc + 1
            
            print("Item points covered", len(covered_items_idx))
            if(len(covered_items_idx) == self.num_item_points):
                all_covered = True

        self.num_convex_covers = len( self.graph )






    def greedy_generate_convex_cover(self, max_num_cc=200):
        all_covered = False
        covered_items_idx = set()
        
        id_cc = 0
        while (all_covered == False) or (id_cc < max_num_cc):
            point = self.find_uncovered_point()
            
            if(point is None):
                rand_point = True
#                uncovered_items_idx = np.array( list( self.all_item_points_idx.difference( covered_items_idx ) ) )
                point = self.sample_free()
#                point = self.sample_free_gaussian(uncovered_items_idx)
            else:
                rand_point = False
                self.set_point_covered(point)
                

            items_sensed_idx = self.find_items_inside_circular_range(point)
            items_inside_range = self.item_points[ items_sensed_idx ]
#            self.set_items_covered(items_inside_range)
            
            covered_items_idx = covered_items_idx.union( items_sensed_idx.flatten() )
    
            hull, centroid = self.build_convex_hull(point, items_inside_range)
                
            if(rand_point == True):
                self.graph.add_node( point, id=id_cc, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type='rand', centroid=point )
            else:
                self.graph.add_node( point, id=id_cc, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type=self.xy_dict[point]['point_type'], centroid=point )
                
            id_cc = id_cc + 1
            
            print("Iteration", id_cc)
            if(len(covered_items_idx) == self.num_item_points):
                all_covered = True

            
        self.num_convex_covers = len( self.graph )






    def min_set_cover(self):
        universe = set( np.arange(self.num_item_points).flatten() )
        uncovered = universe.copy()
        min_set_convex_covers = nx.Graph()
        G_nodes = self.graph.nodes(data=True)
        convex_covers_dict = {}
        id_cc = 0
        
        if(len(self.start_points) > 0):
            for i in range(len(self.start_points)):
                node = self.graph.node[ tuple(self.start_points[i]) ]
                min_set_convex_covers.add_node( tuple(self.start_points[i]), id=id_cc, num_sensed_points=node['num_sensed_points'], sensed_points=node['sensed_points'], convex_hull=node['convex_hull'], point_type=node['point_type'], centroid = node['centroid'] )
                sensed_points = set( node['sensed_points'].flatten() )
                uncovered = uncovered.difference( sensed_points )
                id_cc = id_cc + 1

        if(len(self.goal_points) > 0):
            for i in range(len(self.goal_points)):
                node = self.graph.node[ tuple(self.goal_points[i]) ]
                min_set_convex_covers.add_node( tuple(self.goal_points[i]), id=id_cc, num_sensed_points=node['num_sensed_points'], sensed_points=node['sensed_points'], convex_hull=node['convex_hull'], point_type=node['point_type'], centroid = node['centroid'] )
                sensed_points = set( node['sensed_points'].flatten() )
                uncovered = uncovered.difference( sensed_points )                
                id_cc = id_cc + 1
                
                
        for node in sorted(G_nodes, key=(lambda node:node[1]['num_sensed_points']), reverse=True):
#            convex_covers_dict[node[1]['id']] = {'point':node[0], 'num_sensed_points':node[1]['num_sensed_points'], 'sensed_points':node[1]['sensed_points'], 'point_type':node[1]['point_type']}
            sensed_points = set( node[1]['sensed_points'].flatten() )
            diff = uncovered.difference( sensed_points )
            if(len(diff) < len(uncovered)):
                min_set_convex_covers.add_node( node[0], id=id_cc, num_sensed_points=node[1]['num_sensed_points'], sensed_points=node[1]['sensed_points'], convex_hull=node[1]['convex_hull'], point_type=node[1]['point_type'], centroid = node[1]['centroid'] )
#                print("\n hola")
#                print(sensed_points)
#                print(len(sensed_points))
#                print(diff)
#                print(len(diff))
#                print(uncovered)
#                print(len(uncovered))
                uncovered = diff
                id_cc = id_cc + 1
                
                if(len(uncovered) == 0):
                    break
                
        self.graph = min_set_convex_covers.copy()
        self.num_convex_covers = len( self.graph )
            
            
        
    
        





    def calculate_convex_covers_adjacency_matrix(self, use_obstacle_penalty = False):                        
        # Find neighboring distances between convex covers
        for cover_set in self.graph.nodes_iter(data=True):
            for other_cover in self.graph.nodes_iter(data=True):
                covers_distance = distance.euclidean(cover_set[0], other_cover[0])
                
                free_path = poly_tools.obstacle_free(self.obstacle_triags, cover_set[0], other_cover[0])
                if(covers_distance != 0): #and free_path == True: # and (covers_distance <= 2*self.sensor_range):
                    if free_path:
                        self.graph.add_edge(cover_set[0], other_cover[0], score=covers_distance)
                    else:
                        if(use_obstacle_penalty == True):
                            self.graph.add_edge(cover_set[0], other_cover[0], score=covers_distance * self.obstacle_penalty)
                        else:
                            self.graph.add_edge(cover_set[0], other_cover[0], score=covers_distance)
                else:
                    self.graph.add_edge(cover_set[0], other_cover[0], score=0)
            
        
        self.adjacency_mat_covers = np.zeros( (self.num_convex_covers, self.num_convex_covers) )
        for edge in self.graph.edges_iter(data=True):
#            self.G_map.node[edge[0]]['map_indices']
            i = self.graph.node[edge[0]]['id']
            j = self.graph.node[edge[1]]['id']
            
            self.adjacency_mat_covers[i][j] = edge[2]['score']
            self.adjacency_mat_covers[j][i] = edge[2]['score']
                
        return self.adjacency_mat_covers



    def get_convex_covers_origin_points(self):
        num_origin_points = len(self.graph)
        origin_points = np.zeros( (num_origin_points, 2) )
        for cover_set in self.graph.nodes_iter(data=True):
#            print("Convex cover", cover_set[1]['id'])
            origin = cover_set[0]
            origin_points[cover_set[1]['id']][0] = origin[0]
            origin_points[cover_set[1]['id']][1] = origin[1]
            
        return origin_points



    def get_convex_covers_centroids(self):
        num_origin_points = len(self.graph)
        centroids = np.zeros( (num_origin_points, 2) )
        for cover_set in self.graph.nodes_iter(data=True):
#            print("Convex cover", cover_set[1]['id'])
            if(cover_set[1]['point_type'] == 'item'):
                centroid = cover_set[1]['centroid']
                centroids[cover_set[1]['id']][0] = centroid[0]
                centroids[cover_set[1]['id']][1] = centroid[1]                
            else:
                origin = cover_set[0]
                centroids[cover_set[1]['id']][0] = origin[0]
                centroids[cover_set[1]['id']][1] = origin[1]
            
        return centroids
    
    
    
    
    def plot_scene(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Scene")
        scene_plot.simple_using_A2_format(self.scene_params, ax, fig)
        plt.show()
    
    
    
    
    def plot_scene_with_convex_covers(self, plot_items=False):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Scene with convex covers")
        scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
        
        
        for cover_set in self.graph.nodes_iter(data=True):
            origin = cover_set[0]
            centroid = cover_set[1]['centroid']
            ax.plot(centroid[0], centroid[1], 'bs')
            ax.plot(origin[0], origin[1], 'r^')
            ax.text(origin[0], origin[1], str(cover_set[1]['id']), fontsize=14)
            hull = cover_set[1]['convex_hull']
            item_points_idx = cover_set[1]['sensed_points']
            if(len(item_points_idx) > 0):
                hull_points = self.item_points[ item_points_idx ]
                if(hull is not None):
                    for simplex in hull.simplices:
                        ax.plot(hull_points[simplex, 0], hull_points[simplex, 1], 'g-')
    
                if(plot_items):
                    for point_idx in range(0, len(hull_points) ):
                        color = np.random.rand(3,1)
                        ax.plot(hull_points[point_idx][0], hull_points[point_idx][1], c=color, marker='x')
                
        plt.show()


 
        
        
    def plot_adjacency_matrix(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Convex covers adjacency matrix")
        ax.matshow(self.adjacency_mat_covers, cmap=plt.cm.afmhot)
        plt.show()