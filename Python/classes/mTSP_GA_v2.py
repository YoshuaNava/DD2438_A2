import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import time
import math
import pprint as pp
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.cm as cmx
import matplotlib.colors as colors
from scipy.spatial import distance
from scipy.spatial import ConvexHull
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform
import networkx as nx

from util import scene_plot
from util import polygon_tools as poly_tools



"""
Inputs
- XY (float) is an Nx2 matrix of city locations, where N is the number of cities
- DMAT (float) is an NxN matrix of city-to-city distances or costs
- Nagents (scalar integer) is the number of agents to visit the cities
- MINTOUR (scalar integer) is the minimum tour length for any of the agents, NOT including the start/end point
- POPSIZE (scalar integer) is the size of the population (should be divisible by 8)
- NUMITER (scalar integer) is the number of desired iterations for the algorithm to run

Output:
- RESULTSTRUCT (structure) with the following fields
- OPTROUTE (integer array) is the best route found by the algorithm
- OPTBREAK (integer array) is the list of route break points (these specify the indices into the route used to obtain the individual salesman routes)
- MINDIST (scalar float) is the total distance traveled by the agents
"""



class mTSP_GA:
    
    def __init__(self, xy, num_agents, min_tour, pop_size, num_iter, alpha, dmat=None, scene_params=None):
        self.xy = xy
        self.num_agents = num_agents
        self.min_tour = min_tour
        self.pop_size = pop_size
        self.num_iter = num_iter
        if scene_params is not None:
            self.scene_params = scene_params
            self.start_points = self.scene_params["start_points"]
            self.start_indices = np.zeros(len(self.start_points), dtype=np.int32)
            for i in range(len(self.start_points)):
                self.start_indices[i] = np.where( np.all(self.xy == self.start_points[i], axis=1)  )[0][0]
            self.goal_points = self.scene_params["goal_points"]
            self.goal_indices = np.zeros(len(self.goal_points), dtype=np.int32)
            for i in range(len(self.goal_points)):
                self.goal_indices[i] = np.where( np.all(self.xy == self.goal_points[i], axis=1)  )[0][0]            
        else:
            self.scene_params = None
            self.start_points = []
            self.start_indices = []
            self.goal_points = []
            self.goal_indices = []
            
        
        # Number of cities
        [self.num_cities, _] = np.shape(xy)
        if dmat is None:
            self.dmat = squareform( pdist( self.xy ) )
        else:
            self.dmat = dmat
        
        #Initialization of route break point
        self.num_breaks = self.num_agents - 1
        self.dof = self.num_cities - self.min_tour * self.num_agents          #degrees of freedom
        addto = np.ones(self.dof+1)
        for k in range (1, self.num_breaks):        
            addto = np.cumsum(addto)
        self.cum_prob = np.cumsum(addto) / np.sum(addto)
        
        # Initialization of Population
        self.pop_route = np.zeros( (self.pop_size, self.num_cities), dtype=np.int32 )
        self.pop_breaks = np.zeros( (self.pop_size, self.num_breaks), dtype=np.int32 )
        self.pop_route[0,:] = np.arange(self.num_cities)
        self.pop_breaks[0,:] = self.rand_breaks()
        
        for itr in range (0, self.pop_size):
            self.pop_route[itr,:] = np.random.permutation(self.num_cities)
            self.pop_breaks[itr,:] = self.rand_breaks()
            self.pop_route[itr,:] = self.validate_route_start_goal(self.pop_route[itr,:], self.pop_breaks[itr,:])
        
        self.opt_route = self.pop_route[0, :]
        self.opt_breaks = self.pop_breaks[0, :]
        self.opt_rng = np.transpose( [np.append(0, self.opt_breaks), np.append(self.opt_breaks-1, self.num_cities-1)] )
        # The Genetic Algorithm Initialization
        self.global_min = math.inf
        self.pop_fitness = np.zeros( self.pop_size )
        self.fitness_history = np.zeros( self.num_iter )
        self.tmp_pop_route = np.zeros( (8, self.num_cities ), dtype=np.int32 )
        self.tmp_pop_breaks = np.zeros( (8, self.num_breaks), dtype=np.int32 )
        self.new_pop_route = np.zeros( (self.pop_size, self.num_cities), dtype=np.int32 )
        self.new_pop_breaks = np.zeros( (self.pop_size, self.num_breaks), dtype=np.int32 )

        # Color map for plotting agents        
        self.cmap = self.get_cmap(self.num_agents)            

        self.alpha = alpha
        self.start_goal_penalty = 1000000
        
        
        
        
        
    def set_initial_route(self, route, breaks):
        self.route = self.validate_route_start_goal(route, breaks)
        for itr in range (0, math.floor(self.pop_size/2)):
            self.pop_route[itr,:] = route.copy()
            self.pop_breaks[itr,:] = tuple(breaks.copy())
        self.opt_route = self.pop_route[0, :].copy()
        self.opt_breaks = self.pop_breaks[0, :].copy()
        self.global_min = self.calculate_fitness_paperUGV(route, breaks)


        

    "----------------------------------Random Breaks funtion-----------------------"
    # Function to generate random set of break points
    def rand_breaks(self):
        if (self.min_tour == 0):  # No Constraints on Breaks
            tmp_breaks = np.random.permutation(self.num_cities-1)
            breaks = np.sort( tmp_breaks[ np.arange(1,self.num_breaks+1) ] )
        else:
            rand_num = np.random.rand(1)[0]
            tmp = np.nonzero( rand_num < self.cum_prob )[0]
            a_adjust = np.amin( tmp )
            rand_spaces = self.num_breaks * np.random.rand(a_adjust+1)
            spaces = np.floor( rand_spaces )            
            adjust = np.zeros( self.num_breaks )
            
            for kk in range (0, self.num_breaks):
                if (kk == 0):
                    adjust[kk] = np.sum( spaces == kk )
                else:
                    adjust[kk] = np.sum( spaces == kk ) - 1
            
            breaks = self.min_tour * np.arange(1, self.num_breaks+1) + np.cumsum(adjust)
#            print("hej")
#            print(self.cum_prob)
#            print("random number", rand_num)
#            print("tmp",tmp)
#            print("a_adjust",a_adjust)
#            print("rand_spaces",rand_spaces)
#            print("spaces",spaces)
#            print("adjust",adjust)
#            print("cumsum adjust",np.cumsum(adjust))
#            print("min_tour",self.min_tour)
#            print("breaks",breaks)
        return breaks




    def calculate_paths_length(self, route, breaks, rng):
        paths_length = np.zeros( self.num_agents )
        
        for s in range (0, self.num_agents):
            # Distance between the starting and ending point of the path
#            paths_length[s] = paths_length[s] + self.dmat[ int(route[ int(rng[s,1]) ]) , int(route[ int(rng[s,0]) ]) ]
            
            for k in range ( int(rng[s,0]), int(rng[s, 1]) ):
                paths_length[s] = paths_length[s] + self.dmat[ int(route[k]), int(route[k+1]) ]
                paths_length[s] = paths_length[s] + self.dmat[ int(route[ k+1 ]) , int(route[ int(rng[s,0]) ]) ]
                

        return paths_length




    def validate_route_start_goal(self, route, breaks):
        rng = np.transpose( [np.append(0, breaks), np.append(breaks-1, self.num_cities-1)] )

        if(len(self.start_points) > 0):
            for s in range(self.num_agents):
                curr_start_index = route[rng[s,0]]
                if (curr_start_index != self.start_indices[s]):
                    pos_start = np.where( route == self.start_indices[s] )[0][0]
                    route[pos_start] = curr_start_index
                    route[rng[s,0]] = self.start_indices[s]
        
        if(len(self.goal_points) > 0):
            for s in range(self.num_agents):
                curr_goal_index = route[rng[s,1]]
                if (curr_goal_index != self.goal_indices[s]):
                    pos_goal = np.where( route == self.goal_indices[s] )[0][0]
                    route[pos_goal] = curr_goal_index
                    route[rng[s,1]] = self.goal_indices[s]
                    
                    
                
        return route





    # This distance function can take into account both distance covered and time taken
    def calculate_fitness_paperUGV(self, route, breaks):
        fitness = 0
        rng = np.transpose( [np.append(0, breaks), np.append(breaks-1, self.num_cities-1)] )
        paths_length = self.calculate_paths_length(route, breaks, rng)
        fitness = self.alpha * np.amax(paths_length) + (1-self.alpha) * np.sum(paths_length)

        return fitness




    def evaluate_population(self):

        for indiv_idx in range (0, self.pop_size):
            indiv_route = self.pop_route[indiv_idx, :].copy()
            indiv_breaks = self.pop_breaks[indiv_idx, :].copy()

            fitness = self.calculate_fitness_paperUGV(indiv_route, indiv_breaks)
#            print("fitness", fitness)

            self.pop_fitness[indiv_idx] = fitness





    def find_best_individual(self, itr):
        min_fitness = np.amin(self.pop_fitness)
        min_idx = np.argmin(self.pop_fitness)
        self.fitness_history[itr] = min_fitness
        if (min_fitness < self.global_min):
            print("min_fitness", min_fitness)
            self.global_min = min_fitness
            self.opt_route = self.pop_route[min_idx, :].copy()    # Result - Output
            self.opt_breaks = self.pop_breaks[min_idx, :].copy()   # Result - Output
            self.opt_rng = np.transpose( [np.append(0, self.opt_breaks), np.append(self.opt_breaks-1, self.num_cities-1)] )
            print("Generation: ", itr)
            print("OptRoute: ", self.opt_route)
            print("OptBreak: ", self.opt_breaks)
            self.plot_agents_best_route()
        
        
        
        
    def selection_and_crossover(self):
        random_order = np.random.permutation(self.pop_size)
        
        for indiv_idx in range (8, self.pop_size + 8, 8):
            routes = self.pop_route[random_order[indiv_idx-8: indiv_idx], :].copy()
            breaks = self.pop_breaks[random_order[indiv_idx-8: indiv_idx], :].copy()
            fitness_8 = self.pop_fitness[ random_order[indiv_idx-8: indiv_idx] ].copy()
            idx = np.argmin(fitness_8)
#            min_fitness_8 = np.amin(fitness_8)
            best_of_8route = routes[idx, :]
            best_of_8break = breaks[idx, :]
            # To make sure that I and J are not equal
            route_insertion_points = np.sort( np.ceil( (self.num_cities-1) * np.random.rand(1,2) ) )
            I = int( route_insertion_points[0][0] )
            J = int( route_insertion_points[0][1] )

            # Mutation    
            # Generate 8 new solutions
            for k in range (0, 8):
                self.tmp_pop_route[k,:] = best_of_8route.copy()
                self.tmp_pop_breaks[k,:] = best_of_8break.copy()

                #Crossover
                if (k==1):
                    # Flip
                    self.tmp_pop_route[k, I:J] = self.tmp_pop_route[k, J-1:I-1:-1].copy()
                elif (k==2):
                    # Swap
                    self.tmp_pop_route[k, [I, J]] = self.tmp_pop_route[k, [J, I]].copy()
                elif (k==3):
                    # Slide
                    self.tmp_pop_route[k, I:J] = np.roll(self.tmp_pop_route[k, I:J], 1, axis=0).copy()
#                    self.tmp_pop_route[k, :] = np.random.permutation(self.num_cities)
                elif(k==4):
                    # Modify Breaks
                    self.tmp_pop_breaks[k, :] = self.rand_breaks().copy()
                elif (k==5):
                    # Flip and Modify Breaks 
                    self.tmp_pop_route[k, I:J] = self.tmp_pop_route[k, J-1:I-1:-1].copy()
                    self.tmp_pop_breaks[k, :] = self.rand_breaks().copy()
                elif(k==6):
                    # Swap and Modify Breaks
                    self.tmp_pop_route[k, [I, J]] = self.tmp_pop_route[k, [J, I]].copy()
                    self.tmp_pop_breaks[k,:] = self.rand_breaks().copy()
                elif(k==7):
                    # Slide and Modify Breaks
                    self.tmp_pop_route[k, I:J] = np.roll(self.tmp_pop_route[k, I:J], 1, axis=0).copy()
#                    self.tmp_pop_route[k, :] = np.random.permutation(self.num_cities)
                    self.tmp_pop_breaks[k,:] = self.rand_breaks().copy()
            
            
                self.tmp_pop_route[k,:] = self.validate_route_start_goal(self.tmp_pop_route[k,:], self.tmp_pop_breaks[k,:])
            
            self.new_pop_route[indiv_idx-8 : indiv_idx, :] = self.tmp_pop_route.copy()
            self.new_pop_breaks[indiv_idx-8 : indiv_idx, :] = self.tmp_pop_breaks.copy()
        
        self.pop_route = self.new_pop_route.copy()
        self.pop_breaks = self.new_pop_breaks.copy()
        
        
        



    def run_with_adjacency_matrix(self):
        "----------------------------------Generations Iterations----------------------"
        #Iterations for the GA
        for itr in range (1, self.num_iter):
#            print("Generation: ", itr)
                
            # Evaluate the fitness of the population members
            self.evaluate_population()

            
            # Find the best route in the current population
            self.find_best_individual(itr)
            

            # Genetic Algorithm  - Selection, Crossover and Mutation
            self.selection_and_crossover()





    def build_route_indices_matrix(self, route):
        rim = np.zeros( (len(self.start_points), 2), dtype=np.int32 )
        for s in range( len(self.start_points) ):
            start_index = np.where(route == s)[0][0]
            if (start_index < self.num_cities-1):
                goal_not_found = True
                j = start_index
                while goal_not_found == True:                    
                    if(np.sum(self.xy[route[j+1]] == self.start_points) == 0):
                        j = j + 1
                    else:
                        goal_index = j
                        goal_not_found = False
                        break
                        
                    if j == self.num_cities-1:
                        goal_index = j
                        goal_not_found = False                        
                        break
            else:
                goal_index = start_index
            rim[s, 0] = start_index
            rim[s, 1] = goal_index
            
#        print(route)
#        print(rim)
        return rim






    "----------------------------------Generate color map for plots------------------------"
    def get_cmap(self, N):
        '''Returns a function that maps each index in 0, 1, ... N-1 to a distinct 
        RGB color.'''
        color_norm  = colors.Normalize(vmin=0, vmax=N-1)
        scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='hsv') 
        def map_index_to_rgb_color(index):
            return scalar_map.to_rgba(index)
        return map_index_to_rgb_color




    "----------------------------------Fitness History Plot------------------------"
    def plot_fitness_history(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        t = np.arange(self.num_iter)
        ax.plot(t, self.fitness_history, 'k-')
        plt.show()
    
    
    
        
        
    "----------------------------------Ploting of agents in assignment scene---------------------------"
    def plot_agents_best_route(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        if (self.scene_params is not None):
            scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
        
        total_cities_visited = 0
        
        rng = np.transpose( [np.append(0, self.opt_breaks), np.append(self.opt_breaks-1, self.num_cities-1)] )
#        rng = self.build_route_indices_matrix(self.opt_route)
        
        for i in range(0, self.num_agents):
            agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
#           agent_color = self.cmap(i)
            route_color = agent_color     
            agent_route = self.opt_route[ range(int(rng[i,0]), int(rng[i,1]) + 1) ]
            
            num_points_route = len(agent_route)
            
            for j in range(0, num_points_route):
                pos_agent = self.xy[ int(agent_route[j]) , :]
                if (j < num_points_route-1):
                    pos_agent_next = self.xy[ int(agent_route[j+1]) , :]
                    edge = np.column_stack( (pos_agent, pos_agent_next) ) 
                    ax.plot(edge[0,...], edge[1,...], color = route_color[i])
                ax.plot(pos_agent[0], pos_agent[1], color = agent_color[i], marker = '^')
                ax.text(pos_agent[0], pos_agent[1], str(j), fontsize=14)
                

            total_cities_visited = total_cities_visited + num_points_route
            
        plt.show()
        
        
        
        
    "----------------------------------Ploting of agents in assignment scene---------------------------"
    def plot_agents_route_rim(self, route, rng):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        if (self.scene_params is not None):
            scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
        
        total_cities_visited = 0
        agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
        
        
        for i in range( len(self.start_points) ):
            
    #           agent_color = self.cmap(i)
            route_color = agent_color     
            agent_route = route[ range(int(rng[i,0]), int(rng[i,1]) + 1) ]
            
            num_points_route = len(agent_route)
            
            for j in range(0, num_points_route):
                pos_agent = self.xy[ int(agent_route[j]) , :]
                if (j < num_points_route-1):
                    pos_agent_next = self.xy[ int(agent_route[j+1]) , :]
                    edge = np.column_stack( (pos_agent, pos_agent_next) ) 
                    ax.plot(edge[0,...], edge[1,...], color = route_color[i])
                ax.plot(pos_agent[0], pos_agent[1], color = agent_color[i], marker = '^')
                ax.text(pos_agent[0], pos_agent[1], str(j), fontsize=14)
                
    
            total_cities_visited = total_cities_visited + num_points_route
            
        plt.show()
    
