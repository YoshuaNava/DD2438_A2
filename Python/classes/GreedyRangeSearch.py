import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import time
import math
import random
import pprint as pp
import numpy as np
from scipy.spatial.distance import cdist
from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse
from scipy.spatial import distance
from scipy.spatial import ConvexHull
import networkx as nx

from util import scene_plot
from util import polygon_tools as poly_tools




class GreedyRangeSearch:
    
    def __init__(self, scene_params, use_obstacle_penalty=False):
        self.scene_params = scene_params
        
        self.start_points = self.scene_params["start_points"]
        self.num_start_points = self.scene_params["num_start_pos"]
        self.goal_points = self.scene_params["goal_points"]
        self.num_goal_points = self.scene_params["num_goal_pos"]
        self.boundary_triags = poly_tools._triangulate_polygone(self.scene_params['boundary_polygon'])
        self.obstacle_triags = map(poly_tools._triangulate_polygone, self.scene_params['obstacles'])
        self.obstacle_triags = np.array( list( itertools.chain(*self.obstacle_triags) ) )
        self.sensor_range = self.scene_params["sensor_range"]
        self.sigma_sampling = np.diag([self.sensor_range, self.sensor_range])/2
        
        self.graph = nx.Graph()
        self.num_convex_covers = 0
        self.obstacle_penalty = 10
        self.use_obstacle_penalty = use_obstacle_penalty
        self.xy_dict = {}
        self.dt_sim = 0.2
        self.convergence_thresh = 0.5
        self.item_points = np.zeros( (1, 2) )

        
            
            

    def load_scene_item_points(self):
        self.item_points = self.scene_params["item_points"].copy()        
        self.num_item_points = self.scene_params["num_item_points"]
        self.all_item_points_idx = set( np.arange(self.num_item_points).flatten() )



    def create_points_dict(self):
        for i in range ( len(self.start_points) ):
            point = (self.start_points[i][0], self.start_points[i][1])
            self.xy_dict[point] = {'point_type': 'start', 'covered': False, 'idx':i}
        for i in range ( len(self.goal_points) ):
            point = (self.goal_points[i][0], self.goal_points[i][1])
            self.xy_dict[point] = {'point_type': 'goal', 'covered': False, 'idx':i}
            
        for i in range ( len(self.item_points) ):
            point = (self.item_points[i][0], self.item_points[i][1])
            self.xy_dict[point] = {'point_type': 'item', 'covered': False, 'idx':i}

    
    
    
    def find_uncovered_point(self):
        for point in self.xy_dict:
            if(self.xy_dict[point]['covered'] == False) and (self.xy_dict[point]['point_type'] == 'start'):
                return point

        for point in self.xy_dict:
            if(self.xy_dict[point]['covered'] == False) and (self.xy_dict[point]['point_type'] == 'goal'):
                return point

        if(self.sensor_range == 0):
            for point in self.xy_dict:
                if(self.xy_dict[point]['covered'] == False) and (self.xy_dict[point]['point_type'] == 'item'):
                    return point

        return None
                
    
    
    
    
    def set_point_covered(self, point):
        self.xy_dict[point]['covered'] = True
        
        
        
        

    def set_items_covered(self, items_inside_range):
        for i in range( len(items_inside_range) ):
            item = (items_inside_range[i,0], items_inside_range[i,1])
            if(self.xy_dict[item]['point_type'] == 'item'):
                self.xy_dict[item]['covered'] = True    



    def simulate_simple(self, x_start, x_goal):
#        x_start = waypoints[0]
#        v_max = self.scene_params["v_max"]
        traj = []
        traj.append(x_start)
        
        x = np.array(x_start)
        v = np.zeros(2)
        
        v = (x_goal - x)
        point_reached = False
        while(point_reached == False):
            x = x + v * self.dt_sim
            
            traj.append(x)
                        
            if(np.linalg.norm(x-x_goal) <= self.convergence_thresh):
                point_reached = True
#                    traj.append(x)
                            
        return traj.copy()




    def sample_free(self):
        sizes = np.zeros( self.boundary_triags.shape[0] )

        for i in range( self.boundary_triags.shape[0] ):
            sizes[i] = poly_tools._triag_area(self.boundary_triags[i, 0], self.boundary_triags[i, 1], self.boundary_triags[i, 2])

        while True:
            triangle_choice = np.argmax( np.random.multinomial(1, sizes/np.sum(sizes)) )

            a = self.boundary_triags[triangle_choice, 0]
            b = self.boundary_triags[triangle_choice, 1]
            c = self.boundary_triags[triangle_choice, 2]

            rand = np.random.rand(2)

            AB = b-a
            AC = c-b

            p = rand[0] * AB + rand[1] * AC

            x1 = a + p

            if not poly_tools.point_in_triangle(x1, a, b, c):
                x1 = a + AB + AC - p

            no_obstacle = True

            for t in range( self.obstacle_triags.shape[0] ):
                if poly_tools.point_in_triangle(x1, self.obstacle_triags[t, 0], self.obstacle_triags[t, 1], self.obstacle_triags[t, 2]):
                    no_obstacle = False
                    break

            if no_obstacle:
                return tuple(x1)





    def sample_free_gaussian(self, uncovered_items_idx):
        num_items = uncovered_items_idx.shape[0]
        centroids = self.item_points[uncovered_items_idx]
                
        num_centroids = num_items
        while True:
            rand_index = random.randint(0, num_centroids-1)
            x1 = np.random.multivariate_normal(centroids[rand_index], self.sigma_sampling, 1)[0]

            no_obstacle = not poly_tools.check_point_collision(self.obstacle_triags, self.boundary_triags, x1)
            if no_obstacle:
                return tuple(x1)



    def build_convex_hull(self, cover_origin, items_inside_range):
        if(len(items_inside_range) > 3):
            try:
                hull = ConvexHull( items_inside_range )
                cx = np.mean( hull.points[hull.vertices,0] )
                cy = np.mean( hull.points[hull.vertices,1] )
                centroid = (cx, cy)
            except:
                hull = None
                centroid = (cover_origin[0], cover_origin[1])

        else:
            hull = None
            centroid = (cover_origin[0], cover_origin[1])
            
        return hull, centroid





    def find_items_inside_circular_range(self, center_point):
        items_inside = []
        coverage = cdist(np.atleast_2d(center_point), self.item_points)[0]
        
        items_feasible_idx = np.where(coverage <= self.sensor_range)[0]
#        print(coverage)
#        print(items_feasible_idx)
        for i in range( len(items_feasible_idx) ):
            item = self.item_points[ items_feasible_idx[i] ] 
            free_path = poly_tools.obstacle_free_items(self.obstacle_triags, self.boundary_triags, np.array(center_point), item)
            
            if(free_path == True):
                item_idx = self.xy_dict[ tuple(item) ]['idx']
                items_inside.append(item_idx)

        return np.array(items_inside)





    def find_items_inside_ellipsoidal_range(self, x_start, x_goal):
        points_between = self.simulate_simple(x_start, x_goal)
        items_inside = []

        for j in range(len(points_between)):
            coverage = cdist(np.atleast_2d(points_between[j]), self.item_points)[0]
            items_feasible_idx = np.where(coverage <= self.sensor_range)[0]
            for i in range( len(items_feasible_idx) ):
                item = self.item_points[ items_feasible_idx[i] ] 
                free_path = poly_tools.obstacle_free_items(self.obstacle_triags, self.boundary_triags, np.array(points_between[j]), item)
                
                if(free_path == True):
                    item_idx = self.xy_dict[ tuple(item) ]['idx']
                    items_inside.append(item_idx)

        
        self.plot_scene_with_path_covers(np.array(x_start), np.array(x_goal), points_between, np.array(items_inside))
        return np.array(items_inside)



    def do_greedy_search(self):
        all_covered = False
        covered_items_idx = set()
        G = nx.Graph()
        id_sc = 0
        id_node = 0
        for i in range(self.num_start_points):
            start_point = self.start_points[i]
            self.graph.add_node( tuple(start_point), id=id_node )
            id_node = id_node + 1
            items_sensed_idx = self.find_items_inside_circular_range(start_point)
            if(len(items_sensed_idx) > 0):
                items_inside_range = self.item_points[ items_sensed_idx ]
                self.set_items_covered(items_inside_range)
            else:
                items_inside_range = []
            
            covered_items_idx = covered_items_idx.union( items_sensed_idx.flatten() )
            hull, centroid = self.build_convex_hull(start_point, items_inside_range)
            self.graph.add_edge( tuple(start_point), tuple(start_point), id=id_sc, score=0, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type='start', centroid=start_point)
            id_sc = id_sc + 1


        while (all_covered == False):
            uncovered_items_idx = np.array( list( self.all_item_points_idx.difference( covered_items_idx ) ) )
            rand_point = self.sample_free_gaussian(uncovered_items_idx)
            self.graph.add_node( tuple(rand_point), id=id_node )
            id_node = id_node + 1
            
            G = self.graph.copy()
#            for i in range(self.num_start_points):
#                start_point = self.start_points[i]
            for node in G.nodes_iter(data=True):
                start_point = node[0]
                if(distance.euclidean(rand_point, node[0]) > self.dt_sim):
                    free_path = poly_tools.obstacle_free_items(self.obstacle_triags, self.boundary_triags, start_point, rand_point)
                    if (free_path == True):
                        items_sensed_idx = self.find_items_inside_ellipsoidal_range(start_point, rand_point)
                        if(len(items_sensed_idx) > 0):
                            items_inside_range = self.item_points[ items_sensed_idx ]
                            self.set_items_covered(items_inside_range)
                        else:
                            items_inside_range = []
                        
                        covered_items_idx = covered_items_idx.union( items_sensed_idx.flatten() )
                
                        hull, centroid = self.build_convex_hull(rand_point, items_inside_range)
                        weighted_dist = self.calculate_distance_points(start_point, rand_point)
                        
                        self.graph.add_edge( tuple(start_point), tuple(rand_point), id=id_sc, score=weighted_dist, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type='rand', centroid=rand_point)
                        id_sc = id_sc + 1
#                    print(id_sc)
            
            print("Item points covered", len(covered_items_idx))
            if(len(covered_items_idx) == self.num_item_points):
                all_covered = True

        self.num_set_points = self.graph.number_of_nodes()
        self.num_set_covers = self.graph.number_of_edges()

        print("Adding missing edges to set cover graph")
        id_sc = self.num_set_covers + 1
        for cover_set in self.graph.nodes_iter(data=True):
#            print(cover_set)
            for other_cover in self.graph.nodes_iter(data=True):
                if not self.graph.has_edge(cover_set[0], other_cover[0]):
                    weighted_dist = self.calculate_distance_points(cover_set[0], other_cover[0])
                    
                    items_sensed_idx = self.find_items_inside_ellipsoidal_range(cover_set[0], other_cover[0])
                    if(len(items_sensed_idx) > 0):
                        items_inside_range = self.item_points[ items_sensed_idx ]
                        self.set_items_covered(items_inside_range)
                    else:
                        items_inside_range = []
            
                    hull, centroid = self.build_convex_hull(cover_set[0], items_inside_range)
                    self.graph.add_edge( cover_set[0], other_cover[0], id=id_sc, score=weighted_dist, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type='rand', centroid=cover_set[0])
                    id_sc = id_sc + 1

        self.num_set_points = self.graph.number_of_nodes()
        self.num_set_covers = self.graph.number_of_edges()









    def min_set_cover(self):
        universe = set( np.arange(self.num_item_points).flatten() )
        uncovered = universe.copy()
        min_set_covers = nx.Graph()
        G_edges = self.graph.edges(data=True)
        id_sc = 0
        id_node = 0
        all_covered = False
        
        
        for i in range(self.num_start_points):
            start_point = self.start_points[i]
            min_set_covers.add_node( tuple(start_point), id=id_node )
            id_node = id_node + 1
            items_sensed_idx = self.find_items_inside_circular_range(start_point)
            if(len(items_sensed_idx) > 0):
                items_inside_range = self.item_points[ items_sensed_idx ]
                self.set_items_covered(items_inside_range)
            else:
                items_inside_range = []
            
            uncovered = uncovered.difference( items_sensed_idx.flatten() )
            hull, centroid = self.build_convex_hull(start_point, items_inside_range)
            min_set_covers.add_edge( tuple(start_point), tuple(start_point), id=id_sc, score=0, num_sensed_points=len(items_inside_range), sensed_points=items_sensed_idx, convex_hull=hull, point_type='start', centroid=start_point)
            id_sc = id_sc + 1        
                        
        while(all_covered == False):
            for edge in sorted(G_edges, key=(lambda edge:edge[2]['num_sensed_points']), reverse=True):
#                print(edge[0], edge[1])
                sensed_points = set( edge[2]['sensed_points'].flatten() )
                diff = uncovered.difference( sensed_points )
                print("hej")
                print(len(diff))
                if(len(diff) < len(uncovered)):# and (edge[0] in min_set_covers.nodes()):
                    if(not min_set_covers.has_node(edge[0])):
                        min_set_covers.add_node( tuple(edge[0]), id=id_node )
                        id_node = id_node + 1
                        
                    if(edge[1] not in min_set_covers.nodes()):
                        min_set_covers.add_node( tuple(edge[1]), id=id_node )
                        id_node = id_node + 1
                        
                    min_set_covers.add_edge( edge[0], edge[1], id=id_sc, score=edge[2]['score'], num_sensed_points=edge[2]['num_sensed_points'], sensed_points=edge[2]['sensed_points'], convex_hull=edge[2]['convex_hull'], point_type=edge[2]['point_type'], centroid = edge[2]['centroid'] )
    #                print("\n hola")
    #                print(sensed_points)
    #                print(len(sensed_points))
    #                print(diff)
    #                print(len(diff))
    #                print(uncovered)
    #                print(len(uncovered))
                    uncovered = diff
                    id_sc = id_sc + 1
                    
                    if(len(uncovered) == 0):
                        break
            
#            print("holaaaaaaaaaa")
#            print(len(uncovered))
            if(len(uncovered) == 0):
                all_covered = True
                
                
        print("Original set cover")
        self.plot_scene_with_convex_covers()
        
        self.graph = min_set_covers.copy()
        self.num_set_points = self.graph.number_of_nodes()
        self.num_set_covers = self.graph.number_of_edges()
        
        print("Minimal set cover")
        self.plot_scene_with_convex_covers()
            
        
    
        
    def calculate_distance_points(self, x_start, x_goal):
        weighted_dist = 0
        dist = distance.euclidean(x_start, x_goal)
        
        free_path = poly_tools.obstacle_free(self.obstacle_triags, x_start, x_goal)
        if(dist != 0): #and free_path == True: # and (covers_distance <= 2*self.sensor_range):
            if free_path:
                weighted_dist = dist
            else:
                if(self.use_obstacle_penalty == True):
                    weighted_dist = dist * self.obstacle_penalty
                else:
                    weighted_dist = dist
        else:
            weighted_dist = 0
            
        return weighted_dist





    def calculate_convex_covers_adjacency_matrix(self, use_obstacle_penalty = False):                                            
        print("Filling up distance matrix")
        self.num_set_covers = self.graph.number_of_edges()
        self.adjacency_mat_covers = np.zeros( (self.num_set_points, self.num_set_points) )
        for node in self.graph.nodes_iter(data=True):
            for other_node in self.graph.nodes_iter(data=True):
                i = node[1]['id']
                j = other_node[1]['id']

                if(self.graph.has_edge(node[0], other_node[0])):
                    edge_data = self.graph.get_edge_data(node[0], other_node[0])            
                    self.adjacency_mat_covers[i][j] = edge_data['score']
                    self.adjacency_mat_covers[j][i] = edge_data['score']
                else:
                    self.adjacency_mat_covers[i][j] = 100000
                    self.adjacency_mat_covers[j][i] = 100000

                
        return self.adjacency_mat_covers





    def get_set_covers_points(self):
        num_points = self.graph.number_of_nodes()
        points = np.zeros( (num_points, 2) )
        i = 0
        for node in self.graph.nodes_iter(data=True):
#            print("Convex cover", cover_set[1]['id'])
            points[i, 0] = node[0][0]
            points[i, 1] = node[0][1]
            i = i + 1
#            points[edge[2]['id']][0] = point[0]
#            points[edge[2]['id']][1] = point[1]
            
        return points


    
    
    
    
    def plot_scene(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Scene")
        scene_plot.simple_using_A2_format(self.scene_params, ax, fig)
        plt.show()
    
    
    
    
    def plot_scene_with_convex_covers(self, plot_items=False):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Scene with convex covers")
        scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
        agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
        
        for edge in self.graph.edges_iter(data=True):
            origin = edge[1]
#            print(origin)
            centroid = edge[2]['centroid']
            ax.plot(centroid[0], centroid[1], 'bs')
            if(edge[2]['point_type'] == 'start'):
                ax.plot(origin[0], origin[1], 'r^')
            ax.text(origin[0], origin[1], str(edge[2]['id']), fontsize=14)
            hull = edge[2]['convex_hull']
            item_points_idx = edge[2]['sensed_points']
            if(len(item_points_idx) > 0):
                hull_points = self.item_points[ item_points_idx ]
                if(hull is not None):
                    for simplex in hull.simplices:
                        ax.plot(hull_points[simplex, 0], hull_points[simplex, 1], 'g-')
    
                if(plot_items):
                    for point_idx in range(0, len(hull_points) ):
                        color = np.random.rand(3,1)
                        ax.plot(hull_points[point_idx][0], hull_points[point_idx][1], c=color, marker='x')
                
        plt.show()




    def plot_scene_with_path_covers(self, x_start, x_goal, path, items_inside_idx):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Scene with convex covers")
        scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
        items_inside = self.item_points[items_inside_idx]
        ax.plot(x_start[0], x_start[1], c='r', marker='^')
        ax.plot(x_goal[0], x_goal[1], c='g', marker='s')
        for i in range(items_inside.shape[0]):
            item = items_inside[i]
            ax.plot(item[0], item[1], c='c', marker='x')

        for point in path:
            ax.plot(point[0], point[1], c='k', marker='o')
#            print(point)
        center = (x_start + x_goal)/2
        diff = x_goal-x_start
        angle = math.atan2(diff[1], diff[0]) * 180 / np.pi
        width = distance.euclidean(x_start, x_goal) + self.sensor_range
        height = self.sensor_range*2
        ellipse = Ellipse(center, width, height, angle, fill=False)
        ax.add_patch(ellipse)

        plt.show()
        
        
        
        
    def plot_adjacency_matrix(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        print("Convex covers adjacency matrix")
        ax.matshow(self.adjacency_mat_covers, cmap=plt.cm.afmhot)
        plt.show()