import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import time
import math
import numpy as np
from scipy.spatial import distance
from matplotlib import pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib import collections  as mc

from util import polygon_tools as poly_tools
from util import scene_plot
from classes import  RRT_STAR_KP
#import rrt_star_kp
#import RRT_STAR_KP



class MultiAgentRoute:
    def __init__(self, xy, route, rng, scene_params, dt):
        self.xy = xy
        self.route = route
        self.num_agents = rng.shape[0]
        self.scene_params = scene_params
        self.sensor_range = self.scene_params["sensor_range"]
        self.boundary_triags = poly_tools._triangulate_polygone(self.scene_params['boundary_polygon'])
        self.obstacle_triags = map(poly_tools._triangulate_polygone, self.scene_params['obstacles'])
        self.obstacle_triags = np.array( list( itertools.chain(*self.obstacle_triags) ) )
        self.load_scene_item_points()
        find_optimal = True
        self.rrt_star = RRT_STAR_KP.RRT_STAR_KP(self.scene_params, find_optimal, 1000)
        
        self.dt = dt
        self.rng = rng
        


    def load_scene_item_points(self):
        self.item_points = self.scene_params["item_points"].copy()
        self.num_item_points = self.scene_params["num_item_points"]
        self.all_item_points_idx = set( np.arange(self.num_item_points).flatten() )
        
        



    def run_rrt_between_points(self, start, goal):
        self.rrt_star.set_start_and_goal(start, goal)
        print("RRT* running")
        (V, E) = self.rrt_star.rrt_star()
#        self.rrt_star.plot_tree_and_obstacles(self.scene_params, V, E, self.rrt_star.x_start, self.rrt_star.x_goal)
        V_smooth, E_smooth = self.rrt_star.optimal_smoothed_path(self.scene_params, V, E, self.rrt_star.x_start, self.rrt_star.x_goal, False)
        path = self.rrt_star.extract_ordered_path(V_smooth, E_smooth, self.rrt_star.x_start, self.rrt_star.x_goal)
#        print(path)
#        time.sleep(10)
        
        return path.copy()

    
    
    
    
    def generate_trajs(self):
        self.agents_trajs = {}
        for s in range(self.num_agents):
            traj = []
#            print(self.route)
#            print(self.rng)
            route_indices = self.route[ np.arange(self.rng[s,0], self.rng[s,1]+1) ]
            route_points = self.xy[ route_indices ]
            num_points_route = len(route_points)
            if(num_points_route == 1):
                traj.append(route_points[0])
            else:
                waypoints = []
                for i in range(num_points_route-1):
#                    print("hey", i)
                    start = route_points[i]
                    goal = route_points[i+1]
                    if poly_tools.obstacle_free(self.obstacle_triags, start, goal):
                        waypoints.append(start)
                        if(i == num_points_route-2):
                            waypoints.append(goal)
                    else:
                        path = self.run_rrt_between_points(start, goal)
                        for j in range(len(path)):
                            waypoints.append(path[j,...].copy())

                if(len(waypoints) > 0):
                    traj = self.simulate_simple(waypoints, False)
#                print(traj)
            self.agents_trajs[s] = np.array(traj.copy())
            
        return self.agents_trajs
            
            
    
        
        
    def simulate_simple(self, waypoints, verbose_plots_py=False):
        x_start = waypoints[0]
        v_max = self.scene_params["v_max"]
        traj = []
        traj.append(x_start)
        
        x = np.array(x_start)
        v = np.zeros(2)
        
        num_via_points = len(waypoints)
        
        if (verbose_plots_py):
            fig, ax = plt.subplots()
    
        
        for i in range(1, num_via_points):
            curr_via_point = waypoints[i]
            point_reached = False
            while(point_reached == False):
                v_vec = (curr_via_point - x)
                v = (v_vec / np.linalg.norm(v_vec)) * v_max
                
                x = x + v * self.dt
                
                traj.append(x)
                            
                if(np.linalg.norm(x-curr_via_point) < self.dt):
                    point_reached = True
#                    traj.append(x)
                            
        return traj.copy()
        
        
        
        
        
    def play_trajectories(self):
        from util import scene_plot
        t = 0
        step = 0
        all_goals_reached = False
        agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
        while(all_goals_reached == False):
            fig = plt.figure()
            ax = fig.add_subplot(111)
            scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
            goals_reached = 0
            for s in range(self.num_agents):
#                print("hey", s)
                traj = self.agents_trajs[s]
                traj_lines = []
                if(len(traj) > 0):
                    for i in range(1, len(traj)):
                        line = tuple(( (traj[i-1][0], traj[i-1][1]), (traj[i][0], traj[i][1]) ))
                        traj_lines.append(line)
                                    
                    line_collection = mc.LineCollection(traj_lines, colors=agent_color[s], linewidths=2)
                    ax.add_collection(line_collection)
    
                    if(step < len(traj)):
                        pos = self.agents_trajs[s][step]
                        ax.plot(pos[0], pos[1], color='k', marker='^')
                    else:
                        pos = self.agents_trajs[s][len(traj) - 1]
                        ax.plot(pos[0], pos[1], color='k', marker= 's')
                        goals_reached = goals_reached + 1
                else:
                    goals_reached = goals_reached + 1
            
            
            if(goals_reached < self.num_agents):                
                t = step * self.dt
                step = step + 1
                plt.show()
                print(step)
                print("Time", t, "seconds")
            else:
                all_goals_reached = True





    def find_items_inside_range(self, center_point):
        points_inside = []
        for i in range( len(self.item_points) ):
            radius = distance.euclidean(center_point, self.item_points[i,:])
            free_path = poly_tools.obstacle_free_items(self.obstacle_triags, self.boundary_triags, np.array(center_point), self.item_points[i,:])
            
            if(radius <= self.sensor_range) and (free_path == True):
                item = (self.item_points[i,0], self.item_points[i,1])
#                item_idx = self.xy_dict[ item ]['idx']
                item_idx = i
                points_inside.append(item_idx)

        return np.array(points_inside)




    def play_trajectories_showing_items(self):
        t = 0
        step = 0
        all_goals_reached = False
        agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
        universe = set( np.arange(self.num_item_points).flatten() )
        uncovered = universe.copy()
        while(all_goals_reached == False):
            fig = plt.figure()
            ax = fig.add_subplot(111)
            scene_plot.partial_using_A2_format(self.scene_params, ax, fig)
            goals_reached = 0
            for s in range(self.num_agents):
#                print("hey", s)
                traj = self.agents_trajs[s]
                traj_lines = []
                if(len(traj) > 0):
                    for i in range(1, len(traj)):
                        line = tuple(( (traj[i-1][0], traj[i-1][1]), (traj[i][0], traj[i][1]) ))
                        traj_lines.append(line)
                                    
                    line_collection = mc.LineCollection(traj_lines, colors=agent_color[s], linewidths=2)
                    ax.add_collection(line_collection)
    
                    if(step < len(traj)):
                        pos = self.agents_trajs[s][step]
                        ax.plot(pos[0], pos[1], color='k', marker='^')
                        sensed_items_idx = self.find_items_inside_range(pos)
                        sensed_items = set( sensed_items_idx.flatten() )
                        uncovered = uncovered.difference(sensed_items)
                    else:
                        pos = self.agents_trajs[s][len(traj) - 1]
                        ax.plot(pos[0], pos[1], color='k', marker= 's')
                        goals_reached = goals_reached + 1
                else:
                    goals_reached = goals_reached + 1
            if(len(uncovered) > 0):
                items_to_plot = self.item_points[ np.array(list(uncovered), dtype=np.int32) ]
                for j in range(len(items_to_plot)):
                    ax.plot(items_to_plot[j][0], items_to_plot[j][1], color='c', marker='x')            
        
            if(goals_reached < self.num_agents):                
                t = step * self.dt
                step = step + 1
                plt.show()
                print(step)
                print("Time", t, "seconds")
            else:
                all_goals_reached = True


                
                