import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import math
import random
import time
import itertools
import numpy as np
from scipy.spatial import distance
from matplotlib import pyplot as plt
import matplotlib.cm as colormap
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib import collections  as mc
from util import load_scene
from util import polygon_tools as poly_tools



class RRT_STAR_KP:
    def __init__(self, scene_params, find_optimal=False, num_iter=1500):
        self.num_iter = num_iter
        self.scene_params = scene_params
        self.boundary_triags = poly_tools._triangulate_polygone(self.scene_params['boundary_polygon'])
        self.obstacle_triags = map(poly_tools._triangulate_polygone, self.scene_params['obstacles'])
        self.obstacle_triags = np.array( list( itertools.chain(*self.obstacle_triags) ) )

        self.cost_dict = dict()
        self.parent_dict = dict()

        # Near function constants
        # Calculate mu_X_free, zeta_d and d based on area of free space and are of unit circle
        ############################################
        a = scene_params["boundary_polygon"][0]
        b = scene_params["boundary_polygon"][1]
        c = scene_params["boundary_polygon"][2]
        d = scene_params["boundary_polygon"][3]

        area = ((a[0]*b[1] - a[1]*b[0]) + (b[0]*c[1] - b[1]*c[0]) +
                (c[0]*d[1] - c[1]*d[0]) + (d[0]*a[1] - d[1]*a[0])) / 2
        ############################################
        self.d = int(a.size)
        self.mu_X_free = area
        self.zeta_d = np.pi
        
        self.sigma_sampling = np.diag([2, 2])
        self.min_dist_new_nodes = 1
        self.min_dist_goal = 3
        self.find_optimal = find_optimal



    def set_start(self, start_point):
        self.x_start = tuple(start_point)

    def set_goal(self, goal_point):
        self.x_goal = tuple(goal_point)




    def set_start_and_goal(self, start_point, goal_point):
        self.cost_dict = dict()
        self.parent_dict = dict()

        self.centroids = []
        self.centroids.append(goal_point)
        
        num_obstacles = self.scene_params['num_obstacles']
        for i in range(0, num_obstacles):
            obstacle = self.scene_params['obstacles'][i]
            num_points = obstacle.shape[0]
            for j in range(0, num_points):
                self.centroids.append(obstacle[j])
                
        self.num_centroids = len(self.centroids)

        self.x_start = tuple(start_point)
        self.x_goal = tuple(goal_point)
        
        self.cost_dict[ (self.x_start, self.x_start) ] = 0



    def sample_free(self, i):
        triangles = self.boundary_triags
        obstacles = self.obstacle_triags
        sizes = np.zeros(np.size(triangles, axis=0))

        for i in range(np.size(triangles, axis=0)):
            sizes[i] = poly_tools._triag_area(triangles[i, 0], triangles[i, 1], triangles[i, 2])

        while True:
            triangle_choice = np.argmax(np.random.multinomial(1, sizes/np.sum(sizes)))

            a = triangles[triangle_choice, 0]
            b = triangles[triangle_choice, 1]
            c = triangles[triangle_choice, 2]


            rand = np.random.rand(2)

            AB = b-a
            AC = c-b

            p = rand[0] * AB + rand[1] * AC

            x1 = a + p

            if not poly_tools.point_in_triangle(x1, a, b, c):
                x1 = a + AB + AC - p

            no_obstacle = True

            for t in range(np.size(obstacles, axis=0)):
                if poly_tools.point_in_triangle(x1, obstacles[t, 0], obstacles[t, 1], obstacles[t, 2]):
                    no_obstacle = False
                    break

            if no_obstacle:
                return x1




    def sample_free_gaussian(self):
        while True:
            rand_index = random.randint(0, self.num_centroids-1)
            x1 = np.random.multivariate_normal(self.centroids[rand_index], self.sigma_sampling, 1)[0]

            no_obstacle = not poly_tools.check_point_collision(self.obstacle_triags, self.boundary_triags, x1)
            if no_obstacle:
                return x1



    @staticmethod
    def nearest(V, E, x_rand):
        dist = math.inf

        x_tmp = None

        for x in V:
            new_dist = distance.euclidean(x_rand, x)
            if new_dist < dist:
                x_tmp = x
                dist = new_dist
        return x_tmp, dist


    @staticmethod
    def steer(x_start, x_end, i):
        n = max(10 - (i/100), 1)  # needs to be tuned

        dist = distance.euclidean(x_start, x_end)
        if dist < n:
            return x_end
        else:
            return x_start + (x_end - x_start)*n/dist




    def radius(self, d, mu_X_free, zeta_d, n):
        """
        Function to calculate the radius used in near function.
        """
        return (2 * (1 + (1 / self.d))**(1 / self.d)
                * (self.mu_X_free / self.zeta_d)**(1 / self.d))*(np.log(n)/n)**(1/self.d)


    def near(self, V, E, x, r):
        """
        Checks which points are within a certain radius circle.
        """

        K = set()
        for i in V:
            """
            if np.array_equal(i, x):
                continue
            elif distance.euclidean(i, x) <= r:
                K.add(i)
            """

            dx = np.abs(i[0] - x[0])
            dy = np.abs(i[1] - x[1])

            if np.array_equal(i, x):
                continue
            elif dx + dy <= r:
                K.add(i)
            elif dx > r:
                continue
            elif dy > r:
                continue
            elif dx**2 + dy**2 <= r**2:
                K.add(i)
            else:
                continue
        return K


    @staticmethod
    def line(x_start, x_end):
        """
        Calculates distance (euclidean) between x_start and x_end.
        """
        #return np.linalg.norm(np.matrix(x_start)-np.matrix(x_end))
        return distance.euclidean(x_start, x_end)


    def parent(self, x, E):
        if tuple(x) in self.parent_dict:
            return tuple(self.parent_dict[x])

        if tuple(x) != self.x_start:
            print("No parent!!!!!!!")
#            exit(1)

        return None




    def cost(self, x, tree):
        x = tuple(x)
        c = 0
        child = x
        _parent = self.parent(child, tree)
        while True:
            if _parent == None:
                return c

            c += distance.euclidean(_parent, child)
            child = _parent
            _parent = self.parent(child, tree)





    def rrt_star(self):
        V = set()
        V.add(tuple(self.x_start))
        E = set()
        n = self.num_iter  # number of iterations
        c = 1

        for i in range(n):
#            print("iteration: ", i)
            x_rand = self.sample_free_gaussian()
            x_nearest, dist_nearest = self.nearest(V, E, x_rand)
            x_new = self.steer(x_nearest, x_rand, i)

            if (dist_nearest > self.min_dist_new_nodes) and (poly_tools.obstacle_free(self.obstacle_triags, x_nearest, x_new)) and poly_tools.within_boundary(self.boundary_triags, x_new):
                r_RRT_star = 5  # min(radius(d, mu_X_free, zeta_d, i+1)/100, max(10 - (i/100), 1))
                X_near = self.near(V, E, x_new, r_RRT_star)
                V.add(tuple(x_new))
                x_min = x_nearest

                if (tuple(self.x_start), tuple(x_min)) in self.cost_dict:
                    c_start_min = self.cost_dict[(tuple(self.x_start), tuple(x_min))]
                else:
                    c_start_min = self.cost(x_min, E)
                    self.cost_dict[(tuple(self.x_start), tuple(x_min))] = c_start_min

                c_min = c_start_min + c * self.line(x_nearest, x_new)

                for x_near in X_near:
                    if (tuple(self.x_start), tuple(x_near)) in self.cost_dict:
                        c_start_near = self.cost_dict[(tuple(self.x_start), tuple(x_near))]
                    else:
                        c_start_near = self.cost(x_near, E)
                        self.cost_dict[(tuple(self.x_start), tuple(x_near))] = c_start_near

                    c_new = c_start_near + c * self.line(x_near, x_new)

                    if poly_tools.obstacle_free(self.obstacle_triags, x_near, x_new) and c_new < c_min:
                        x_min = x_near
                        c_min = c_new

                E.add((tuple(x_min), tuple(x_new)))
                self.parent_dict[tuple(x_new)] = tuple(x_min)
                self.cost_dict[(tuple(self.x_start), tuple(x_new))] = c_min

                for x_near in X_near:
                    if (tuple(self.x_start), tuple(x_near)) in self.cost_dict:
                        c_near = self.cost_dict[(tuple(self.x_start), tuple(x_near))]
                    else:
                        c_near = self.cost(x_near, E, self.x_start)
                        self.cost_dict[(tuple(self.x_start), tuple(x_near))] = c_near

                    if (tuple(self.x_start), tuple(x_new)) in self.cost_dict:
                        c_start_new = self.cost_dict[(tuple(self.x_start), tuple(x_new))]
                    else:
                        c_start_new = self.cost(x_new, E)
                        self.cost_dict[(tuple(self.x_start), tuple(x_new))] = c_start_new

                    c_new = c_start_new + c * self.line(x_new, x_near)

                    if poly_tools.obstacle_free(self.obstacle_triags, x_new, x_near) and c_new < c_near:
                        x_parent = self.parent(x_near, E)
                        E.remove((tuple(x_parent), tuple(x_near)))
                        E.add((tuple(x_new), tuple(x_near)))
                        self.parent_dict[tuple(x_near)] = tuple(x_new)
                        self.cost_dict[(tuple(self.x_start), tuple(x_near))] = c_new
                        
         
                if(self.find_optimal == False):
                    if (distance.euclidean(x_new, self.x_goal) < self.min_dist_goal) and poly_tools.obstacle_free_items(self.obstacle_triags, self.boundary_triags, x_new, self.x_goal):
                        E.add((tuple(x_new), tuple(self.x_goal)))
                        V.add(self.x_goal)
                        self.parent_dict[tuple(self.x_goal)] = tuple(x_new)
                        c_goal = c_min + c * self.line(x_new, self.x_goal)
                        self.cost_dict[ (tuple(self.x_start), tuple(self.x_goal)) ] = c_goal
    #                    print("iterations needed to converge to goal: ", i)
                        break
#                else:
#                    print("RRT* iteration: ", i)

        return (V, E)





    def path_between(self, E, x0, x1):        
        edges = set()
        vertices = set()
        child = x1
        _parent = self.parent_dict[ tuple(child) ]
        vertices.add(child)        
        
        while True:
            if _parent == x0:
                edges.add( (tuple(_parent), tuple(child)) )
                vertices.add(_parent)
                return vertices, edges
            if _parent == self.x_start:
                return set(), set()
                
            edges.add( (tuple(_parent), tuple(child)) )
            vertices.add(_parent)
            child = _parent
            _parent = self.parent_dict[ tuple(child) ]





    def path_to_start(self, E, x1):
        edges = set()
        vertices = set()
        child = x1
        if(x1 is self.x_start):
            return vertices, edges
            
        _parent = self.parent_dict[ tuple(child) ]
        vertices.add(child)
        
        while True:
            if _parent == self.x_start:
                edges.add( (tuple(_parent), tuple(child)) )
                vertices.add(_parent)
                return vertices, edges
                
            edges.add( (tuple(_parent), tuple(child)) )
            vertices.add(_parent)
            child = _parent
            _parent = self.parent_dict[ tuple(child) ]
            
            

    def find_child(self, E, x1):
        for (parent, child) in E:
            if parent is x1:
                return child

        return None



    def optimal_path(self, scene, V, E):
        xTup_nearest, dist_nearest = self.nearest(V, E, self.x_goal)
        radius = 5
        XTup_near = self.near(V, E, self.x_goal, radius)
        xTup_min = xTup_nearest
        c_min = self.cost_dict[(tuple(self.x_start), tuple(xTup_min))]
        c = 1
        
        for xTup_near in XTup_near:
            c_near = self.cost_dict[(tuple(self.x_start), tuple(xTup_near))]
                        
            if (c_near < c_min) and poly_tools.obstacle_free(self.obstacle_triags, xTup_near, self.x_goal):
                xTup_min = xTup_near
                c_min = c_near
        
        # Backtrack path
        optimal_V, optimal_E = self.path_to_start(E, xTup_min)
#        traj_goal, t_goal, feasible_goal, overtime_goal, collision_goal, point_reached_goal, dyn_valid_goal = self.simulate_traj(state_min, state_goal, self.dt_sim, self.dt_sampling, self.max_sim_time)
        
        
        if poly_tools.obstacle_free_items(self.obstacle_triags, self.boundary_triags, xTup_min, self.x_goal):
            optimal_E.add((tuple(xTup_min), tuple(self.x_goal)))
            optimal_V.add(tuple(self.x_goal))
    
            c_start_min = self.cost_dict[(tuple(self.x_start), tuple(xTup_min))]        
            c_goal = c_start_min + c * self.line(xTup_min, self.x_goal)            
            self.parent_dict[ tuple(self.x_goal) ] = tuple(xTup_min)            
            self.cost_dict[ (tuple(self.x_start), tuple(self.x_goal)) ] = c_goal
            
            return optimal_V, optimal_E
        else:
            return set(), set()




    def smooth_path(self, scene, V, E, xTup_start, xTup_goal):

        N = 100
        V_smooth = V.copy()
        E_smooth = E.copy()
        

        for i in range(N):
#            print("Smoothing iteration: ", i)
            xTup_rand1, xTup_rand2 = random.sample(V_smooth, 2)       
            cost_x1_start = self.cost_dict[ (tuple(self.x_start), tuple(xTup_rand1) ) ]
            cost_x2_start = self.cost_dict[ (tuple(self.x_start), tuple(xTup_rand2) ) ]           

            if cost_x1_start <= cost_x2_start:
                xTup_first = xTup_rand1
                xTup_second = xTup_rand2

            else:
                xTup_first = xTup_rand2
                xTup_second = xTup_rand1
                     
            curr_nodes, curr_path = self.path_between(E_smooth, xTup_first, xTup_second)

            if poly_tools.obstacle_free(self.obstacle_triags, xTup_first, xTup_second):
                new_edge = (tuple(xTup_first), tuple(xTup_second))
                for edge in curr_path:
                    if edge in E_smooth:
                        E_smooth.remove(edge)
                for node in curr_nodes:
                    if node in V_smooth:
                        V_smooth.remove(node)
                E_smooth.add(new_edge)
                V_smooth.add(xTup_first)
                V_smooth.add(xTup_second)
                self.parent_dict[ tuple(xTup_second) ] = xTup_first



        return V_smooth, E_smooth




    def extract_ordered_path(self, V, E, x_start, x_goal):

        num_points = len(V)
        path = np.zeros((num_points,2))

        i = 0
        for vertex in V:
            path[i, ...] = np.array(vertex)
            i = i + 1

#        path[num_points-1, ...] = x_goal

        for j in range(num_points):
            for i in range(num_points - 1):
                x1 = path[i, ...].copy()
                x2 = path[i+1, ...].copy()
                
                # This can be improved using self.cost_dict
                cost_x1_start = self.cost_dict[ (tuple(self.x_start), tuple(x1)) ]
                cost_x2_start = self.cost_dict[ (tuple(self.x_start), tuple(x2)) ]

                if cost_x2_start < cost_x1_start:
                    path[i, ...] = x2
                    path[i+1, ...] = x1

        return path




    def optimal_smoothed_path(self, scene, V, E, x_start, x_goal, verbose_plots_py=True):
        optimal_V, optimal_E = self.optimal_path(scene, V, E)
        if verbose_plots_py:
            print("Optimal path")
            self.plot_tree_and_obstacles(scene, optimal_V, optimal_E, x_start, x_goal)

        if(self.find_optimal == True):
            V_smooth, E_smooth = self.smooth_path(scene, optimal_V, optimal_E, x_start, x_goal)
    
            if verbose_plots_py:
                print("Path smoothing")
                self.plot_tree_and_obstacles(scene, V_smooth, E_smooth, x_start, x_goal)
    
            return V_smooth, E_smooth
        else:            
            return optimal_V, optimal_E





    def plot_tree_and_obstacles(self, params, V, E, x_start, x_goal):
        fig, ax = plt.subplots()
        boundaries = Polygon(params['boundary_polygon'], fill=False)
        ax.add_patch(boundaries)
    
        num_obstacles = params['num_obstacles']
        patches = []
        for i in range(0, num_obstacles):
            obstacle = Polygon(params['obstacles'][i])
            patches.append(obstacle)
    
        p = PatchCollection(patches, cmap=colormap.jet, alpha=0.5)
        colors = 100*np.random.rand(len(patches))
        p.set_array(np.array(colors))
        ax.add_collection(p)
    
        V_new = V.copy()
        if tuple(x_start) in V_new:
            V_new.remove(tuple(x_start))
    
        vertices = np.array(list(V_new))
    
        c = np.array([(0, 0, 0, 1)])
        lines = []
    
        for e in E:
            lines.append(e)
    
        line_collection = mc.LineCollection(lines, colors=c, linewidths=2)
    #    print(".")
    
        ax.add_collection(line_collection)
        ax.plot(vertices[:, 0], vertices[:, 1], 'gx')
    #    print("..")
        ax.plot(x_start[0], x_start[1], 'r^')
        ax.plot(x_goal[0], x_goal[1], 'ms')
    #    print("...")
        fig.set_size_inches(5, 5.5)
        ax.margins(0.01)
        ax.autoscale()
        plt.show()
    #    print("....")






# Run example

#scene = load_scene.load_from_json("../json_scenes/problem_A4.json")
#find_optimal = True
#rrt = RRT_STAR_KP(scene, find_optimal, 1000)
#
#rrt.set_start_and_goal(scene["start_points"][3], scene["goal_points"][2])
#
#print("Run RRT*")
#start = time.time()
#(V, E) = rrt.rrt_star()
#end = time.time()
#print(end - start, "seconds")
#print("Finished")
#
#rrt.plot_tree_and_obstacles(scene, V, E, rrt.x_start, rrt.x_goal)
#
#V_smooth, E_smooth = rrt.optimal_smoothed_path(scene, V, E, rrt.x_start, rrt.x_goal, False)
#path = rrt.extract_ordered_path(V_smooth, E_smooth, rrt.x_start, rrt.x_goal)
#rrt.plot_tree_and_obstacles(scene, V_smooth, E_smooth, rrt.x_start, rrt.x_goal)
#
