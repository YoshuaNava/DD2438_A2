# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

@author: tanay
"""

import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

from util import load_scene

from classes import ConvexCovers as CC
import networkx as nx


# Very important
sys.setrecursionlimit(15000)

scene_params = load_scene.load_from_json('../json_scenes/problem_B13_new.json')
scene_params["start_points"] = []
scene_params["num_start_pos"] = 0
scene_params["goal_points"] = []
scene_params["num_goal_pos"] = 0
#scene_params["sensor_range"] = 0


convex_covers = CC.ConvexCovers(scene_params)



print("Step1")
convex_covers.load_scene_item_points()
print("Step2")
convex_covers.create_points_dict()
print("Step3")
#use_obstacle_penalty = True
#convex_covers.generate_convex_cover(use_obstacle_penalty)
max_num_cc = 250
convex_covers.greedy_generate_convex_cover(max_num_cc)
print("Step4")
convex_covers.plot_scene()
print("Step5")
convex_covers.plot_scene_with_convex_covers()



G_nodes = convex_covers.graph.nodes(data=True)
G_max_covers = nx.Graph()
num_max_covers = 4

i = 0
# Order convex covers with the most points inside
for node in sorted(G_nodes, key=(lambda node:node[1]['num_sensed_points']), reverse=True):
    print("Convex cover", node[1]['id'], "; Number of items covered", node[1]['num_sensed_points'])
    if(i < num_max_covers):
        G_max_covers.add_node( node[0], id=node[1]['num_sensed_points'], num_sensed_points=node[1]['num_sensed_points'], sensed_points=node[1]['sensed_points'], convex_hull=node[1]['convex_hull'], centroid = node[1]['centroid'])
        i = i + 1



print("\n\n")
print("Maximal convex covers:")
convex_covers.graph = G_max_covers.copy()
G_max_nodes = G_max_covers.nodes(data=True)
covered = set()
for node in G_max_nodes:
    print("Convex cover", node[1]['id'], "; Number of items covered", node[1]['num_sensed_points'])
    sensed_points = set( node[1]['sensed_points'].flatten() )
    covered = covered.union( sensed_points )
    
print("Number of unique points covered:", len(covered))
plot_items = True
convex_covers.plot_scene_with_convex_covers(plot_items)






