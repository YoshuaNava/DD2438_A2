# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

@author: tanay
"""

import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

from util import load_scene

from classes import DiscreteScene as DS
import networkx as nx


# Very important
sys.setrecursionlimit(15000)

scene_params = load_scene.load_from_json('../json_scenes/problem_A12.json')
#scene_params["sensor_range"] = 0

# Maximum resolution for now: 100
x_res = 50
y_res = 50
disc_scene = DS.DiscreteScene(scene_params, x_res, y_res)


disc_scene.discretize_map()
disc_scene.load_scene_item_points()

disc_scene.plot_map_stored()



disc_scene.create_map_graph()
disc_scene.generate_convex_cover()


disc_scene.plot_scene()
disc_scene.plot_scene_with_convex_covers()


G_nodes = disc_scene.G_convex_covers.nodes(data=True)
G_max_covers = nx.Graph()
num_max_covers = len( scene_params["start_points"] )

i = 0
# Order convex covers with the most points inside
for node in sorted(G_nodes, key=(lambda node:node[1]['num_items_inside']), reverse=True):
    print("Convex cover", node[1]['id'], "; Number of items covered", node[1]['num_items_inside'])
    if(i < num_max_covers):
        G_max_covers.add_node( node[0], id=node[1]['num_items_inside'], num_sensed_points=node[1]['num_items_inside'], sensed_points=node[1]['sensed_points'], convex_hull=node[1]['convex_hull'] )
        i = i + 1

disc_scene.G_convex_covers = G_max_covers.copy()
disc_scene.plot_scene_with_convex_covers()






