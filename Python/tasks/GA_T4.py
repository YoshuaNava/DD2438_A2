# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

@author: tanay
"""

import sys
from pathlib import Path 
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import math
import numpy as np
from util import load_scene
from classes import mTSP_GA_v2 as MTSP_GA
from classes import ConvexCovers as CC
from classes import MultiAgentRoute as MAR
from numpy.linalg import norm


# Very important
sys.setrecursionlimit(15000)



scene_params = load_scene.load_from_json('../json_scenes/problem_A4.json')
#scene_params["sensor_range"] = 0

goal_points = scene_params["goal_points"].copy()
scene_params["goal_points"][0] = goal_points[2].copy()
scene_params["goal_points"][1] = goal_points[0].copy()
scene_params["goal_points"][2] = goal_points[3].copy()
scene_params["goal_points"][3] = goal_points[1].copy()


from scipy.spatial.distance import cdist

def closest_point(point, others):
    distances = cdist(np.atleast_2d(point), others)
#    return others[distances.argmin()]
    return np.argmin(distances)


def cluster_points(goal_points, start_points):
    clusters  = []
    for point in goal_points:
#        distance = np.linalg.norm(cover - start_points)  #\
        closest = closest_point(point, start_points)
        clusters.append(closest)

    return np.array(clusters)
 

clusters = cluster_points(np.array(scene_params["goal_points"]), np.array(scene_params["start_points"]))

print(clusters)



convex_covers = CC.ConvexCovers(scene_params)


print("Step0")
convex_covers.plot_scene()
print("Step1")
convex_covers.load_scene_item_points()
print("Step2")
convex_covers.create_points_dict()
print("Step3")
convex_covers.generate_convex_cover()
print("Step4")
convex_covers.plot_scene_with_convex_covers()
print("Step5")
convex_covers.min_set_cover()
print("Step6")
convex_covers.plot_scene_with_convex_covers()
print("Step7")
use_obstacle_penalty = False
adj_matrix = convex_covers.calculate_convex_covers_adjacency_matrix(use_obstacle_penalty)
print("Step8")
origin_points = convex_covers.get_convex_covers_centroids()
print("Step9")
convex_covers.plot_adjacency_matrix()


num_points = len(origin_points)



print("Step9")
alpha = 1
num_salesmen = 4
#min_tour = max(1, math.floor(num_points/num_salesmen))
min_tour = 1
pop_size = 80
num_iter = 5000
mtsp_ga = MTSP_GA.mTSP_GA(origin_points, num_salesmen, min_tour, pop_size, num_iter, alpha, adj_matrix, scene_params)



mtsp_ga.run_with_adjacency_matrix()


print("Algorithm finished running")
print("Results")
print("Optimal Breaks:")
print(mtsp_ga.opt_breaks)
print("Optimal Route (list of cities):")
print(mtsp_ga.opt_route)
print("Global Minimum")
print(mtsp_ga.global_min)
mtsp_ga.plot_agents_best_route()
mtsp_ga.plot_fitness_history()



rim = mtsp_ga.build_route_indices_matrix(mtsp_ga.opt_route)
#print(rim)

mtsp_ga.plot_agents_route_rim(mtsp_ga.opt_route, rim)

dt = 0.2
multi_agent_route = MAR.MultiAgentRoute(origin_points, mtsp_ga.opt_route, rim, scene_params, dt)
agents_trajs = multi_agent_route.generate_trajs()
multi_agent_route.play_trajectories()