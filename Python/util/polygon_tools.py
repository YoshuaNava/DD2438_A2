import math
from scipy.spatial import Delaunay
from numpy.linalg import norm
import numpy as np
from matplotlib import pyplot as plt
import shapely.geometry

#import locale
#locale.setlocale(locale.LC_NUMERIC, 'C')
#from numba import jit


#@jit(nopython=True)
def sign(p1, p2, p3):
    return (p1[0] - p3[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p3[1])


#@jit(nopython=True)
def point_in_triangle(pt, v1, v2, v3):
    """
    Checks if a point pt is within a triangle v1, v2, v3
    """
    b1 = sign(pt, v1, v2) < 0
    b2 = sign(pt, v2, v3) < 0
    b3 = sign(pt, v3, v1) < 0

    return (b1 == b2) and (b2 == b3)


def triangleArea(x1, y1, x2, y2, x3, y3):
    return abs((x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))/2.0);


#@jit(nopython=True)
def pointInTriangle(P, A, B, C):
    """
    Checks if a point pt is within a triangle v1, v2, v3
    """
    ## Calculate area of triangle ABC 
    ABC = triangleArea(A[0], A[1], B[0], B[1], C[0], C[1]);
    
    ## Calculate area of triangle PBC 
    PBC = triangleArea(P[0], P[1], B[0], B[1], C[0], C[1]);

    ## Calculate area of triangle PAC 
    PAC = triangleArea(P[0], P[1], A[0], A[1], C[0], C[1]);

    ## Calculate area of triangle PAB 
    PAB = triangleArea(P[0], P[1], A[0], A[1], B[0], B[1]);

    ## Check if sum of PAC, PBC and PAB is same as ABC
    return (ABC == PAC + PBC + PAB);


#@jit(nopython=True)
def _triag_area(a, b, c):
    """
    Returns the area of a triangel between a, b, c
    """
    return 0.5 * norm(np.cross(b-a, c-a))

#@jit(nopython=True)
def _triangulate_polygone(poly):
    """
    Decomposes a polygone into triangles
    """
    tri = Delaunay(poly)

    return poly[tri.simplices]


#@jit(nopython=True)
def line_intersect2(p, q, a, b):
    denominator = (p[0] - q[0]) * (a[1] - b[1]) - (p[1] - q[1]) * (a[0] - b[0]);
    if (denominator == 0):
        return False
        
    point = [((p[0]*q[1] - p[1]*q[0]) * (a[0] - b[0]) - (p[0] - q[0]) * (a[0]*b[1] - a[1]*b[0])),
             ((p[0]*q[1] - p[1]*q[0]) * (a[1] - b[1]) - (p[1] - q[1]) * (a[0]*b[1] - a[1]*b[0]))] / denominator
    
    a_point = norm(a - point)
    point_b = norm(point - b)
    a_b = norm(a-b)
    
    p_point = norm(p - point)
    point_q = norm(point - q)
    p_q = norm(p-q)
        
    if(a_b == a_point + point_b) and (p_q == p_point + point_q):
        return True
    else:
        return False
    

#@jit(nopython=True)
def line_intersect(p, q, a, b):
    r = q - p
    s = b - a
    d = r[0] * s[1] - r[1] * s[0]

    if d == 0:
        d = 1

    u = ((a[0] - p[0]) * r[1] - (a[1] - p[1]) * r[0]) / d
    t = ((a[0] - p[0]) * s[1] - (a[1] - p[1]) * s[0]) / d
    
    if u >= 0 and u <= 1 and t >= 0 and t <= 1:
        return True
    else:
        return False



#@jit(nopython=True)
def obstacle_intersect(p0, p1, t0, t1, t2):
    if line_intersect2(p0, p1, t0, t1) == True:
        return True
    elif line_intersect2(p0, p1, t1, t2) == True:
        return True
    elif line_intersect2(p0, p1, t2, t0) == True:
        return True
    else:
        return False
        
#@jit(nopython=True)  
def obstacle_inertsect2(l1, l2, ob1, ob2, ob3):
    #poly = shapely.geometry.Polygon([[12, 15], [15, 18], [10, 18]])
    poly = shapely.geometry.Polygon([ob1, ob2, ob3])
    #line =  shapely.geometry.LineString([[4.9454544529770352, 17.886868101177793], [15.866666369967989, 15.533332824707031]])   
    line = shapely.geometry.LineString([l1, l2])
    if(line.intersects(poly)):
        return True
    else:
        return False

#@jit(nopython=True)  
def obstacle_free(obstacle_triags, x_start, x_end):
    for i in range(len(obstacle_triags)):
        #print("Obstacle_Free_Trig:", i)
        result = obstacle_inertsect2(np.array(x_start), np.array(x_end), np.array(obstacle_triags[i][0]), np.array(obstacle_triags[i][1]), np.array(obstacle_triags[i][2]))
        if result == True:
            return False
    return True
          
        
#@jit(nopython=True)        
def within_boundary(boundary_triags, x):
    boundary = boundary_triags
    inside_boundary = False
    for i in range(len(boundary)):
        inside_boundary = inside_boundary or ( point_in_triangle(x[0:2], boundary[i][0], boundary[i][1], boundary[i][2]) )

    return inside_boundary



#@jit(nopython=True)
def check_point_collision(obstacle_triags, boundary_triags, x):
    if(within_boundary(boundary_triags, x) == True):
        for i in range(np.size(obstacle_triags, axis=0)):
            if point_in_triangle(x[0:2], obstacle_triags[i, 0], obstacle_triags[i, 1], obstacle_triags[i, 2]):
                return True
        ### If we get here, the points do not collide with the boundary or obstacles, and are within the boundary
        return False
    else:
        return True
        
        
        
        
def obstacle_free_items(obstacle_triags, boundary_triags, x_start, x_item):
    if(check_point_collision(obstacle_triags, boundary_triags, x_item) == False):
        return obstacle_free(obstacle_triags, x_start, x_item)
    else:
        epsilon = 0.1
        v = np.array(x_start) - np.array(x_item)
        x_item_new = x_item + v*epsilon
        return obstacle_free(obstacle_triags, x_start, x_item_new)




#@jit(nopython=True)
def bounding_box(points):
    min_x, min_y = np.min(points, axis=0)
    max_x, max_y = np.max(points, axis=0)
    if(len(points) > 1):
        center = np.array( (min_x + max_x)/2, (min_y + max_y)/2 )
    else:
        center = np.array(points)
    box = np.array([(min_x, min_y), (max_x, min_y), (max_x, max_y), (min_x, max_y)])
    return box, center



#@jit(nopython=True)
def in_hull(hull, p):
    """
    Test if points in `p` are in `hull`

    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the 
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    Reference: http://stackoverflow.com/questions/16750618/whats-an-efficient-way-to-find-if-a-point-lies-in-the-convex-hull-of-a-point-cl
    """
    if not isinstance(hull,Delaunay):
        hull = Delaunay(hull)

    return hull.find_simplex(p)>=0


