"""
Created on Thu Jan 26 07:55:10 2017

@author: Group 10

"""



import sys
from pathlib import Path # if you haven't already done so
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)
sys.path.append(root+"/vrep_python")
import numpy
import math
import time
from vrep_python import vrep
from util import load_params
from util import scene_plot
import matplotlib.pyplot as plt
from numpy.linalg import norm







def plot_lines(wall_lines, floor_size):
    num_lines = len(wall_lines[...,1])
    plt.xlim(-floor_size/2, floor_size*2);
    plt.ylim(-floor_size/2, floor_size*2);
    for i in range(0, num_lines):
        dx = wall_lines[i, 2] * math.cos(wall_lines[i, 3])
        dy = wall_lines[i, 2] * math.sin(wall_lines[i, 3])
        point_1 = numpy.array((wall_lines[i,0] - dx/2, wall_lines[i,1] - dy/2))
        point_2 = numpy.array((wall_lines[i,0] + dx/2, wall_lines[i,1] + dy/2))
        line = numpy.column_stack((point_1, point_2))
        plt.plot(line[0,...], line[1,...], 'r')
        plt.plot(wall_lines[i,0], wall_lines[i,1], 'ko')
        
        


def generateWallsParams(obstacle_points, floor_size):
    num_points = len(obstacle_points[...,1])
    """ Wall parameters: 
            middle point (x,y)
            length
            yawAngle """
    walls = numpy.zeros((num_points, 4))
    
    for i in range(0, num_points):
        point_i = obstacle_points[i, ...]
        if (i == 0):
            point_prev = obstacle_points[num_points-1, ...]            
        if(i > 0):            
            point_prev = obstacle_points[i-1, ...]
            
        middle_point = ((point_i + point_prev) / 2)
        length = math.sqrt( (point_i.item(0) - point_prev.item(0))**2 + (point_i.item(1) - point_prev.item(1))**2 )
        yawAngle = math.atan2(point_i.item(1) - point_prev.item(1), point_i.item(0) - point_prev.item(0))
        walls[i, 0] = middle_point.item(0)
        walls[i, 1] = middle_point.item(1)
        walls[i, 2] = length
        walls[i, 3] = yawAngle        
    
#    print(walls)
    return walls
            
    
                

def drawObstaclesVREP(params, clientID, floor_size, verbose):
    num_obstacles = params['num_obstacles']
    emptyBuff = bytearray()
    for i in range(0, num_obstacles):
        obs_i_walls = generateWallsParams(params['obstacles'][i], floor_size)
        num_sides = len(obs_i_walls[...,1])
        for j in range(0, num_sides):
            if(verbose == 1):
                plot_lines(obs_i_walls, floor_size)
            res, retInts, retFloats, retStrings, retBuffer = vrep.simxCallScriptFunction(clientID, 'API_commandServer', vrep.sim_scripttype_childscript, 
                                                                                        'drawWall_function', [], [obs_i_walls[j][0] - floor_size/2, obs_i_walls[j][1] - floor_size/2, obs_i_walls[j][2], obs_i_walls[j][3]], 
                                                                                        [], emptyBuff, vrep.simx_opmode_blocking)





def drawBoundaryVREP(params, clientID, floor_size, verbose):
    boundary_walls = generateWallsParams(params['boundary_polygon'], floor_size)
    emptyBuff = bytearray()
    if(verbose == 1):
        plot_lines(boundary_walls, floor_size)
    num_sides = len(boundary_walls[...,1])
    for j in range(0, num_sides):
        res, retInts, retFloats, retStrings, retBuffer = vrep.simxCallScriptFunction(clientID, 'API_commandServer', vrep.sim_scripttype_childscript, 
                                                                                    'drawBoundary_function', [], [boundary_walls[j][0] - floor_size/2, boundary_walls[j][1] - floor_size/2, boundary_walls[j][2], boundary_walls[j][3]], 
                                                                                    [], emptyBuff, vrep.simx_opmode_blocking)



def drawStartGoalPositions(params, clientID, floor_size):
    start_pos = params["start_pos"]
    goal_pos = params["goal_pos"]
    
    start_error_code, start_pos_handle = vrep.simxGetObjectHandle(clientID, 'startPosition', vrep.simx_opmode_oneshot_wait)
    goal_error_code, goal_pos_handle = vrep.simxGetObjectHandle(clientID, 'goalPosition', vrep.simx_opmode_oneshot_wait)
    
    if(start_error_code == 0):
        vrep.simxSetObjectPosition(clientID, start_pos_handle, -1, [params["start_pos"][0] - floor_size/2, params["start_pos"][1] - floor_size/2, 1], vrep.simx_opmode_blocking)
    if(goal_error_code == 0):
        vrep.simxSetObjectPosition(clientID, goal_pos_handle, -1, [params["goal_pos"][0] - floor_size/2, params["goal_pos"][1] - floor_size/2, 1], vrep.simx_opmode_blocking)



def drawScene(clientID, scene, floor_size, verbose_plots_py, robot_name):
    scene_plot.using_A1_format(scene)
    verbose_plots_py = 0
    drawBoundaryVREP(scene, clientID, floor_size, verbose_plots_py)
    drawObstaclesVREP(scene, clientID, floor_size, verbose_plots_py)
    drawStartGoalPositions(scene, clientID, floor_size)
    moveRobot(scene["start_pos"], clientID, floor_size, robot_name)



def drawSceneT2(clientID, scene, floor_size, verbose_plots_py, robot_name):
    scene_plot.using_A1_format(scene)
    verbose_plots_py = 0
    drawBoundaryVREP(scene, clientID, floor_size, verbose_plots_py)
    drawObstaclesVREP(scene, clientID, floor_size, verbose_plots_py)
    drawStartGoalPositions(scene, clientID, floor_size)
    moveRobot(scene["start_pos"], clientID, floor_size, robot_name)
#    rotateRobot(start_angle, clientID, floor_size, robot_name)
    norm_start_vel = norm(scene["start_vel"])
    mass = 1.0
    if(norm_start_vel != 0):
        start_acc = (scene["start_vel"] / norm_start_vel) * scene["a_max"] * 0.5
    else:
        start_acc = scene["start_vel"]
    applyForceRobot([start_acc[0]/mass, start_acc[1]/mass], clientID, floor_size, robot_name)
        



def moveRobot(position, clientID, floor_size, robot_name):
    robot_error_code, robot_handle = vrep.simxGetObjectHandle(clientID, robot_name, vrep.simx_opmode_oneshot_wait)
    if(robot_error_code == 0):
        vrep.simxSetObjectPosition(clientID, robot_handle, -1, [position[0] - floor_size/2, position[1] - floor_size/2, 2], vrep.simx_opmode_blocking)


def rotateRobot(yaw_angle, clientID, floor_size, robot_name):
    robot_error_code, robot_handle = vrep.simxGetObjectHandle(clientID, robot_name, vrep.simx_opmode_oneshot_wait)
    if(robot_error_code == 0):
        vrep.simxSetObjectOrientation(clientID, robot_handle, -1, [0, 0, yaw_angle], vrep.simx_opmode_blocking)



def setVelocityRobot(v, clientID, floor_size, robot_name):
    emptyBuff = bytearray()
    print(v)
    robot_error_code, robot_handle = vrep.simxGetObjectHandle(clientID, robot_name, vrep.simx_opmode_oneshot_wait)
    if(robot_error_code == 0):
        res, retInts, retFloats, retStrings, retBuffer = vrep.simxCallScriptFunction(clientID, 'Robot', vrep.sim_scripttype_childscript, 
                                                                            'setVelocity_function', [], [v[0], v[1]], 
                                                                            [], emptyBuff, vrep.simx_opmode_blocking)



def applyForceRobot(force, clientID, floor_size, robot_name):
    emptyBuff = bytearray()
    robot_error_code, robot_handle = vrep.simxGetObjectHandle(clientID, robot_name, vrep.simx_opmode_oneshot_wait)
    if(robot_error_code == 0):
        res, retInts, retFloats, retStrings, retBuffer = vrep.simxCallScriptFunction(clientID, 'Robot', vrep.sim_scripttype_childscript, 
                                                                            'addForce_function', [], [force[0], force[1]], 
                                                                            [], emptyBuff, vrep.simx_opmode_blocking)
        vrep.simxSynchronousTrigger(clientID)

    

    
def establishConnection(IP, port):
    vrep.simxFinish(-1) # just in case, close all opened connections
    clientID = vrep.simxStart(IP, port, True, True, 5000, 5) # Connect to V-REP
    if clientID!=-1:
        print ('Connected to remote API server')
    else:
        print('Connection not successful')
        sys.exit('Could not connect')
    
    return clientID
    
    
    
    
def closeConnection(clientID):
    vrep.simxFinish(clientID)
    
    
def setSynchronousMode(clientID):
    vrep.simxSynchronous(clientID, True);



def playTrajectoryT1(clientID, traj, floor_size, robot_name, dt):
    num_points = len(traj)
    for i in range(num_points-1):
        moveRobot(traj[i].tolist(), clientID, floor_size, robot_name)
        time.sleep(dt)
        


def playTrajectoryT2(clientID, traj, floor_size, robot_name, dt):
    num_points = len(traj)
    for i in range(num_points-1):
        moveRobot([traj[i][0,0], traj[i][1,0]], clientID, floor_size, robot_name)
        time.sleep(dt)


def playTrajectoryT2_applyingForce(clientID, controls, floor_size, robot_name, dt, mass):
    num_points = len(controls)
    for i in range(num_points-1):
        print(controls[i][...,0])
        applyForceRobot([controls[i][0,0]/mass, controls[i][1,0]/mass], clientID, floor_size, robot_name)
        time.sleep(dt)




def playTrajectoryT3(clientID, traj, floor_size, robot_name):
    num_points = len(traj)
    emptyBuff = bytearray()
    for i in range(num_points-1):
        moveRobot([traj[i][0,0], traj[i][1,0]], clientID, floor_size, robot_name)
#        if(i==1):
#            vrep.simxCallScriptFunction(clientID, 'API_commandServer', vrep.sim_scripttype_childscript, 
#                                        'resetGraph_function', [], [], 
#                                        [], emptyBuff, vrep.simx_opmode_blocking)
        time.sleep(0.1)
        
        
        
        