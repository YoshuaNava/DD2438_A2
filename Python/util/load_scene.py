# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 16:53:51 2017

@author: alfredoso
"""
import json
import re
import numpy as np
import pprint as pp


def load_from_json(filepath):
    """
    Loads a JSON file for the assignment
    """
    with open(filepath) as data_file:
        data = json.load(data_file)


#    pp.pprint(data)
    scene = {}
    scene["L_car"] = float(data["L_car"])
    scene["a_max"] = float(data["a_max"])
    scene["boundary_polygon"] = np.array(data["boundary_polygon"], dtype='f')
    scene["omega_max"] = float(data["omega_max"])
    scene["phi_max"] = float(data["phi_max"])
    scene["k_friction"] = float(data["k_friction"])
    scene["gravity"] = 9.81
    scene["sensor_range"] = float(data["sensor_range"])



    num_polygons = 0
    polygon_regex = re.compile(r"polygon\d*$")
    for d in data:
        polygon_matches = polygon_regex.match(d)
        if polygon_matches:
            num_polygons += 1

    for i in range(0, num_polygons):
        polygon = np.array(data["polygon"+str(i)], dtype='f')
        if i == 0:
            obstacles = [polygon]
        else:
            obstacles.append(polygon)

    scene["obstacles"] = obstacles
    scene["num_obstacles"] = num_polygons




    num_start_pos = 0
    i = 0
    start_pos_regex = re.compile(r"start_pos\d*$")
    for d in data:
        start_pos_matches = start_pos_regex.match(d)
        if start_pos_matches:
            field_str = start_pos_matches.group(0)
            start_pos = np.array(data[field_str])
            if (i==0):
                start_points = [start_pos]
            else:                
                start_points.append(start_pos)
            i = i + 1
            num_start_pos = num_start_pos + 1
            
    scene["start_points"] = start_points
    scene["num_start_pos"] = num_start_pos



    num_goal_pos = 0
    goal_pos_regex = re.compile(r"goal_pos\d*$")
    i = 0
    for d in data:
        goal_pos_matches = goal_pos_regex.match(d)        
        if goal_pos_matches:
            field_str = goal_pos_matches.group(0)
            goal_pos = np.array(data[field_str])
            if (i==0):
                goal_points = [goal_pos]
            else:                
                goal_points.append(goal_pos)
            i = i + 1
            num_goal_pos = num_goal_pos + 1
        
            
    scene["goal_points"] = goal_points
    scene["num_goal_pos"] = num_goal_pos



    items_set = set()
    
    num_item_points = 0
    item_regex = re.compile(r"item_(\d+)_\d+$")
    for d in data:
        item_matches = item_regex.search(d)
        if item_matches is not None:
#            field_str = item_matches.group(0)
            item_group = item_matches.groups()
            items_set.add( int(item_group[0]) )
            num_item_points = num_item_points + 1
    
#    num_items = len(items_set)
#    scene["num_items"] = num_items
#    print(num_item_points)
    
    item_points = []    
    
    for idx in items_set:
        item_regex = re.compile(r"^item_" + str(idx) + "_\d+$")
        num_points = 0
        for d in data:
            item_matches = item_regex.search(d)
            if item_matches is not None:
                point = np.array( (data[d][0],data[d][1]), dtype='f')
                if(len(item_points) == 0):
                    item_points = [point]
                else:
                    item_points.append(point)

    scene["item_points"] = np.array(item_points)
    scene["num_item_points"] = num_item_points


    scene["start_vel"] = np.array(data["start_vel"], dtype='f')
    scene["goal_vel"] = np.array(data["goal_vel"], dtype='f')
    scene["v_max"] = float(data["v_max"])

    return scene







def load_from_json_T5(filepath):
    """
    Loads a JSON file for the assignment
    """
    with open(filepath) as data_file:
        data = json.load(data_file)


#    pp.pprint(data)
    scene = {}
    scene["L_car"] = float(data["L_car"])
    scene["a_max"] = float(data["a_max"])
    scene["boundary_polygon"] = np.array(data["boundary_polygon"], dtype='f')
    scene["omega_max"] = float(data["omega_max"])
    scene["phi_max"] = float(data["phi_max"])
    scene["k_friction"] = float(data["k_friction"])
    scene["gravity"] = 9.81
    scene["sensor_range"] = float(data["sensor_range"])



    num_polygons = 0
    polygon_regex = re.compile(r"polygon\d*$")
    for d in data:
        polygon_matches = polygon_regex.match(d)
        if polygon_matches:
            num_polygons += 1

    for i in range(0, num_polygons):
        polygon = np.array(data["polygon"+str(i)], dtype='f')
        if i == 0:
            obstacles = [polygon]
        else:
            obstacles.append(polygon)

    scene["obstacles"] = obstacles
    scene["num_obstacles"] = num_polygons




    num_start_pos = 0
    start_pos_regex = re.compile(r"start_pos\d*$")
    for d in data:
        start_pos_matches = start_pos_regex.match(d)
        if start_pos_matches:
            num_start_pos += 1
    for i in range(0, num_start_pos):        
        start_pos = np.array(data["start_pos"+str(i)], dtype='f')
        if (i==0):
            start_points = [start_pos]
        else:                            
            
            start_points.append(start_pos)
            
    scene["start_points"] = start_points
    scene["num_start_pos"] = num_start_pos



    num_goal_pos = 0
    goal_pos_regex = re.compile(r"goal_pos\d*$")
    for d in data:
        goal_pos_matches = goal_pos_regex.match(d)
        if goal_pos_matches:
            num_goal_pos += 1
    for i in range(0, num_goal_pos):
        goal_pos = np.array(data["goal_pos"+str(i)], dtype='f')
        if (i==0):
            goal_points = [goal_pos]
        else:                
            goal_points.append(goal_pos)
            
    scene["goal_points"] = goal_points
    scene["num_goal_pos"] = num_goal_pos





    scene["start_vel"] = np.array(data["start_vel"], dtype='f')
    scene["goal_vel"] = np.array(data["goal_vel"], dtype='f')
    scene["v_max"] = float(data["v_max"])

    return scene

