# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 16:53:51 2017

@author: alfredoso
"""
import json
import re
import numpy


def load_from_json(filepath):
    """
    Loads a JSON file for the assignment
    """
    with open(filepath) as data_file:
        data = json.load(data_file)

    params = {}
    params["L_car"] = float(data["L_car"])
    params["a_max"] = float(data["a_max"])
    params["boundary_polygon"] = numpy.array(data["boundary_polygon"], dtype='f')
    params["goal_pos"] = numpy.array(data["goal_pos"], dtype='f')
    params["goal_vel"] = numpy.array(data["goal_vel"], dtype='f')
    params["omega_max"] = float(data["omega_max"])
    params["phi_max"] = float(data["phi_max"])
    params["k_friction"] = float(data["k_friction"])
    params["gravity"] = 9.81

    num_polygons = 0
    polygon_regex = re.compile(r"^polygon\d$")
    for d in data:
        polygon_matches = polygon_regex.match(d)
        if polygon_matches:
            num_polygons += 1

    for i in range(0, num_polygons):
        polygon = numpy.array(data["polygon"+str(i)], dtype='f')
        if i == 0:
            obstacles = [polygon]
        else:
            obstacles.append(polygon)

    params["obstacles"] = obstacles
    params["num_obstacles"] = num_polygons

    params["start_pos"] = numpy.array(data["start_pos"], dtype='f')
    params["start_vel"] = numpy.array(data["start_vel"], dtype='f')
    params["v_max"] = float(data["v_max"])

    return params
