# -*- coding: utf-8 -*-
"""
Created on Fri Jan 20 01:03:52 2017

@author: alfredoso
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as colormap
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection


scene_span = 15


def full_using_A2_format(scene, ax=None, fig=None):
    if(ax is None) or (fig is None):
        fig, ax = plt.subplots()
        
    agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
        
    boundaries = Polygon(scene['boundary_polygon'], fill=False)
    ax.add_patch(boundaries)
    
    num_obstacles = scene['num_obstacles']
    patches = []
    for i in range(0, num_obstacles):
        obstacle = Polygon(scene['obstacles'][i])
        patches.append(obstacle)
        
    obstacles_coll = PatchCollection(patches, cmap=colormap.jet, alpha=0.5)
    colors = np.array([1,] * len(patches) )
    obstacles_coll.set_array(np.array(colors))
    ax.add_collection(obstacles_coll)
    
    
    num_items = scene["num_items"]
    for item_idx in range(0, num_items):
        item = scene["item_" + str(item_idx+1) + "_points"]
        num_points_item = scene["item_" + str(item_idx+1) + "_cardinality"]
        color = np.random.rand(3,1)
        for point_idx in range(0, num_points_item):
            ax.plot(item[point_idx][0], item[point_idx][1], c=color, marker='x')
            
    start_positions = scene["start_points"]
    num_start_pos = scene["num_start_pos"]
    sensor_range = scene["sensor_range"]
    
    for start_idx in range(0, num_start_pos):
        color = np.random.rand(3,1)
        start_pos = start_positions[start_idx]
        ax.plot(start_pos[0], start_pos[1], c=agent_color[start_idx], marker='*')
        if(sensor_range < scene_span):
            circle = plt.Circle(start_pos, sensor_range, color = agent_color[start_idx], fill=False)
            ax.add_patch(circle)
    #        start_vel_vector = start_pos + scene['start_vel']
    #        starting_state = np.column_stack((start_pos, start_vel_vector))
    #        plt.plot(starting_state[0,...], starting_state[1,...], 'g')


    goal_positions = scene["goal_points"]
    num_goal_pos = scene["num_goal_pos"]
    for goal_idx in range(0, num_goal_pos):
        color = np.random.rand(3,1)
        ax.plot(goal_positions[goal_idx][0], goal_positions[goal_idx][1], c=agent_color[goal_idx], marker='s')

    plt.axis('equal')
    fig.set_size_inches(5, 5.5)
    ax.margins(0.01)
    ax.autoscale()
#    ax.margins(0.1)
    plt.show()
    
    
    

def simple_using_A2_format(scene, ax=None, fig=None):
    if(ax is None) or (fig is None):
        fig, ax = plt.subplots()
    boundaries = Polygon(scene['boundary_polygon'], fill=False)
    ax.add_patch(boundaries)
    agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
    
    num_obstacles = scene['num_obstacles']
    patches = []
    for i in range(0, num_obstacles):
        obstacle = Polygon(scene['obstacles'][i])
        patches.append(obstacle)
        
    obstacles_coll = PatchCollection(patches, cmap=colormap.jet, alpha=0.5)
    colors = np.array([1,] * len(patches) )
    obstacles_coll.set_array(np.array(colors))
    ax.add_collection(obstacles_coll)
            
    start_positions = scene["start_points"]
    num_start_pos = scene["num_start_pos"]
    sensor_range = scene["sensor_range"]
    
    for start_idx in range(0, num_start_pos):
        color = np.random.rand(3,1)
        start_pos = start_positions[start_idx]
        ax.plot(start_pos[0], start_pos[1], c=agent_color[start_idx], marker='*')
        if(sensor_range < scene_span):
            circle = plt.Circle(start_pos, sensor_range, color = agent_color[start_idx], fill=False)
            ax.add_patch(circle)


    goal_positions = scene["goal_points"]
    num_goal_pos = scene["num_goal_pos"]
    for goal_idx in range(0, num_goal_pos):
        color = np.random.rand(3,1)
        ax.plot(goal_positions[goal_idx][0], goal_positions[goal_idx][1], c=agent_color[goal_idx], marker='s')

    plt.axis('equal')
    fig.set_size_inches(5, 5.5)
    ax.margins(0.01)
    ax.autoscale()
#    ax.margins(0.1)
    plt.show()
    




    
def partial_using_A2_format(scene, ax=None, fig=None):
    if(ax is None) or (fig is None):
        fig, ax = plt.subplots()
    boundaries = Polygon(scene['boundary_polygon'], fill=False)
    ax.add_patch(boundaries)
    
    num_obstacles = scene['num_obstacles']
    patches = []
    for i in range(0, num_obstacles):
        obstacle = Polygon(scene['obstacles'][i])
        patches.append(obstacle)
        
    obstacles_coll = PatchCollection(patches, cmap=colormap.jet, alpha=0.5)
    colors = np.array([1,] * len(patches) )
    obstacles_coll.set_array(np.array(colors))
    ax.add_collection(obstacles_coll)

    agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
    start_positions = scene["start_points"]
    num_start_pos = scene["num_start_pos"]
    sensor_range = scene["sensor_range"]
    
    for start_idx in range(0, num_start_pos):
        start_pos = start_positions[start_idx]
        ax.plot(start_pos[0], start_pos[1], c=agent_color[start_idx], marker='*')
        if(sensor_range < scene_span):
            circle = plt.Circle(start_pos, sensor_range, color = agent_color[start_idx], fill=False)
            ax.add_patch(circle)
    
    goal_positions = scene["goal_points"]
    num_goal_pos = scene["num_goal_pos"]
    for goal_idx in range(0, num_goal_pos):
        ax.plot(goal_positions[goal_idx][0], goal_positions[goal_idx][1], c=agent_color[goal_idx], marker='s')

    plt.axis('equal')
#    fig.set_size_inches(5, 5.5)
    fig.set_size_inches(10, 10.5)
    ax.margins(0.01)
    ax.autoscale()
#    ax.margins(0.1)








def simple_using_T5_format(scene, ax=None, fig=None):
    if(ax is None) or (fig is None):
        fig, ax = plt.subplots()
    boundaries = Polygon(scene['boundary_polygon'], fill=False)
    ax.add_patch(boundaries)
    agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
    
    num_obstacles = scene['num_obstacles']
    patches = []
    for i in range(0, num_obstacles):
        obstacle = Polygon(scene['obstacles'][i])
        patches.append(obstacle)
        
    obstacles_coll = PatchCollection(patches, cmap=colormap.jet, alpha=0.5)
    colors = np.array([1,] * len(patches) )
    obstacles_coll.set_array(np.array(colors))
    ax.add_collection(obstacles_coll)
            
    start_positions = scene["start_points"]
    num_start_pos = scene["num_start_pos"]
    sensor_range = scene["sensor_range"]
    
    for start_idx in range(0, num_start_pos):
        color = np.random.rand(3,1)
        start_pos = start_positions[start_idx]
        ax.plot(start_pos[0], start_pos[1], c=agent_color[start_idx], marker='*')
        start_vel_vector = start_pos + scene['start_vel']
        starting_state = np.column_stack((start_pos, start_vel_vector))
        plt.plot(starting_state[0,...], starting_state[1,...], c=agent_color[start_idx])
#        if(sensor_range < scene_span):
#            circle = plt.Circle(start_pos, sensor_range, color = color, fill=False)
#            ax.add_patch(circle)
            


    goal_positions = scene["goal_points"]
    num_goal_pos = scene["num_goal_pos"]
    for goal_idx in range(0, num_goal_pos):
        color = np.random.rand(3,1)
        ax.plot(goal_positions[goal_idx][0], goal_positions[goal_idx][1], c=agent_color[goal_idx], marker='s')

    plt.axis('equal')
    fig.set_size_inches(5, 5.5)
    ax.margins(0.01)
    ax.autoscale()
#    ax.margins(0.1)
    plt.show()


    

    
def partial_using_T5_format(scene, ax=None, fig=None):
    if(ax is None) or (fig is None):
        fig, ax = plt.subplots()
    boundaries = Polygon(scene['boundary_polygon'], fill=False)
    ax.add_patch(boundaries)
    
    num_obstacles = scene['num_obstacles']
    patches = []
    for i in range(0, num_obstacles):
        obstacle = Polygon(scene['obstacles'][i])
        patches.append(obstacle)
        
    obstacles_coll = PatchCollection(patches, cmap=colormap.jet, alpha=0.5)
    colors = np.array([1,] * len(patches) )
    obstacles_coll.set_array(np.array(colors))
    ax.add_collection(obstacles_coll)

    agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
    start_positions = scene["start_points"]
    num_start_pos = scene["num_start_pos"]
    sensor_range = scene["sensor_range"]
    
    for start_idx in range(0, num_start_pos):
        start_pos = start_positions[start_idx]
        ax.plot(start_pos[0], start_pos[1], c=agent_color[start_idx], marker='*')
        start_vel_vector = start_pos + scene['start_vel']
        starting_state = np.column_stack((start_pos, start_vel_vector))
        ax.plot(starting_state[0,...], starting_state[1,...], c=agent_color[start_idx])
#        if(sensor_range < scene_span):
#            circle = plt.Circle(start_pos, sensor_range, color = agent_color[start_idx], fill=False)
#            ax.add_patch(circle)
    

    goal_positions = scene["goal_points"]
    num_goal_pos = scene["num_goal_pos"]
    for goal_idx in range(0, num_goal_pos):
        ax.plot(goal_positions[goal_idx][0], goal_positions[goal_idx][1], c=agent_color[goal_idx], marker='s')

    plt.axis('equal')
    fig.set_size_inches(5, 5.5)
    ax.margins(0.01)
    ax.autoscale()
#    ax.margins(0.1)



    
    
    
    
def from_scene_graph(G_scene, ax=None, fig=None):
    if(ax is None) or (fig is None):
        fig, ax = plt.subplots()
    red = (1, 0, 0)
    green = (0, 1, 0)
    light_blue = (0, 0, 0.2)
    middle_blue = (0, 0, 0.6)
    dark_blue = (0, 0, 1)
    black = (0, 0, 0)
    white = (1, 1, 1)
    
    star = '*'
    triangle = '^'
    square = 's'
    pixel = ','    

    for node in G_scene.nodes_iter(data=True):
        if node[1]['invalid'] == 1 or node[1]['obstacle'] == 1:
            if node[1]['obstacle'] == 1:
                ax.plot(node[0][0], node[0][1], color=black, marker=square)
            if node[1]['invalid'] == 1:
                ax.plot(node[0][0], node[0][1], color=dark_blue, marker=square)
        else:
            if node[1]['covered'] == 1:
                ax.plot(node[0][0], node[0][1], color=green, marker=triangle)
            if node[1]['covered'] == 1:
                ax.plot(node[0][0], node[0][1], color=red, marker=triangle)

    plt.show()