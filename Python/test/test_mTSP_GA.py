# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

@author: tanay
"""

import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

from classes import mTSP_GA_v3 as MTSP_GA
import numpy as np



    
# Inputs
#xy = 10*np.random.rand(40,2)
#xy = np.zeros( (18,2) )
xy = np.array( [[82,14], [91,42], [12,92], [92, 80], [63,96], [9,66], [28,3], [55,85], [96,94], [97,68],
                [15,76], [98,75], [96,39], [49,66], [80,17], [12,78], [80,80], [55,45]] )
#x=[82 91 12 92 63 9 28 55 96 97 15 98 96 49 80 12 80 55];
#
#y=[14 42 92 80 96 66 3 85 94 68 76 75 39 66 17 78 80 45];
dmat = None
num_salesmen = 4
min_tour = 1
pop_size = 80
num_iter = 1000


print("Step 1: Genetic algorithm initialization")
alpha = 1
mtsp_ga = MTSP_GA.mTSP_GA(xy, num_salesmen, min_tour, pop_size, num_iter, alpha, dmat)

print("Step 2: Optimization of mTSP route")
mtsp_ga.run_with_adjacency_matrix()


print("Algorithm finished running")
print("Results")
print("Optimal Breaks:")
print(mtsp_ga.opt_breaks)
print("Optimal Route (list of cities):")
print(mtsp_ga.opt_route)
mtsp_ga.plot_agents_best_route()
mtsp_ga.plot_fitness_history()

