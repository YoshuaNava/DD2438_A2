# -*- coding: utf-8 -*-
"""
Created on Fri Jan 20 00:24:54 2017

@author: alfredoso
"""

import sys
from pathlib import Path # if you haven't already done so
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import matplotlib.pyplot as plt
from util import load_scene
from util import scene_plot

scene = load_scene.load_from_json('../json_scenes/problem_A12.json')

#import pprint as pp
#pp.pprint(scene)
fig, ax = plt.subplots()
scene_plot.full_using_A2_format(scene, ax, fig)

scene_plot.simple_using_A2_format(scene)

