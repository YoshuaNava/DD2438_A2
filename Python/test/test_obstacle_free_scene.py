# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 19:50:19 2017

@author: alfredoso
"""

import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import time
import math
import pprint as pp
import numpy as np
import numpy.matlib
from matplotlib import pyplot as plt
import matplotlib.cm as cmx
import matplotlib.colors as colors
from scipy.spatial import distance
from scipy.spatial import ConvexHull
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform
import networkx as nx

from util import scene_plot
from util import polygon_tools as poly_tools

from util import load_scene
from classes import DiscreteScene as DS

scene_params = load_scene.load_from_json('../json_scenes/problem_A12.json')
x_res = 100
y_res = 100
disc_scene = DS.DiscreteScene(scene_params, x_res, y_res)

print("Step1")
disc_scene.discretize_map()
print("Step2")
disc_scene.load_scene_item_points()
print("Step3")
disc_scene.plot_map_stored()
print("Step4")
disc_scene.create_map_graph()
print("Step5")
disc_scene.generate_convex_cover()
print("Step6")
disc_scene.plot_scene()
print("Step7")
#disc_scene.plot_scene_with_convex_covers()
print("Step8")
adj_matrix = disc_scene.calculate_convex_covers_adjacency_matrix()
print("Step9")
#disc_scene.plot_adjacency_matrix()

origin_points = disc_scene.get_convex_covers_origin_points()

print("Step10")

for cover in disc_scene.G_convex_covers.nodes_iter(data=True):
    for other_cover in disc_scene.G_convex_covers.nodes_iter(data=True):
        covers_distance = distance.euclidean(cover[0], other_cover[0])
        free_path = poly_tools.obstacle_free(disc_scene.obstacle_triags, cover[0], other_cover[0])
        print("Cover: ", cover[0])
        print("Cover ID:", cover[1]['id'])
        print("Other Cover:", other_cover[0])
        print("Other Cover ID:", other_cover[1]['id'])
        print("Distance", covers_distance)
        print("Obstacle free", free_path)
        fig, ax = plt.subplots()
        scene_plot.partial_using_A2_format(disc_scene.scene_params, ax, fig)
        
        ax.plot(cover[0][0], cover[0][1], 'kp')
        ax.plot(other_cover[0][0], other_cover[0][1], 'ko')
        line = np.column_stack((cover[0], other_cover[0]))
        if free_path:
            ax.plot(line[0,:], line[1,:], 'g')
        else:
            ax.plot(line[0,:], line[1,:], 'r')
        
        plt.show()
        
        print("hey")
        
        time.sleep(2)
        
        