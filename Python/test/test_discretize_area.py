# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

@author: tanay
"""

import sys
from pathlib import Path 
from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)


#import matplotlib.pyplot as plt
from util import load_scene
#from util import scene_plot
from util import polygon_tools as poly_tools
import itertools
import numpy as np
from matplotlib import pyplot as plt
import math

scene = load_scene.load_from_json('../json_scenes/problem_A12.json')


boundary_triags = poly_tools._triangulate_polygone(scene['boundary_polygon'])
obstacle_triags = map(poly_tools._triangulate_polygone, scene['obstacles'])
obstacle_triags = np.array(list(itertools.chain(*obstacle_triags)))

# Resolution of the scene in x and y.
x_res = 250
y_res = 250

x_lim = scene["boundary_polygon"][2][0]
y_lim = scene["boundary_polygon"][2][1]

scene_discrete = np.zeros( (x_res, y_res) )

x_disc = np.linspace(-1, x_lim, num=x_res)
y_disc = np.linspace(-1, y_lim, num=y_res)

#def my_range(start, end, step):
#    while start <= end:
#        yield start
#        start += step
        
for i in range(0, len(x_disc)):
    for j in range(0, len(y_disc)):
        point = np.array( (x_disc[i], y_disc[j]) )
        if poly_tools.withinBoundary(boundary_triags, point):
            if poly_tools.checkPointCollision(obstacle_triags, boundary_triags, point, point):
                scene_discrete[j][i] = 1
        else:
            scene_discrete[j][i] = -1


transform = ndimage.distance_transform_edt(scene_discrete==0)

print("\n\n")
print("Discretized scene")
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(scene_discrete, cmap=plt.cm.coolwarm, origin='lower')
legend = ['Out of boundary', 'Free space', 'Obstacle']
cbar = fig.colorbar(cax, ticks=[-1, 0, 1])
cbar.ax.set_yticklabels(legend)
plt.show()



            
