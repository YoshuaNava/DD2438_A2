# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

"""

import sys
from pathlib import Path 
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import math
from util import load_scene
from util import scene_plot
from classes import mTSP_GA_v2 as MTSP_GA
from classes import GreedyRangeSearch as GRS
from classes import MultiAgentRoute as MAR
import numpy as np
from numpy.linalg import norm
from matplotlib import pyplot as plt


# Very important
sys.setrecursionlimit(15000)



scene_params = load_scene.load_from_json('../json_scenes/problem_A3.json')
scene_params["goal_points"] = []
scene_params["num_goal_pos"] = 0


use_obstacle_penalty = False
grs = GRS.GreedyRangeSearch(scene_params, use_obstacle_penalty)



print("Step0")
grs.plot_scene()
print("Step1")
grs.load_scene_item_points()
print("Step2")
grs.create_points_dict()
print("Step3")
grs.do_greedy_search()
print("Step4")
grs.plot_scene_with_convex_covers()
print("Step5")
grs.min_set_cover()
print("Step6")
adj_matrix = grs.calculate_convex_covers_adjacency_matrix(use_obstacle_penalty)
print("Step7")
grs.plot_adjacency_matrix()
print("Step8")
origin_points = grs.get_set_covers_points()





print("Step9")
alpha = 1
num_agents = 4
#min_tour = max(1, math.floor(num_points/num_agents))
min_tour = 1
pop_size = 80
num_iter = 10000
mtsp_ga = MTSP_GA.mTSP_GA(origin_points, num_agents, min_tour, pop_size, num_iter, alpha, adj_matrix, scene_params)


print("Step11")
mtsp_ga.run_with_adjacency_matrix()




print("Algorithm finished running")
print("Results")
print("Optimal Breaks:")
#print(mtsp_ga.opt_breaks)
print("Optimal Route (list of cities):")
print(mtsp_ga.opt_route)
print("Global Minimum")
print(mtsp_ga.global_min)
mtsp_ga.plot_agents_best_route()
mtsp_ga.plot_fitness_history()






#rim = mtsp_ga.build_route_indices_matrix(mtsp_ga.opt_route)
#print(rim)

mtsp_ga.plot_agents_route_rim(mtsp_ga.opt_route, mtsp_ga.opt_rng)


dt = 0.2
print("Step11")
multi_agent_route = MAR.MultiAgentRoute(origin_points, mtsp_ga.opt_route, mtsp_ga.opt_rng, scene_params, dt)
agents_trajs = multi_agent_route.generate_trajs()
#multi_agent_route.play_trajectories()
print("Step12")
multi_agent_route.play_trajectories_showing_items()

