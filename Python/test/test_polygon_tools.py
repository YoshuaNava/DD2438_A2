# -*- coding: utf-8 -*-
"""
Created on Fri Jan 20 00:24:54 2017

@author: alfredoso
"""

import sys
from pathlib import Path # if you haven't already done so
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import matplotlib.pyplot as plt
from util import load_scene
from util import scene_plot
from util import polygon_tools as poly_tools
import itertools
import numpy as np



scene = load_scene.load_from_json('../json_scenes/problem_A12.json')

#import pprint as pp
#pp.pprint(scene)
fig, ax = plt.subplots()
scene_plot.full_using_A2_format(scene, ax, fig)

scene_plot.simple_using_A2_format(scene)


boundary_triags = poly_tools._triangulate_polygone(scene['boundary_polygon'])
obstacle_triags = map(poly_tools._triangulate_polygone, scene['obstacles'])
obstacle_triags = np.array(list(itertools.chain(*obstacle_triags)))


point = np.array( (5,10) )

result1 = poly_tools.point_in_triangle(point, boundary_triags[0][0], boundary_triags[0][1], boundary_triags[0][2])

result2 = poly_tools.withinBoundary(boundary_triags, point)

result3 = poly_tools.checkPointCollision(obstacle_triags, boundary_triags, point, point)