# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

@author: tanay
"""

import sys
from pathlib import Path 
#from scipy import ndimage
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

from util import load_scene

from classes import DiscreteScene as DS


# Very important
sys.setrecursionlimit(15000)



scene_params = load_scene.load_from_json('../json_scenes/problem_A12.json')

# Maximum resolution for now: 100
x_res = 50
y_res = 50
disc_scene = DS.DiscreteScene(scene_params, x_res, y_res)


disc_scene.discretize_map()
disc_scene.load_scene_item_points()

disc_scene.plot_map_stored()



disc_scene.create_map_graph()
disc_scene.generate_convex_cover()


disc_scene.plot_scene()
disc_scene.plot_scene_with_convex_covers()


adj_matrix = disc_scene.calculate_convex_covers_adjacency_matrix()
disc_scene.plot_adjacency_matrix()

origin_points = disc_scene.get_convex_covers_origin_points()

#disc_scene.plot_convex_covers_and_neighbors()



