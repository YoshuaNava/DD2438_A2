# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

@author: tanay
"""

import sys
from pathlib import Path 
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)


#import matplotlib.pyplot as plt
from util import load_scene
#from util import scene_plot
from util import polygon_tools as poly_tools
import itertools
import numpy as np

scene = load_scene.load_from_json('../json_scenes/problem_A12.json')


boundary_triags = poly_tools._triangulate_polygone(scene['boundary_polygon'])
obstacle_triags = map(poly_tools._triangulate_polygone, scene['obstacles'])
obstacle_triags = np.array(list(itertools.chain(*obstacle_triags)))

scene_map = np.zeros((250,250))
inside_point = []

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step
        
for x in my_range (0, 25.1, 0.1):
    for y in my_range (0, 25.1, 0.1):
        point =np.array((x, y))
        if poly_tools.withinBoundary(boundary_triags, point) and poly_tools.checkPointCollision(obstacle_triags, boundary_triags, point, point):
            print("inside: %d", point)
            x = int(x*10)
            y = int(y*10)
            inside_point.append([x,y])
            scene_map[x][y] = 1
            
            
