# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 12:53:54 2017

@author: alfredoso
"""

import sys
from pathlib import Path # if you haven't already done so
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import itertools
import math
import time
import numpy as np
from numpy.linalg import norm
from matplotlib import pyplot as plt
from util import load_scene
from util import scene_plot
from util import polygon_tools as poly_tools
from classes import  RRT_STAR_KP

scene_params = load_scene.load_from_json_T5('../json_scenes/problem_B5_new.json')

#scene_params["goal_points"] = np.array( [[14,18], [18,14], [14,14], [18,18]] )
start_points = np.array(scene_params["start_points"])
goal_points = np.array(scene_params["goal_points"])

boundary_triags = poly_tools._triangulate_polygone(scene_params['boundary_polygon'])
obstacle_triags = map(poly_tools._triangulate_polygone, scene_params['obstacles'])
obstacle_triags = np.array(list(itertools.chain(*obstacle_triags)))


#import pprint as pp
#pp.pprint(scene)
fig, ax = plt.subplots()
scene_plot.simple_using_T5_format(scene_params, ax, fig)




################ Dynamical system equations ####################
def A_t(task_number, x, u):
    # Dynamic point mass
    if(task_number == 2):
        # x = [x, y, v_x, v_y]
        # u = [u_x, u_y]
        return np.matrix([[0, 0, 1, 0], [0, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0]])
        
    # Differential drive
    if(task_number == 3):
        # x = [x, y, theta]
        # u = [u_v, u_omega]
        return np.matrix([[0, 0, -u[0]*math.sin(x[2])], [0, 0, u[0]*math.cos(x[2])], [0, 0, 0]])
    
    # Kinematic car
    if(task_number == 4):
        # x = [x, y, theta]
        # u = [u_v, u_phi]
        return np.matrix([[0, 0, -u[0]*math.sin(x[2])], [0, 0, u[0]*math.cos(x[2])], [0, 0, 0]])    

    # Dynamic car
    if(task_number == 5):
        # x = [x, y, theta, v]
        # u = [u_a, u_phi]
        L_car = scene_params["L_car"]
        return np.matrix([[0, 0, -x[3]*math.sin(x[2]), math.cos(x[2])], [0, 0, x[3]*math.cos(x[2]), math.sin(x[2])], [0, 0, 0, 1/L_car * math.tan(u[1])], [0, 0, 0, 0]])    
    
    return None



def B_t(task_number, x, u):
    # Dynamic point mass
    if(task_number == 2):
        # x = [x, y, v_x, v_y]
        # u = [u_x, u_y]
        return np.matrix([[0, 0], [0, 0], [1, 0], [0, 1 ]])

    # Differential drive
    if(task_number == 3):
        # x = [x, y, theta]
        # u = [u_v, u_omega]
        return np.matrix([[math.cos(x[2]), 0], [math.sin(x[2]), 0], [0, 1 ]])

    # Kinematic car
    if(task_number == 4):
        # x = [x, y, theta]
        # u = [u_v, u_phi]
        L_car = scene_params["L_car"]
        return np.matrix([[math.cos(x[2]), 0], [math.sin(x[2]), 0], [math.tan(u[1]) / L_car, (u[0]/L_car) * pow(1/math.cos(u[1]),2) ]])

    # Dynamic car
    if(task_number == 5):
        # x = [x, y, theta, v]
        # u = [u_a, u_phi]
        return np.matrix([[0, 0], [0, 0], [0, (x[3]/L_car) * pow(1/math.cos(u[1]),2) ], [1, 0]])
        
    return None
    
    
    
def systemDimensionsOfTask(task_number):
    # Dynamic point mass
    if(task_number == 2):
        return 4

    # Differential drive
    if(task_number == 3):
        return 3

    # Kinematic car
    if(task_number == 4):
        return 3

    # Dynamic car
    if(task_number == 5):
        return 4
        
    return None
            



def applyInputConstraints(task_number, u):
    # Dynamic point mass
    if(task_number == 2):
        a_max = scene_params["a_max"]
        if (norm(u) > a_max):                
            u = (u / norm(u)) * a_max
#                self.verbose("Acceleration over limit")
        return u

    # Differential drive
    if(task_number == 3):
        v_max = scene_params["v_max"]
        if (abs(u[0]) > v_max):
            u[0] = sign(u[0]) * v_max
#                self.verbose("Velocity over limit")
        
        omega_max = scene_params["omega_max"]
        if (abs(u[1]) > omega_max):
            u[1] = sign(u[1]) * omega_max
#                self.verbose("Steering rate over limit")
        return u
        
    # Kinematic car
    if(task_number == 4):
        v_max = scene_params["v_max"]
        phi_max = scene_params["phi_max"]
        if (abs(u[0]) > v_max):
            u[0] = sign(u[0]) * v_max
#                self.verbose("Velocity over limit")
        
        if (abs(u[1]) > phi_max):
            u[1] = sign(u[1]) * phi_max
#                self.verbose("Steering angle over limit")
        return u
        

    # Dynamic car
    if(task_number == 5):
        a_max = scene_params["a_max"]
        phi_max = scene_params["phi_max"]
        if (abs(u[0]) > a_max):
            u[0] = sign(u[0]) * a_max
#                self.verbose("Acceleration over limit")
        
        if (abs(u[1]) > phi_max):
            u[1] = sign(u[1]) * phi_max
#                self.verbose("Steering angle over limit")
        return u
        
    return None



    
############### Dynamic constraints for each task (T1-T6) go here ##################
def verifyDynamicConstraints(task_number, x, u):
    # Dynamic point mass
    if(task_number == 2):
        v_max = scene_params["v_max"]
        if (norm(x[2:4]) < v_max):
            return True
        else:
            return False
            
    # Differential drive
    if(task_number == 3):
        return True
        
    # Kinematic car
    if(task_number == 4):
        return True
        
    # Dynamic car
    if(task_number == 5):
        return True
        
        
def sign(x):
    if x>=0:
        return 1
    else:
        return -1

################################################################



epsilon = 0.001
leader_index = 1
num_robots = 2

xG_leader = np.array( (np.mean(goal_points[...,0]), np.mean(goal_points[:,1])) )
x0_leader = np.array( (np.mean(start_points[...,0]), np.mean(start_points[:,1])) )
F_pattern = goal_points - xG_leader
x = np.zeros( (num_robots, 2) )
z = np.zeros( (num_robots, 2) )


agent_color = ['r', 'g', 'b', 'y', 'm', 'c']
fig, ax = plt.subplots()
scene_plot.partial_using_T5_format(scene_params, ax, fig)
ax.plot(xG_leader[0], xG_leader[1], c=agent_color[-1], marker='^')
#ax.plot((x0+F_pattern)[...,0], (x0+F_pattern)[...,1], c=agent_color[4], marker='^')
plt.show()



#delta_x = np.zeros( (num_robots, 2) )
#delta_z = np.zeros( (num_robots, 2) )
#intputs = np.zeros( (num_robots, 2) )


dt = 0.5
find_optimal=True
rrt = RRT_STAR_KP.RRT_STAR_KP(scene_params, find_optimal, 1000)

rrt.set_start_and_goal(x0_leader, xG_leader)

print("Run RRT*")
start = time.time()
(V, E) = rrt.rrt_star()
end = time.time()
print(end - start, "seconds")
print("Finished")

rrt.plot_tree_and_obstacles(scene_params, V, E, rrt.x_start, rrt.x_goal)

V_smooth, E_smooth = rrt.optimal_smoothed_path(scene_params, V, E, rrt.x_start, rrt.x_goal, True)
path = rrt.extract_ordered_path(V_smooth, E_smooth, rrt.x_start, rrt.x_goal)



def simulate_simple(x_start, x_goal, scene, dt, verbose_plots_py=False):
    traj = []
    
    x = np.array(x_start)
    
    if (verbose_plots_py):
        fig, ax = plt.subplots()

    point_reached = False
    while(point_reached == False):
        v = x_goal - x
#            v = (v_vec / np.linalg.norm(v_vec)) * v_max
        
        x = x + v*dt
        
        traj.append(x)
                    
        if(np.linalg.norm(x - x_goal) < 0.5):
            point_reached = True                
        
    return np.array(traj)
    
    



def generate_formation_trajectory(F_pattern, path):
    num_leader_points = path.shape[0]
    F_path = []
    length_subpaths = np.zeros(num_leader_points - 1)
#    num_F_samples_subpath = 

    for i in range(num_leader_points - 1):
        x = path[i, ...]
        x_next = path[i+1, ...]
        length_subpaths[i] = norm(x - x_next)
    
    F_prev = start_points
    for i in range(num_leader_points):
        x = path[i, ...]
        F = F_pattern + x
        not_redundant = True
#        for k in range(F.shape[0]):
#            not_redundant = not_redundant and not poly_tools.obstacle_free(obstacle_triags, F_prev[k,...], F[k,...])
        
        if(i > 0):
            if (not_redundant == True):
                F_path.append(F)

        F_prev = F
        if (i < num_leader_points - 1):
            num_F_samples_subpath = math.floor(length_subpaths[i] / np.sum(length_subpaths) * 10)

            if(num_F_samples_subpath != 0):
                dx = 1.0 / num_F_samples_subpath
                x_goal = path[i+1,...]
                for j in range(num_F_samples_subpath):
                    v = x_goal - x
                    x = x + v*dx
                    F = F_pattern + x
                    obstacle_free = True
                    not_redundant = True
                    for k in range(F.shape[0]):
                        obstacle_free = obstacle_free and not poly_tools.check_point_collision(obstacle_triags, boundary_triags, F[k,...])
#                        not_redundant = not_redundant and not poly_tools.obstacle_free(obstacle_triags, F_prev[k,...], F[k,...])
                        
                    if(obstacle_free == True) and (not_redundant == True):
                        F_path.append(F)
                    F_prev = F

    return F_path




    
F_path = generate_formation_trajectory(F_pattern, path)


fig, ax = plt.subplots()
scene_plot.partial_using_T5_format(scene_params, ax, fig)
ax.plot(xG_leader[0], xG_leader[1], c=agent_color[-1], marker='^')
#ax.plot((x0+F_pattern)[...,0], (x0+F_pattern)[...,1], c=agent_color[4], marker='^')
for i in range(len(F_path)):
    F = F_path[i]
    for j in range(F.shape[0]):
        x = F[j, ...]
        ax.plot(x[0], x[1], c=agent_color[j], marker='s')
        ax.text(x[0], x[1], str(i), fontsize=10)
        
plt.show()


