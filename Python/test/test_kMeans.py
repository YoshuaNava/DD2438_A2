# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 08:28:19 2017

"""

import sys
from pathlib import Path 
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)

import time
import math
import random
from util import load_scene
from classes import mTSP_GA_v2 as MTSP_GA
from classes import ConvexCovers as CC
from classes import MultiAgentRoute as MAR
import numpy as np
from numpy.linalg import norm


# Very important
sys.setrecursionlimit(15000)



scene_params = load_scene.load_from_json('../json_scenes/problem_A12.json')
scene_params["goal_points"] = []
scene_params["num_goal_pos"] = 0


convex_covers = CC.ConvexCovers(scene_params)



print("Step0")
convex_covers.plot_scene()
print("Step1")
convex_covers.load_scene_item_points()
print("Step2")
convex_covers.create_points_dict()
print("Step3")
convex_covers.generate_convex_cover()
print("Step4")
convex_covers.plot_scene_with_convex_covers()
print("Step5")
convex_covers.min_set_cover()
convex_covers.min_set_cover()
print("Step6")
convex_covers.plot_scene_with_convex_covers()
print("Step7")
use_obstacle_penalty = True
adj_matrix = convex_covers.calculate_convex_covers_adjacency_matrix(use_obstacle_penalty)
print("Step8")
origin_points = convex_covers.get_convex_covers_centroids()
print("Step9")
convex_covers.plot_adjacency_matrix()



num_points = len(origin_points)


from scipy.spatial.distance import cdist

def closest_point(point, others):
    distances = cdist(np.atleast_2d(point), others)
#    return others[distances.argmin()]
    return np.argmin(distances)


def cluster_points(covers_centroids, start_points):
    clusters  = []
    for cover in covers_centroids:
#        distance = np.linalg.norm(cover - start_points)  #\
        closest = closest_point(cover, start_points)
        clusters.append(closest)

    return np.array(clusters)
 

def initial_route_from_clusters(clusters, start_points):
    breaks = np.zeros(len(start_points) - 1)
    route = np.zeros(len(clusters))
    total_points_covered = 0
    for i in range( len(start_points) ):
        num_points_agent = np.sum( clusters == i )
        point_indices = np.where(clusters == i)[0]
        print(point_indices)
        for j in range( num_points_agent ):
            route[total_points_covered + j] = point_indices[j]
            
        total_points_covered = total_points_covered + num_points_agent
        if(i < len(start_points) - 1):
            breaks[i] = total_points_covered
        
    return route, breaks
 

clusters = cluster_points(np.array(origin_points), np.array(scene_params["start_points"]))

route, breaks = initial_route_from_clusters(clusters, scene_params["start_points"])




print("Step9")
alpha = 1
num_salesmen = len(scene_params["start_points"])
#min_tour = max(1, math.floor(num_points/num_salesmen))
min_tour = 2
pop_size = 80
num_iter = 5000
mtsp_ga = MTSP_GA.mTSP_GA(origin_points, num_salesmen, min_tour, pop_size, num_iter, alpha, adj_matrix, scene_params)

mtsp_ga.set_initial_route(route, breaks)
mtsp_ga.plot_agents_best_route()

time.sleep(5)
mtsp_ga.run_with_adjacency_matrix()



print("Algorithm finished running")
print("Results")
print("Optimal Breaks:")
print(mtsp_ga.opt_breaks)
print("Optimal Route (list of cities):")
print(mtsp_ga.opt_route)
print("Global Minimum")
print(mtsp_ga.global_min)
mtsp_ga.plot_agents_best_route()
mtsp_ga.plot_fitness_history()

