#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define		NUMERO_ORGANISMOS	1100
#define		NUMERO_GENES		40		//Originalmente seteada en 20.
#define		LIMITE_INFERIOR		0
#define		TASA_CRUCE			0.3
#define		TASA_MUTACION		0.001		//Originalmente seteada en 0.001.
#define 	INTERVALO 			10.24/pow(2,NUMERO_GENES/2)
#define		TRUE				1
#define		FALSE				0
#define		TOLERANCIA_ERROR	0.00001



/****************************************		Proyecto de Robotica e Inteligencia Artificial - Algoritmo genetico 	************************************
 * 		El siguiente codigo se basa en un ejemplo de AG que encontramos en http://just4cool.wordpress.com/2010/01/27/algoritmos-geneticos-un-ejemplo-simple/
 * y que fue desarrollado para hallarle un minimo absoluto a la funcion z=x²+y². Nosotros modificamos la funcion objetivo de dicho algoritmo genetico
 * para llevarlo a ser un metodo para conseguir raices de la funcion z=|sen(x+y)|. Mejoramos la precision (Sustituyendo la funcion abs() de ANSI C por un
 * codigo propio), aumentamos el numero de organismos de las poblaciones, duplicamos el numero de genes, y disminuimos la tolerancia al error.
 * 
 *		No tocamos muchos otros aspectos, pues no tenemos la suficiente experticia como para implementar metodos de seleccion, mutacion y codificacion 
 * mejores que los que ya estan (Seleccion por torneo, mutacion basada en un coeficiente probabilistico y codificacion en binario de numeros double).
 
 *		Integrantes:
 *			Luis Vicens
 *			Emily Pagua
 *			Yoshua Nava
 *
 *********************************************************************************************************************************************************/




/***************************************		Estructura Organismo		*******************************************/
typedef struct {
	int genotipo [NUMERO_GENES];
	double aptitud;
} Organismo;
/*************************************************************************************************************************



/************************************		Funciones y procedimientos del algoritmo		**********************************/
void DecodificarBinario (double *, double *, int *);
double Aptitud (double, double);
int GenerarBinarioAleatorio (void);
Organismo GenerarOrganismo (void);
Organismo *GenerarPoblacion (void);
Organismo *SeleccionTorneos(Organismo *pob);
void MutacionHijos (Organismo *);
void CruzarSeleccion (Organismo *);
Organismo Elite(Organismo *);
void AG();
void imprimePoblacion (Organismo *);
void imprimeGenotipo(Organismo);
void generarGraficoGeneracional(void);




int main(){
	srand(time(NULL));
	AG();
	return EXIT_SUCCESS;
}


/* PROC DecodificarBinario(double *x, double *y, int *genotipo)                       DEV (double)
 * MODIFICA (double *x double *y)
 * EFECTO recibe un vector de enteros compuesto de 0 y 1 que representa dos numeros
 *  codificados en binario. se encarga de convertir estos dos numeros binarios a su
 *  equivalente en decimal con ayuda de la constante PRECISION (incremento del valor entre
 *  cada binario) y la constante LIMITE que es el valor del limite inferior de la repre-
 *  sentacion que en el problema es -5.12   */
void DecodificarBinario(double * x, double * y, int * genotipo)
{
	int i;
	*x = *y = 0.0;
	//printf("    %f\n",*x);
	// calculo del primer decimal
	for(i=0; i < NUMERO_GENES/2 ;i++)
	{
		*x += genotipo[i] * pow(2, (NUMERO_GENES/2)-(i+1));
		//printf("%i    %f\n",i,*x);
		//getchar();
	}
	//printf("%i    %f\n",i,*x);
	*x = (*x) * INTERVALO + LIMITE_INFERIOR;

	//calculo del segundo decimal
	for(i=NUMERO_GENES/2; i < NUMERO_GENES ;i++)
	{
		*y += genotipo[i] * pow(2, NUMERO_GENES-(i+1));
	}
	*y = (*y) * INTERVALO + LIMITE_INFERIOR;
}
 
 
 
/* PROC Aptitud(double p1, double p2)                  DEV (double)
 * MODIFICA nada
 * EFECTO recibe dos valores que representan los puntos que caracterizan a un Organismo
 *  la funcion sirve para calcular la aptitud o Aptitud de cierto Organismo segun sus
 *  puntos. este valor de aptitud es el que devuelve la funcion. */
double Aptitud(double punto1, double punto2)
{
	//La funcion cuya raiz queremos buscar es z=|sin(x+y)|
	double valorFuncion = sin(punto1 + punto2);
	if(valorFuncion < 0)
	{
		return -1.0*valorFuncion;
	}
	else
	{
		return valorFuncion;
	}
}
 
 
 
/* PROC GenerarBinarioAleatorio (void)                               DEV (void)
 * MODIFICA nada
 * EFECTO se encarga de devolver un valor entero que siempre sera cero o uno. lo vamos a
 *  utilizar para generar los Organismos al principio dado que su genoma es una cadena
 *  binaria que se genera aleatoriamente    */
int GenerarBinarioAleatorio (void) {
    if (1 + (int) (10.0*rand()/(RAND_MAX+1.0)) > 5)
	{
        return 1;
	}
    else
	{
        return 0;
	}
}
 
 
 
/* PROC GenerarOrganismo (void)                         DEV (Organismo)
 * MODIFICA nada
 * EFECTO se encarga de generar un Organismo utilizando valores aleatorios. primero crea
 *  la cadena de bits del genotipo utilizando la funcion generaBinario y luego evalua
 *  la aptitud del Organismo utilizando la funcion DecodificarBinario para decodificar el genotipo
 *  y la funcion Aptitud para obtener la aptitud.   */
Organismo GenerarOrganismo (void){
    Organismo individuo;
    int i;
    double x, y;
 
    for (i=0; i<NUMERO_GENES; i++)
	{
        individuo.genotipo[i]=GenerarBinarioAleatorio();
	}
 
    DecodificarBinario(&x, &y, individuo.genotipo);
    individuo.aptitud = Aptitud(x,y);
 
    return individuo;
}
 
 
 
/* PROC GenerarPoblacion (void)                         DEV (Organismo *)
 * MODIFICA nada
 * EFECTO esta funcion genera una poblacion con la cantidad de Organismos dada por la
 *  constante NUMERO_ORGANISMOS. para generar cada Organismo utiliza la funcion GenerarOrganismo()
 *  y una vez ha terminado el bucle, devuelve el puntero al primer Organismo    */
Organismo* GenerarPoblacion(void)
{
    Organismo* poblacion;
    int i;
 
    poblacion = (Organismo*) malloc(sizeof(Organismo)*NUMERO_ORGANISMOS);
	
    for(i=0;i<NUMERO_ORGANISMOS;i++)
	{
        poblacion[i] = GenerarOrganismo();
	}
 
    return poblacion;
}
 
 
 
/* PROC SeleccionTorneos(Organismo *)                  DEV (Organismo *)
 * MODIFICA nada
 * EFECTO se crea un nuevo vector de Organismos que contendra a los Organismos seleccionados
 *  para el cruce. La seleccion se hace por torneos por tanto cada Organismo seleccionado
 *  sera el que tenga mejor aptitud de dos candidatos. el proceso se repite en NUMERO_ORGANISMOS-1
 *  iteraciones, que es la cantidad de Organismos que se deben seleccionar. la reserva de
 *  memoria se hace sobre NUMERO_ORGANISMOS Organismos dado que, como luego vamos a seleccionar el
 *  mejor de la poblacion anterior por elitismo, debemos dejar una plaza de la siguiente
 *  generacion libre. la seleccion es con repeticion */
Organismo* SeleccionTorneos(Organismo* poblacion)
{
    Organismo candidato_a, candidato_b;
    int i;
 
    Organismo* seleccion = (Organismo *) malloc (sizeof(Organismo)*NUMERO_ORGANISMOS);
 
    for (i=0; i<NUMERO_ORGANISMOS-1; i++)
    {
        candidato_a = poblacion[(int) (((double) NUMERO_ORGANISMOS)*rand()/(RAND_MAX+1.0))];
        candidato_b = poblacion[(int) (((double) NUMERO_ORGANISMOS)*rand()/(RAND_MAX+1.0))];
 
        if (candidato_a.aptitud < candidato_b.aptitud)
		{
            seleccion[i] = candidato_a;
		}
        else
		{
            seleccion[i] = candidato_b;
		}
    }
 
    return seleccion;
}
 
 
 
/* PROC MutacionHijos (Organismo*)                         DEV (void)
 * MODIFICA (Organismo *)
 * EFECTO esta funcion aplica la mutacion segun la probabilidad dada por TASA_MUTACION.
 *  Recibe un vector de Organismos en el que debe ocurrir que los dos primeros sean
 *  los hijos correspondientes a un cruce. el genotipo de cada uno de los Organismos
 *  se recorre por completo calculando la probabilidad de que el bit mute y cambiando
 *  si se diera el caso positivo    */
void MutacionHijos (Organismo* hijos)
{
    int i, j;
 
    for(i=0; i<2; i++)

        for(j=0; j<NUMERO_GENES; j++)
            if ((double) rand()/(RAND_MAX+1.0) < TASA_MUTACION)
            {
                if(hijos[i].genotipo[j]==1)
				{
                    hijos[i].genotipo[j] = 0;
				}
                else
				{
					hijos[i].genotipo[j] = 1;
				}
            }
}
 
 
 
/* PROC CruzarSeleccion (Organismo*)                   DEV (void)
 * MODIFICA (Organismo*)
 * EFECTO esta funcion se encarga de cruzar los Organismos seleccionados. la seleccion
 *  esta ordenada luego cruzamos los Organismos seguidos de dos en dos. para cada una
 *  de las iteraciones se aplica la probabilidad de cruce. en caso de que se crucen
 *  los Organismos se genera un punto de cruce y se intercambian las porciones del
 *  vector. luego se llama a la funcion de mutacion. en caso de que no haya cruce, los
 *  padres pasan directamente a la siguiente generacion */
void CruzarSeleccion (Organismo * seleccion)
{
    int i, j, punto, aux;
    double x, y;
 
    for(i=0; i<NUMERO_ORGANISMOS-1 ;i+=2)
    {
        if((double) rand()/(RAND_MAX+1.0) < TASA_CRUCE)
        {
            punto = (int) (((double) NUMERO_GENES)*rand()/(RAND_MAX+1.0));
 
            for(j=punto; j<NUMERO_GENES; j++)
            {
                aux=seleccion[i].genotipo[j];
                seleccion[i].genotipo[j]=seleccion[i+1].genotipo[j];
                seleccion[i+1].genotipo[j]=aux;
            }
 
            MutacionHijos(&seleccion[i]);
 
            DecodificarBinario(&x, &y, seleccion[i].genotipo);
            seleccion[i].aptitud = Aptitud(x,y);
 
            DecodificarBinario(&x, &y, seleccion[i+1].genotipo);
            seleccion[i+1].aptitud = Aptitud(x,y);
        }
    }
}
 
 
 
/* PROC Elite (Organismo* poblacion)                   DEV (Organismo)
 * MODIFICA nada
 * EFECTO se trata de una funcion que devuelve el mejor Organismo de una poblacion
 *  que se pasa como argumento. utiliza un Organismo como comparador y devuelve
 *  el que para nuestro caso tiene el mejor Aptitud, es decir, aptitud mas baja */
Organismo Elite (Organismo* poblacion)
{
    int i;
    Organismo mejor = poblacion[0];
 
    for(i=0; i<NUMERO_ORGANISMOS; i++)
	{
        if(mejor.aptitud > poblacion[i].aptitud)
		{
            mejor = poblacion[i];
		}
	}
 
    return mejor;
}


 
/* PROC AG(void)                                                DEV (void)
 * MODIFICA nada
 * EFECTO se trata de la funcion que ejecuta el algoritmo. el proceso es el siguiente
 *  1 - Generar la poblacion
 *  2 - Seleccion de candidatos al cruce
 *  3 - Cruce de los candidatos (incluye mutacion)
 *  4 - Incluir al mejor de la generacion anterior en la nueva
 *  5 - Repetir el proceso  */
void AG (void)
{
    Organismo *seleccion;
	Organismo *poblacion;
    Organismo mejor;
    int generacion = 0;
    double x, y;
	
	poblacion = GenerarPoblacion();
	mejor = Elite(poblacion);		
	

	if(mejor.aptitud > TOLERANCIA_ERROR)
	{	
		do
		{
			seleccion = SeleccionTorneos(poblacion);
			CruzarSeleccion(seleccion);
			seleccion[NUMERO_ORGANISMOS-1] = Elite(poblacion);
			free(poblacion);
			poblacion = seleccion;
			generacion++;
			mejor = Elite(poblacion);

		} while (mejor.aptitud > TOLERANCIA_ERROR);
	}
	
    free(poblacion);
    DecodificarBinario(&x, &y, mejor.genotipo);
 
    printf ("*************************************\n");
    printf ("*          FIN DEL ALGORITMO        *\n");
    printf ("*************************************\n");
    printf (" - En el punto (%.16f, %.16f)\n", x, y);
    printf (" - Su fenotipo (error) es %.15f\n", mejor.aptitud);
    printf (" - Es la generacion numero %i\n", generacion);
    printf ("*************************************\n");
}



/*********************************************************************************************************************************************************/


