#include <stdio.h>
#include <stdlib.h>

#define		NUMERO_ORGANISMOS	1000		//Originalmente seteada en 100.
#define		NUMERO_GENES		20		//Originalmente seteada en 20.
#define		TRUE				1
#define		FALSE				0
#define		ALELOS				4
#define		MAXIMA_APTITUD		10		//Originalmente seteada en NUMERO_GENES.
#define		TASA_MUTACION		0.001		//Originalmente seteada en 0.001.


void ImprimirGenesOrganismo(char *organismo);
void ImprimirSecuenciaGenesCodificadaLetrasGeneracion(char **generacion);
void ImprimirSecuenciaGenesNumericaGeneracionActual();

/************************************		Variables necesarias del algoritmo		**********************************/
char	**generacionActual, **proximaGeneracion;
char	*organismoModelo;
char	*organismoMejor;
int		*gradoAptitudOrganismos;
int		totalAptitudAlcanzadaEnConjunto;
int		numeroGeneraciones = 1;
/*********************************************************************************************************************/



//Variable para debuggear que esté mejorando la aptitud conforme avanzan las generaciones
int maximaAptitudAlcanzadaActualmente = 0;



/************************************		Funciones y procedimientos del algoritmo		**********************************/
int SeleccionarUnOrganismo()
{
	int organismo;
	int sumaAptitud;
	int puntoAleatorioSeleccion;
	
	sumaAptitud = 0;
	puntoAleatorioSeleccion = rand() % (totalAptitudAlcanzadaEnConjunto + 1);
									
									
									//printf("Punto aleatorio de seleccion: %i\n",puntoAleatorioSeleccion);
									
	for(organismo=0; organismo < NUMERO_ORGANISMOS ;organismo++)
	{
		sumaAptitud += gradoAptitudOrganismos[organismo];
									//printf("Suma de las aptitudes: %i\n",sumaAptitud);
		if(sumaAptitud >= puntoAleatorioSeleccion)
		{
			return organismo;
		}
	}
}



void ProducirProximaGeneracion()
{
	int organismo;
	int gen;
	int primerPadre;
	int segundoPadre;
	int puntoDeCruce;
	int mutarGen;
	
	srand(time(NULL));
	
	for(organismo=0; organismo < NUMERO_ORGANISMOS ;organismo++)
	{
		primerPadre = SeleccionarUnOrganismo();
		segundoPadre = SeleccionarUnOrganismo();
		
		
		//Aleatorizar la salida de la funcion rand en base al tiempo del sistema.
		puntoDeCruce = rand() % NUMERO_GENES;
		
		//primerPadre = rand() % NUMERO_GENES;
		//segundoPadre = rand() % NUMERO_GENES;
		
												//printf("Total aptitud alcanzada: %i\n",totalAptitudAlcanzadaEnConjunto);
												//printf("Primer padre: %i\n Segundo padre: %i\n",primerPadre,segundoPadre);
												//getchar();
		
		for(gen=0; gen < NUMERO_GENES ;gen++)
		{
			mutarGen = rand() % (int)(1.0/TASA_MUTACION);
			
			if(mutarGen == 0)
			{
				proximaGeneracion[organismo][gen] = rand() % ALELOS;
			}
			else
			{
				if(gen < puntoDeCruce)
				{
					proximaGeneracion[organismo][gen] = generacionActual[primerPadre][gen];
				}
				else
				{
					proximaGeneracion[organismo][gen] = generacionActual[segundoPadre][gen];				
				}
			}
		}
	}
	
	for(organismo=0; organismo < NUMERO_ORGANISMOS ;organismo++)
	{
		for(gen=0; gen < NUMERO_GENES ;gen++)
		{
			generacionActual[organismo][gen] = proximaGeneracion[organismo][gen];
		}	
	}
}


int EvaluarOrganismos()
{
	int organismo;
	int gen;
	int aptitudAcumuladaActualmente;
	
	totalAptitudAlcanzadaEnConjunto = 0;

	for(organismo=0; organismo < NUMERO_ORGANISMOS ;organismo++)
	{
		aptitudAcumuladaActualmente = 0;
		
		for(gen=0; gen < NUMERO_GENES ;gen++)
		{
			if(generacionActual[organismo][gen] == organismoModelo[gen])
			{
				aptitudAcumuladaActualmente++;

				/*
				//Bloque de codigo para debuggear que este mejorando la aptitud con cada generacion.
												if (aptitudAcumuladaActualmente > maximaAptitudAlcanzadaActualmente)
												{
													if((organismo==0) && (gen==0))
													{
														maximaAptitudAlcanzadaActualmente = aptitudAcumuladaActualmente;
														printf("Hola %i\n",aptitudAcumuladaActualmente);
													}
													else
													{
															maximaAptitudAlcanzadaActualmente = aptitudAcumuladaActualmente;
															printf("Hola %i\n",aptitudAcumuladaActualmente);
															//printf("Generaciones realizadas: %i\n",numeroGeneraciones);
															ImprimirGenesOrganismo(generacionActual[organismo]);
															ImprimirGenesOrganismo(organismoModelo);
													}
												}
				///////////////////////////////////////////////////////////////////////////////////
				*/
			}
		}
		gradoAptitudOrganismos[organismo] = aptitudAcumuladaActualmente;
		totalAptitudAlcanzadaEnConjunto += aptitudAcumuladaActualmente;
		
		
		if(aptitudAcumuladaActualmente >= MAXIMA_APTITUD)
		{
			organismoMejor = generacionActual[organismo];
			return TRUE;
		}
	}
	
	return FALSE;	
}




void InicializarOrganismos()
{
	int organismo;
	int gen;

	//Aleatorizar la salida de la funcion rand en base al tiempo del sistema.
	srand(time(NULL));
	
	//Inicializar cada uno de los organismos con valores aleatorios en sus genes.
	for(organismo=0; organismo < NUMERO_ORGANISMOS ;organismo++)
	{
		for(gen=0; gen < NUMERO_GENES ;gen++)
		{	
			generacionActual[organismo][gen] = rand() % ALELOS;
		}
	}
	
	//Inicializar cada uno de los genes del organismo modelo con valores aleatorios.
	for(gen=0; gen < NUMERO_GENES ;gen++)
	{
		organismoModelo[gen] = rand() % ALELOS;
	}
}





int EjecutarAlgoritmoGenetico()
{
	int alcanzadaGeneracionPerfecta = FALSE;
	
	InicializarOrganismos();
	
							//printf("Generacion #1:\n");
							//ImprimirSecuenciaGenesNumericaGeneracionActual();
							//ImprimirSecuenciaGenesCodificadaLetrasGeneracion(generacionActual);
	
	
	while(alcanzadaGeneracionPerfecta == FALSE)
	{
		alcanzadaGeneracionPerfecta = EvaluarOrganismos();
		//getchar();

		if(alcanzadaGeneracionPerfecta == FALSE)
		{
			ProducirProximaGeneracion();
			numeroGeneraciones++;
			
							/*
							printf("Generacion #%i:\n",numeroGeneraciones);
							ImprimirSecuenciaGenesCodificadaLetrasGeneracion(generacionActual);
							printf("Organismo modelo:\n");
							ImprimirGenesOrganismo(organismoModelo);
							printf("\n\n");
							*/
		}
		//printf("¿Alcanzada generacion perfecta?   %i   \n",alcanzadaGeneracionPerfecta);
		//printf("Numero de generaciones:   %i   \n",numeroGeneraciones);
	}
	
	return numeroGeneraciones;
	
}



void UbicarVariablesAlgoritmoEnMemoria()
{
	int i;
	
	//Se crean los arreglos que contendran las generaciones de los organismos, que a su vez vienen representados por arrays de char.
	generacionActual = (char**) malloc(sizeof(char*) * NUMERO_ORGANISMOS);
	proximaGeneracion = (char**) malloc(sizeof(char*) * NUMERO_ORGANISMOS);
	
	//Se crea un arreglo de char que representara un organismo modelo.
	organismoModelo = (char*) malloc(sizeof(char) * NUMERO_GENES);
	
	//Se crea un arreglo de enteros que representara la aptitud alcanzada por cada organismo de la generacion actual.
	gradoAptitudOrganismos = (int*) malloc(sizeof(int) * NUMERO_ORGANISMOS);
	
	//Se crea un organismo en cada una de las casillas de los arreglos de las generaciones de los organismos.
	for (i=0; i < NUMERO_ORGANISMOS ;i++)
	{
		generacionActual[i] = (char*) malloc(sizeof(char) * NUMERO_GENES);
		proximaGeneracion[i] = (char*) malloc(sizeof(char) * NUMERO_GENES);
	}
	
}



int main(){
	int numeroGeneracionFinal;
	
	//Se ubica y se da espacio en memoria a las variables que utilizara el algoritmo genetico.
	UbicarVariablesAlgoritmoEnMemoria();
	
	numeroGeneracionFinal = EjecutarAlgoritmoGenetico();
	
	printf("Organismo modelo:\n");
	ImprimirGenesOrganismo(organismoModelo);
	printf("\n\n");
	
	printf("Generaciones que pasaron antes de llegar al resultado final: %i\n", numeroGeneracionFinal);
	printf("Mejor organismo:\n");
	
	ImprimirGenesOrganismo(organismoMejor);
	//printf("HOla!!!!\n");
	return EXIT_SUCCESS;
}
/*********************************************************************************************/









/************************************		Funciones y procedimientos para imprimir		**********************************/
void ImprimirGenesOrganismo(char *organismo)
{
	int gen;
	
	for(gen=0; gen < NUMERO_GENES ;gen++)
	{
		char salida;
		switch(organismo[gen])
		{
			case 0:
				salida = 'C';
				break;
			case 1:
				salida = 'G';
				break;
			case 2:
				salida = 'A';
				break;
			case 3:
				salida = 'T';
				break;
			default:
				break;
		}
		if(gen !=NUMERO_GENES-1)
		{
			printf("%c ",salida);
		}
		else
		{
			printf("%c\n",salida);
		}
	}
}



void ImprimirSecuenciaGenesCodificadaLetrasGeneracion(char **generacion)
{
	int organismo;
	int gen;

	//Inicializar cada uno de los organismos con valores aleatorios en sus genes.
	for(organismo=0; organismo < NUMERO_ORGANISMOS ;organismo++)
	{
		for(gen=0; gen < NUMERO_GENES ;gen++)
		{
			char salida;
			switch(generacion[organismo][gen])
			{
				case 0:
					salida = 'C';
					break;
				case 1:
					salida = 'G';
					break;
				case 2:
					salida = 'A';
					break;
				case 3:
					salida = 'T';
					break;
				default:
					break;
			}
			if(gen !=NUMERO_GENES-1)
			{
				printf("%c ",salida);
			}
			else
			{
				printf("%c\n",salida);
			}
		}
	}
}



void ImprimirSecuenciaGenesNumericaGeneracionActual()
{
	int organismo;
	int gen;

	//Inicializar cada uno de los organismos con valores aleatorios en sus genes.
	for(organismo=0; organismo < NUMERO_ORGANISMOS ;organismo++)
	{
		for(gen=0; gen < NUMERO_GENES ;gen++)
		{
			if(gen !=NUMERO_GENES-1)
			{
				printf("%i ",generacionActual[organismo][gen]);
			}
			else
			{
				printf("%i\n",generacionActual[organismo][gen]);
			}
		}
	}
}
/*********************************************************************************************/



